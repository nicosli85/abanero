-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 05-12-2014 a las 00:59:36
-- Versión del servidor: 5.6.12-log
-- Versión de PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `nicosli_aba`
--
CREATE DATABASE IF NOT EXISTS `nicosli_aba` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `nicosli_aba`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contiene`
--

CREATE TABLE IF NOT EXISTS `contiene` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paquete_id` int(11) NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `incluye` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `no_incluye` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=56 ;

--
-- Volcado de datos para la tabla `contiene`
--

INSERT INTO `contiene` (`id`, `paquete_id`, `descripcion`, `incluye`, `no_incluye`, `created_at`, `updated_at`) VALUES
(1, 2, 'Transportación ida y vuelta', 'si', '', '2014-06-11 23:10:17', '2014-06-11 23:10:17'),
(2, 2, 'Guía Certificado y entradas', 'si', '', '2014-06-11 23:10:17', '2014-06-11 23:10:17'),
(3, 2, 'Comida', 'si', '', '2014-06-11 23:10:17', '2014-06-11 23:10:17'),
(4, 2, 'Seguro de viaje', 'si', '', '2014-06-11 23:10:17', '2014-06-11 23:10:17'),
(5, 2, 'Bebidas', '', 'no', '2014-06-11 23:10:17', '2014-06-11 23:10:17'),
(6, 1, 'Transportación ida y vuelta', 'si', '', '2014-06-12 20:53:05', '2014-06-12 20:53:05'),
(7, 1, 'Guía Certificado y entradas', 'si', '', '2014-06-12 20:53:05', '2014-06-12 20:53:05'),
(8, 1, 'Comida', 'si', '', '2014-06-12 20:53:05', '2014-06-12 20:53:05'),
(9, 1, 'Seguro de viaje', 'si', '', '2014-06-12 20:53:05', '2014-06-12 20:53:05'),
(10, 1, 'Bebidas', '', 'no', '2014-06-12 20:53:05', '2014-06-12 20:53:05'),
(11, 3, 'Transportación ida y vuelta', 'si', '', '2014-06-12 21:05:25', '2014-06-12 21:05:25'),
(12, 3, 'Guía Certificado y entradas', 'si', '', '2014-06-12 21:05:25', '2014-06-12 21:05:25'),
(13, 3, 'Comida', 'si', '', '2014-06-12 21:05:25', '2014-06-12 21:05:25'),
(14, 3, 'Seguro de viaje', 'si', '', '2014-06-12 21:05:25', '2014-06-12 21:05:25'),
(15, 3, 'Bebidas', '', 'no', '2014-06-12 21:05:25', '2014-06-12 21:05:25'),
(16, 7, 'Admission fee to the natural reserve', 'si', '', '2014-07-15 21:34:12', '2014-07-15 21:34:12'),
(17, 7, 'Bilingual guides', 'si', '', '2014-07-15 21:34:12', '2014-07-15 21:34:12'),
(18, 7, 'Specialized equipment (wetsuit, life vest, special shoes and hardhat with headlamp)', 'si', '', '2014-07-15 21:34:12', '2014-07-15 21:34:12'),
(19, 7, 'Towel and lockers', 'si', '', '2014-07-15 21:34:12', '2014-07-15 21:34:12'),
(20, 7, 'Snacks', 'si', '', '2014-07-15 21:34:12', '2014-07-15 21:34:12'),
(21, 7, 'Guests also have access to our facilities (bathrooms, showers, hammocks, etc.)', 'si', '', '2014-07-15 21:34:12', '2014-07-15 21:34:12'),
(22, 7, 'Transfer', 'si', '', '2014-07-15 21:34:58', '2014-07-15 21:34:58'),
(23, 8, 'Transportation in air-conditioned van', 'si', '', '2014-07-15 22:06:22', '2014-07-15 22:06:22'),
(24, 8, 'Professional bilingual guide', 'si', '', '2014-07-15 22:06:22', '2014-07-15 22:06:22'),
(25, 8, 'Entrance fees', 'si', '', '2014-07-15 22:06:22', '2014-07-15 22:06:22'),
(26, 8, 'Snorkeling equipment*', 'si', '', '2014-07-15 22:06:22', '2014-07-15 22:06:22'),
(27, 8, 'Lunch and beverages', 'si', '', '2014-07-15 22:06:22', '2014-07-15 22:06:22'),
(28, 8, 'Insurance', 'si', '', '2014-07-15 22:06:22', '2014-07-15 22:06:22'),
(29, 8, 'Taxes', 'si', '', '2014-07-15 22:06:22', '2014-07-15 22:06:22'),
(30, 9, 'Transportation in air-conditioned van', 'si', '', '2014-07-15 22:18:21', '2014-07-15 22:18:21'),
(31, 9, 'Professional bilingual guide', 'si', '', '2014-07-15 22:18:21', '2014-07-15 22:18:21'),
(32, 9, 'Entrance fees', 'si', '', '2014-07-15 22:18:21', '2014-07-15 22:18:21'),
(33, 9, 'Snorkeling and climbing equipment', 'si', '', '2014-07-15 22:18:21', '2014-07-15 22:18:21'),
(34, 9, 'Swimming with the turtles', 'si', '', '2014-07-15 22:18:21', '2014-07-15 22:18:21'),
(35, 9, 'Prescription masks available on request', 'si', '', '2014-07-15 22:18:21', '2014-07-15 22:18:21'),
(36, 9, 'Lunch and beverages', 'si', '', '2014-07-15 22:18:21', '2014-07-15 22:18:21'),
(37, 9, 'Insurance', 'si', '', '2014-07-15 22:18:21', '2014-07-15 22:18:21'),
(38, 9, 'Taxes', 'si', '', '2014-07-15 22:18:21', '2014-07-15 22:18:21'),
(39, 9, 'Tips', '', 'no', '2014-07-15 22:18:35', '2014-07-15 22:18:35'),
(40, 9, 'Pictures', '', 'no', '2014-07-15 22:23:14', '2014-07-15 22:23:14'),
(41, 9, 'Souvenirs', '', 'no', '2014-07-15 22:23:14', '2014-07-15 22:23:14'),
(42, 9, '$ 7 usd per person for the Bay and marine turtle preservation fees.', '', 'no', '2014-07-15 22:23:14', '2014-07-15 22:23:14'),
(43, 10, 'Transportation in air-conditioned van', 'si', '', '2014-07-15 22:24:46', '2014-07-15 22:24:46'),
(44, 10, 'Professional bilingual guide', 'si', '', '2014-07-15 22:24:46', '2014-07-15 22:24:46'),
(45, 10, 'Entrance fees', 'si', '', '2014-07-15 22:24:46', '2014-07-15 22:24:46'),
(46, 10, 'Snorkeling and climbing equipment', 'si', '', '2014-07-15 22:24:46', '2014-07-15 22:24:46'),
(47, 10, 'Prescription masks available on request', 'si', '', '2014-07-15 22:24:46', '2014-07-15 22:24:46'),
(48, 10, 'Lunch and beverages', 'si', '', '2014-07-15 22:24:46', '2014-07-15 22:24:46'),
(49, 10, 'Insurance', 'si', '', '2014-07-15 22:24:46', '2014-07-15 22:24:46'),
(50, 10, 'Taxes', 'si', '', '2014-07-15 22:24:46', '2014-07-15 22:24:46'),
(51, 11, 'Transportation in air-conditioned van', 'si', '', '2014-07-15 22:29:01', '2014-07-15 22:29:01'),
(52, 11, 'Professional bilingual guide', 'si', '', '2014-07-15 22:29:01', '2014-07-15 22:29:01'),
(53, 11, 'ATV rental fees and taxes', 'si', '', '2014-07-15 22:29:01', '2014-07-15 22:29:01'),
(54, 11, 'Helmet and goggles for driver and passenger', 'si', '', '2014-07-15 22:29:01', '2014-07-15 22:29:01'),
(55, 11, 'Climbing and snorkeling equipment & lunch buffet', 'si', '', '2014-07-15 22:29:01', '2014-07-15 22:29:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE IF NOT EXISTS `estados` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'pausa', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'baja', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paquetes`
--

CREATE TABLE IF NOT EXISTS `paquetes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipos_id` int(11) NOT NULL,
  `estados_id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_en` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcion_en` text COLLATE utf8_unicode_ci NOT NULL,
  `duracion` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `duracion_en` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `latitud` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `longitud` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `enportada` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `enbanner` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `paquetes`
--

INSERT INTO `paquetes` (`id`, `tipos_id`, `estados_id`, `nombre`, `nombre_en`, `descripcion`, `descripcion_en`, `duracion`, `duracion_en`, `latitud`, `longitud`, `enportada`, `enbanner`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Ek Balam & Cenote', 'Ek Balam & Cenote', 'El sitio arqueológico de EkBalam, está localizado a 30 km al norte de la ciudad de Valladolid, a 2 km del poblado maya de Ek Balam que en Maya Yucateco significa "Jaguar negro"  Fue una de las capitales mayas  mas ricas.  En los primeros años del siglo XI, una irrupción extranjera posiblemente de los itzáes sometió los centros de poder de Ek Balam y Yaxuná. \r\nUna señal de la riqueza material y cultural, así como del desarrollo tecnológico y artístico en Ek Balam, está manifestado en la pintura mural, donde los artesanos alcanzaron gran maestría, decorando sus construcciones con increíbles obras de arte, que van desde tapas de bóvedas con diseños monocromáticos, hasta bóvedas completas cubiertas de coloridos y complejos murales.\r\nLa excursión está perfectamente diseñada para conocer los detalles de esta impresionante Zona Arqueológica y sus alrededores.\r\nEl itinerario es como sigue:\r\nSalida temprano por la mañana desde su hotel o punto de reunión, en un cómodo transporte con aire acondicionado, en el trayecto el guía les dará detalles del sitio arqueológico. Una vez llegando a Ek Balam la visita será guiada por un lapso promedio de 40 minutos, después se dará tiempo libre aproximadamente 30 minutos para pasear por la zona arqueológica, subir a la pirámide, caminar por sus habitaciones y entrar en sus bóvedas,  al término alistarse para salir hacia un hermoso Cenote para nadar. Después de haberse refrescado los llevaremos a comer un delicioso y variado buffet en donde podrá elegir entre ricos platillos incluyendo comida regional Yucateca.', '', 'Todo el Día', 'All day', '20.89067001043956', '-88.13604712486267', 'si', 'si', '2014-05-26 20:45:10', '2014-08-13 07:21:03'),
(2, 1, 1, 'Chichen-itza & Cenote', 'Chichen-itza & Cenote', 'La zona arqueológica de Chichén- Itzá es reconocida como una de las 7 Maravillas del mundo moderno, su significado en maya es (Chichén) Boca del pozo; de los (Itzá) brujos de agua,  El dios que representa el sitioarqueologico de acuerdo con la historia es KUKULCAN, representación maya de QUETZALCOATL dios y emperador Azteca, y conocidos cada uno en sus culturas como  “Serpiente Emplumada”.\r\nLa excursión esta perfectamente diseñada para conocer los detalles de esta impresionante Zona Arquelogica y sus alrededores.\r\nEl itinerario es como sigue :\r\nSalida temprano por la mañana desde su hotel o punto de reunión, en un comodo transporte con aire acondicionado, en el trayecto el guía les dara detalles del sitio arqueológico. Una vez llegando a Chichen-Itza la visita será guiada por un lapso promedio de 40 minutos, después se dara tiempo libre aproximadamente 30 minutos para pasear por la zona arqueológica, comprar artesanías y manualidades hechas por los lugareños,  al termino alistarse para salir a un hermoso Cenote para nadar. Despues de haberse refrescadolos llevaremos a comer un delicioso y variado buffet en donde podrá elegir entre ricos platillos incluyendo comida regional yucateca.\r\n', '', 'Todo el Día', 'All day', '20.682457358725596', '-88.5678505897522', 'si', 'si', '2014-06-11 23:09:29', '2014-06-12 20:35:03'),
(3, 1, 1, 'Coba Aldea Maya', 'Coba Mayan Village', 'Existen varias versiones sobre el nombre y traducción del sitio arqueológico de Coba, sin embargo todos convergen en que fue edificada alrededor de lagunas y cenotes por lo que la traducción mas acertada es  “cob” (o “kob”), turbia o picada, y “ha”, agua, que formarían la denominación “lugar de agua turbia o picada”, en referencia a los lagos en torno a los cuales fue edificada la ciudad.Sin duda se trata del asentamiento más importante del noreste de la península de Yucatán, sólo comparable en tamaño e importancia a Chichén Itzá, su rival y enemigo a lo largo de gran parte de su historia prehispánica. Tiene una extensión de poco más de 70 kilómetros cuadrados y una red de 45 caminos (o sacbes) que comunica a los diversos conjuntos del sitio, y con otras comunidades menores, seguramente dependientes de su dominio. Dentro de este complejo sistema de caminos destaca una sacbé de 100 kilómetros que une a Coba con Yaxuná, un sitio arqueológico en el vecino estado de Yucatán.\r\nLa excursión esta perfectamente diseñada para conocer los detalles de esta impresionante Zona Arqueológica y sus alrededores.\r\nEl itinerario es como sigue :\r\nSalida temprano por la mañana desde su hotel o punto de reunión, en un cómodo transporte con aire acondicionado, en el trayecto el guía les dará detalles del sitio arqueológico. Una vez llegando a Coba la visita será guiada por un lapso promedio de 40 minutos, después se dara tiempo libre aproximadamente 30 minutos para pasear por la zona arqueológica, subir a la pirámide, explorar la zona que esta todavía en excavación.Al termino alistarse para salir hacia una tradicional ALDEA MAYA en donde aprenderemos del estilo de vida de los Mayas hoy en día, su artesanía y sus conocimientos ahí mismo encontraremos un hermoso Cenote para nadar. Después de haberse refrescado y convivido con la gente local los llevaremos a comer un delicioso y variado buffet en donde podrá elegir entre ricos platillos incluyendo comida regional yucateca.', '', 'Todo el Día', 'All day', '20.490783301612627', '-87.73280918598175', 'si', 'si', '2014-06-12 21:04:30', '2014-06-12 21:08:31'),
(4, 3, 1, 'Tulum Express', 'Tulum Express', 'Located on the great Mexican Caribbean Coastline, Tulum is the second most visited Mayan city in the Yucatan peninsula. The contrasting Turquoise Caribbean Waters create a beautiful backdrop for those great pictures you sure will cherish once your vacation will be over. An expert and knowledgeable tour guide will walk you through this magnificent city. He will be showing and explaining you the highlighted temples and buildings of the city as how they used to be as back on those days when the city was at its best explendour.\r\n\r\nITINERARY\r\n\r\nThe tour starts at your own hotel’s lobby (where applies). Small group transportation will pick you up and drive you to the archeological site of Tulum. At site location and expert and well knowledge tour guide will explain all the wonderful facts of the City of Tulum. About an hour guided tour in the zone and 45 minutes of free time. Optional shopping time at a local artisan and souvenir market. Be back at the hotel just on time for lunch.\r\n\r\nAMENITIES\r\n\r\nSmall group for better service and comfort\r\nRound trip train ride in Tulum\r\nSECTUR certified professional Tour guides\r\nOne Hour guided and 45 minutes of free time in Tulum\r\nContinental breakfast in Tulum\r\nFree time to wander around or picture taking\r\nA/C Transportation and bottled water on board', '', 'Todo el Día', 'All day', '20.20904530637771', '-87.45932579040527', '', '', '2014-07-15 21:29:36', '2014-07-15 21:29:50'),
(5, 3, 1, 'Tulum and Turtle Sanctuary', 'Tulum and Turtle Sanctuary', 'Located on the great Mexican Caribbean Coastline, Tulum is the second most visited Mayan city in the Yucatan peninsula. The contrasting Turquoise Caribbean Waters create a beautiful backdrop for those great pictures you sure will cherish once your vacation will be over. An expert and knowledgeable tour guide will walk you through this magnificent city. He will be showing and explaining you the highlighted temples and buildings of the city as how they used to be back on those days when the city was at its best explendour. A nice stop at the beautiful Akumal Bay will let you experience an unforgettable encounter with sea turtles, you will have ample time to snorkel and enjoy the beach, take picture or just relax. A must to do visit which should be included in your trip to the Riviera Maya!\r\n\r\nITINERARY\r\n\r\nThe tour starts at your own hotel’s lobby (where applies). Small group transportation will pick you up and drive you to the archeological site of Tulum. At site location and expert and well knowledge tour guide will explain all the wonderful facts of the City of Tulum. About an hour guided tour in the zone and 45 minutes of free time. Optional shopping time at a local artisan and souvenir market. About an hour of snorkel time in Akumal and free time afterwards for great pictures to be taken. Be back at the hotel just on time for lunch.\r\n\r\nAMENITIES\r\n\r\nSmall group for better service and comfort\r\nRound trip train ride in Tulum SECTUR certified professional Tour guides\r\nOne Hour guided and 45 minutes of free time in Tulum\r\nEntrance fee to both Tulum and The Turtle Sanctuary\r\nContinental breakfast in Tulum\r\nOne hour of snorkeling in Akumal\r\nFree time to wander around or picture taking at The Sanctuary\r\nA/C Transportation and bottled water on board', '', 'Todo el Día', 'All day', '20.206789979010612', '-87.46559143066406', '', '', '2014-07-15 21:31:20', '2014-07-15 21:31:23'),
(6, 1, 1, 'Coba Express', 'Coba Express', 'Coba is considered the oldest and the largest City dated from the classic period of the Mayan Empire. The tour will guide you through the 4 most important highlights of Coba; The Church, Its important Drawings and Paintings, The Ball Court, and Nohoch Mul. Nohoch Mul is the tallest pyramid in The Yucatan Peninsula which you will be able to climb to the top of it still. At site location an expert and well knowledgeable tour guide will explain all the wonderful facts of the City of Coba, they are certified tour guides by SECTUR, who will sure get you to travel back in time to rediscover this magnificent ancient city which had been hidden for centuries. Enjoy an optional bike ride at a little extra cost to explore through jungle paths as ancient Mayans did.\r\n\r\nITINERARY\r\n\r\nThe tour starts at your own hotel’s lobby (where applies). Small group transportation will pick you up and drive you to the archeological site of Coba. At site location and expert and well knowledgeable tour guide will explain all the wonderful facts of the City of Coba. About a 2 hour guided tour in the zone and 45 minutes of free time. Optional shopping time at a local artisan and souvenir market. Be back at the hotel just on time for lunch.\r\n\r\nAMENITIES\r\nSmall group for better service and comfort\r\nSECTUR certified professional Tour guides\r\nTwo Hour guided tour in Coba\r\nEntrance fee to Coba\r\nFree time to wander around or picture taking at Coba\r\nA/C Transportation and bottled water on board', '', 'Todo el Día', 'All day', '20.633426851764806', '-87.10681915283203', '', '', '2014-07-15 21:32:27', '2014-07-15 21:32:29'),
(7, 2, 1, 'Rio Secreto', 'Rio Secreto', 'In a half day adventure, swimming in Río Secreto, an extraordinary and unforgettable sensory experience and could very well be one of the great wonders of the world. Come and experience this stunning underground river with thousands of ancient geological structures of dramatic stalactites and stalagmites, columns, pearls, curtains and corals and a variety of colors and textures. It’s like stepping back in time to witness something mysterious and truly spectacular.\r\n\r\nTour Hours Depending Hotel Location:\r\nDaily at 9:00 a.m. | 10:00 a.m. | 12:00 p.m. | 13:00 p.m. | 14:00 pm', '', 'Medio Día', 'All day', '20.73813455404504', '-87.02133178710938', 'si', '', '2014-07-15 21:33:16', '2014-07-15 22:09:01'),
(8, 3, 1, 'Mayan Adventure Snorkeling Tour', 'Mayan Adventure Snorkeling Tour', 'Live a wonderful jungle and ocean adventure discovering in one day three extraordinary ecosystems of the Mayan world and join the snorkeling that will change your way of feeling nature surrounded by exotic marine life in a beautiful ocean inlet with crystalline water; explore an underground cavern in the middle of the jungle, and finally discover the mystical wonders of the Mayan sinkholes (cenotes). This is such a great experience for the lover of water.', '', 'Medio Día', 'All day', '20.73299721279593', '-87.01549530029297', 'si', '', '2014-07-15 22:05:35', '2014-07-15 22:08:52'),
(9, 3, 1, 'Swim with the Turtles', 'Swim with the Turtles', 'Join for a full day of excitement while surrounded by the exotic natural beauties of the Riviera Maya. We’ll start in the middle of the jungle by experiencing the thrill of flying 20m. (66ft.) over the jungle canopy in the zip line circuit. We will then gear you up to rappel 18m. (60ft.) into the depths of a cenote.\r\n\r\nYou will swim and snorkel in the crystal clear waters of a mystical Mayan underground river while exploring astounding rock formations. And top of it off, an authentic Mexican meal to be had while surrounded by the sounds of a nature will await you at the end of your jungle adventure. Finally, we will take you on a short journey to our beach club in Turtle Bay where you will enjoy the best ocean snorkeling while surrounded by marine turtles, and we’ll finish by relaxing in one of the most beautiful sandy beaches of the Riviera Maya.', '', 'Medio Día', 'Middle of the day', '21.031955437049735', '-86.83696746826172', 'si', '', '2014-07-15 22:17:30', '2014-07-15 22:23:17'),
(10, 2, 1, 'Tulum Adrenaline', 'Tulum Adrenaline', 'The perfect combination of culture and adventure\r\nJoin us on the perfect combination of culture and adventure. We will take you to the heart of the Maya land and the majestic archeological site of Tulum with its spectacular views over the Caribbean waters.\r\nExperience the thrill of flying over the jungle canopy on a spectacular zip-line ride, test your tenacity as you rappel 60 ft down into the Mayan jungle and enjoy snorkeling through an underground river with its crystal clear waters and outstanding rock formations. Delight yourself by sampling real Mexican cuisine surrounded by the sounds of nature only to be topped off by traveling back in time to admire the beauty of the archeological site of Tulum, where a certified guide will make you feel part of history while you tour around this ancient ceremonial center. Our guides will accompany you at all times and share valuable information about the local traditions, flora and fauna, all to make sure you experience an XTREME day!', '', 'Medio Día', 'Middle of the day', '20.21581109239457', '-87.47108459472656', '', '', '2014-07-15 22:24:00', '2014-07-15 22:26:27'),
(11, 3, 1, 'Zip Lines and ATV', 'Zip Lines and ATV', 'Welcome to the Jungle!\r\nThe tour departs directly into the jungle over dirt roads where you will get to ride over terrain that is challenging enough for even the most experienced riders, but easy enough for beginners to maneuver at their own pace. After riding for about 30 minutes we arrive to a fresh-water sinkhole in the middle of the jungle called a "cenote" where you will have an opportunity to do the mayan sacrifice and zip into its refreshing waters. After we are done we ride for 45 minutes and arrive to our adventure park where we will fly above the jungle canopy on a thrilling zip line circuit and snorkel in a wondrous underground river with crystal clear waters. As a perfect finish you will enjoy a delicious buffet lunch with real mexican food.\r\nNOTE: Atv’s can be combined with snorkeling with turtles instead of the Cenote\r\nThe tour time is around four hours. Total distance traveled is 25 kilometers (or about 15 miles). Not a bad way to spend your day!\r\nTwo tours daily, early morning and midday with pick-up times depending on the location of your resort.', '', 'Medio Día', 'Middle of the day', '20.220965779522313', '-87.50473022460938', '', '', '2014-07-15 22:27:51', '2014-07-15 22:30:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservaciones`
--

CREATE TABLE IF NOT EXISTS `reservaciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `correo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `comentario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pq_nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `pq_descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pq_duracion` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `pq_latitud` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pq_longitud` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pq_tipo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pq_nino` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pq_adulto` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pq_fecha` date NOT NULL,
  `pq_tarifa_nino` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pq_tarifa_adulto` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `linkreporte` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `payment_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `reservaciones`
--

INSERT INTO `reservaciones` (`id`, `nombre`, `apellidos`, `correo`, `telefono`, `comentario`, `pq_nombre`, `pq_descripcion`, `pq_duracion`, `pq_latitud`, `pq_longitud`, `pq_tipo`, `pq_nino`, `pq_adulto`, `pq_fecha`, `pq_tarifa_nino`, `pq_tarifa_adulto`, `linkreporte`, `payment_id`, `created_at`, `updated_at`) VALUES
(1, 'Nicolas', 'Navarrete', 'nicosli@gmail.com', '9981734337', 'TEST', 'Ek Balam & Cenote', 'El sitio arqueológico de EkBalam, está localizado a 30 km al norte de la ciudad de Valladolid, a 2 km del poblado maya de Ek Balam que en Maya Yucateco significa ', 'Todo el Día', '20.89067001043956', '-88.13604712486267', 'Zonas Arqueológicas y Aldeas Mayas', '4', '3', '2014-12-04', '87.00', '74.00', 'DIKp7nrmFhwQzRNH7cWbVmfgUX5C7zxmXddRvfb926bGk6kFpg', '', '2014-12-05 06:16:46', '2014-12-05 06:16:46'),
(2, 'Nicolas', 'Navarrete', 'nicosli@gmail.com', '9981734337', 'Prueba', 'Ek Balam & Cenote', 'El sitio arqueológico de EkBalam, está localizado a 30 km al norte de la ciudad de Valladolid, a 2 km del poblado maya de Ek Balam que en Maya Yucateco significa ', 'Todo el Día', '20.89067001043956', '-88.13604712486267', 'Zonas Arqueológicas y Aldeas Mayas', '0', '1', '2014-12-05', '87.00', '74.00', 'e3u8WmkhhyxkxeIh5UEebApDjTpEqMSZ94emWziwbYzayUJxYT', '', '2014-12-05 06:19:37', '2014-12-05 06:19:37'),
(3, 'Nicolas', 'Navarrete', 'nicosli@gmail.com', '9981734337', '', 'Ek Balam & Cenote', 'El sitio arqueológico de EkBalam, está localizado a 30 km al norte de la ciudad de Valladolid, a 2 km del poblado maya de Ek Balam que en Maya Yucateco significa ', 'Todo el Día', '20.89067001043956', '-88.13604712486267', 'Zonas Arqueológicas y Aldeas Mayas', '2', '2', '2014-12-16', '87.00', '74.00', '58C8usEg6g94FmhewoMv4DVH9p0dU1HZHugv58B1YhQHtcTSub', '', '2014-12-05 06:40:02', '2014-12-05 06:40:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temporadas`
--

CREATE TABLE IF NOT EXISTS `temporadas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paquetes_id` int(11) NOT NULL,
  `fhInicio` date NOT NULL,
  `fhFin` date NOT NULL,
  `precio_adulto` decimal(10,2) NOT NULL,
  `precio_nino` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `temporadas`
--

INSERT INTO `temporadas` (`id`, `paquetes_id`, `fhInicio`, `fhFin`, `precio_adulto`, `precio_nino`, `created_at`, `updated_at`) VALUES
(1, 2, '2014-06-11', '2014-12-31', '89.00', '75.00', '2014-06-11 23:53:47', '2014-06-11 23:53:47'),
(2, 1, '2014-06-12', '2014-12-31', '87.00', '74.00', '2014-06-12 21:01:35', '2014-06-12 21:01:35'),
(3, 3, '2014-06-12', '2014-06-30', '90.00', '88.00', '2014-06-12 21:11:49', '2014-06-12 21:11:49'),
(4, 7, '2014-01-01', '2014-12-31', '109.00', '50.00', '2014-07-15 22:03:43', '2014-07-15 22:03:43'),
(5, 8, '2014-07-15', '2014-12-31', '119.00', '89.00', '2014-07-15 22:11:12', '2014-07-15 22:11:12'),
(6, 9, '2014-07-15', '2014-12-31', '139.00', '109.00', '2014-07-15 22:19:43', '2014-07-15 22:19:43'),
(7, 10, '2014-07-15', '2014-12-31', '129.00', '99.00', '2014-07-15 22:25:08', '2014-07-15 22:25:08'),
(8, 11, '2014-07-15', '2014-12-31', '136.00', '106.00', '2014-07-15 22:29:28', '2014-07-15 22:29:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE IF NOT EXISTS `tipos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_en` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`id`, `nombre`, `nombre_en`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Zonas Arqueológicas y Aldeas Mayas', 'Zonas Arqueológicas y Aldeas Mayas', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Cenotes y Cavernas', 'Cenotes y Cavernas', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Playas Vírgenes Manglares y Arrecifes', 'Virgin Beaches Coral reefs & Mangroves', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Amusement Parks', 'Amusement Parks', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Adventuras Extremas', 'Extreme Adventures', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `alias`, `nombre`, `apellido`, `created_at`, `updated_at`) VALUES
(1, 'jcoleman@abaexcursiones.com', '$2y$08$r9Gzi5QrtBkoDcZPkVZy9uJx/mUKtoHzY4Gn3CUnOMd7G6vu2GM3O', 'pedrito', 'Pedro', 'Baranda', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'nicosli@nicosli.com', '$2y$08$r9Gzi5QrtBkoDcZPkVZy9uJx/mUKtoHzY4Gn3CUnOMd7G6vu2GM3O', 'nicosliX', 'Nicolas', 'Navarrete', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

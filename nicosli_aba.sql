-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 19-05-2015 a las 17:23:11
-- Versión del servidor: 5.5.42-37.1
-- Versión de PHP: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `nicosli_aba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contiene`
--

CREATE TABLE IF NOT EXISTS `contiene` (
  `id` int(10) unsigned NOT NULL,
  `paquete_id` int(11) NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `incluye` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `no_incluye` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `contiene`
--

INSERT INTO `contiene` (`id`, `paquete_id`, `descripcion`, `incluye`, `no_incluye`, `created_at`, `updated_at`) VALUES
(1, 2, 'Transportación ida y vuelta', 'si', '', '2014-06-11 23:10:17', '2014-06-11 23:10:17'),
(2, 2, 'Guía Certificado y entradas', 'si', '', '2014-06-11 23:10:17', '2014-06-11 23:10:17'),
(3, 2, 'Comida', 'si', '', '2014-06-11 23:10:17', '2014-06-11 23:10:17'),
(4, 2, 'Seguro de viaje', 'si', '', '2014-06-11 23:10:17', '2014-06-11 23:10:17'),
(5, 2, 'Bebidas', '', 'no', '2014-06-11 23:10:17', '2014-06-11 23:10:17'),
(6, 1, 'Air conditioned Transportation / Transportación ida y vuelta', 'si', '', '2014-06-12 20:53:05', '2015-02-12 21:22:13'),
(7, 1, 'Certified Tour Guide / Guía Certificado y entradas', 'si', '', '2014-06-12 20:53:05', '2015-02-12 21:22:13'),
(8, 1, 'Lunch / Comida', 'si', '', '2014-06-12 20:53:05', '2015-02-12 21:22:13'),
(9, 1, 'Insurance/Seguro de viaje', 'si', '', '2014-06-12 20:53:05', '2015-02-12 21:22:13'),
(10, 1, 'Bebidas', '', 'no', '2014-06-12 20:53:05', '2014-06-12 20:53:05'),
(11, 3, 'Transportación ida y vuelta', 'si', '', '2014-06-12 21:05:25', '2014-06-12 21:05:25'),
(12, 3, 'Guía Certificado y entradas', 'si', '', '2014-06-12 21:05:25', '2014-06-12 21:05:25'),
(13, 3, 'Comida', 'si', '', '2014-06-12 21:05:25', '2014-06-12 21:05:25'),
(14, 3, 'Seguro de viaje', 'si', '', '2014-06-12 21:05:25', '2014-06-12 21:05:25'),
(15, 3, 'Bebidas', '', 'no', '2014-06-12 21:05:25', '2014-06-12 21:05:25'),
(16, 7, 'Admission fee to the natural reserve', 'si', '', '2014-07-15 21:34:12', '2014-07-15 21:34:12'),
(17, 7, 'Bilingual guides', 'si', '', '2014-07-15 21:34:12', '2014-07-15 21:34:12'),
(18, 7, 'Specialized equipment (wetsuit, life vest, special shoes and hardhat with headlamp)', 'si', '', '2014-07-15 21:34:12', '2014-07-15 21:34:12'),
(19, 7, 'Towel and lockers', 'si', '', '2014-07-15 21:34:12', '2014-07-15 21:34:12'),
(20, 7, 'Snacks', 'si', '', '2014-07-15 21:34:12', '2014-07-15 21:34:12'),
(21, 7, 'Guests also have access to our facilities (bathrooms, showers, hammocks, etc.)', 'si', '', '2014-07-15 21:34:12', '2014-07-15 21:34:12'),
(22, 7, 'Transfer', 'si', '', '2014-07-15 21:34:58', '2014-07-15 21:34:58'),
(23, 8, 'Air Conditioned Transportation / Transportacion con Aire Acondicionado', 'si', '', '2014-07-15 22:06:22', '2015-02-02 15:03:50'),
(24, 8, 'Professional Tour Guide / Guia Profesional', 'si', '', '2014-07-15 22:06:22', '2015-02-02 15:03:50'),
(26, 8, 'Snorkeling equipment / Equipo de Snorkel', 'si', '', '2014-07-15 22:06:22', '2015-02-02 15:03:50'),
(27, 8, 'Meals and Beverages', 'si', '', '2014-07-15 22:06:22', '2015-02-02 15:03:50'),
(28, 8, 'Insurance', 'si', '', '2014-07-15 22:06:22', '2014-07-15 22:06:22'),
(29, 8, 'Taxes', 'si', '', '2014-07-15 22:06:22', '2014-07-15 22:06:22'),
(30, 9, 'Air conditioned Transportation / Transportacion con Aire Acondicionado', 'si', '', '2014-07-15 22:18:21', '2015-01-31 08:54:18'),
(31, 9, 'Professional bilingual guide / Guia Bilingüe', 'si', '', '2014-07-15 22:18:21', '2015-01-31 08:54:18'),
(33, 9, 'Snorkeling equipment / Equipo de Snorkel', 'si', '', '2014-07-15 22:18:21', '2015-01-31 08:54:18'),
(36, 9, 'Lunch and beverages / Alimentos y bebidas', 'si', '', '2014-07-15 22:18:21', '2015-01-31 08:54:18'),
(37, 9, 'Insurance / Seguro de viaje', 'si', '', '2014-07-15 22:18:21', '2015-01-31 08:54:18'),
(38, 9, 'Taxes / Impuesto', 'si', '', '2014-07-15 22:18:21', '2015-01-31 08:54:18'),
(39, 9, 'Tips / Propinas', '', 'no', '2014-07-15 22:18:35', '2015-01-31 08:54:18'),
(42, 9, 'Pier and Reef Tax / Impuesto de Muelle y Arrecife', '', 'no', '2014-07-15 22:23:14', '2015-01-31 08:54:18'),
(43, 10, 'Transportation in air-conditioned van', 'si', '', '2014-07-15 22:24:46', '2014-07-15 22:24:46'),
(44, 10, 'Professional bilingual guide', 'si', '', '2014-07-15 22:24:46', '2014-07-15 22:24:46'),
(45, 10, 'Entrance fees', 'si', '', '2014-07-15 22:24:46', '2014-07-15 22:24:46'),
(46, 10, 'Snorkeling and climbing equipment', 'si', '', '2014-07-15 22:24:46', '2014-07-15 22:24:46'),
(47, 10, 'Prescription masks available on request', 'si', '', '2014-07-15 22:24:46', '2014-07-15 22:24:46'),
(48, 10, 'Lunch and beverages', 'si', '', '2014-07-15 22:24:46', '2014-07-15 22:24:46'),
(49, 10, 'Insurance', 'si', '', '2014-07-15 22:24:46', '2014-07-15 22:24:46'),
(50, 10, 'Taxes', 'si', '', '2014-07-15 22:24:46', '2014-07-15 22:24:46'),
(51, 11, 'Air Conditioned transportation / Transportacion con Aire Acondicionado', 'si', '', '2014-07-15 22:29:01', '2015-01-31 08:37:16'),
(52, 11, 'Professional bilingual guide / Guia profesional en español', 'si', '', '2014-07-15 22:29:01', '2015-01-31 08:37:16'),
(55, 11, 'Meals & Open Bar / Alimentos & Barra Libre', 'si', '', '2014-07-15 22:29:01', '2015-01-31 08:37:16'),
(56, 12, 'Air Conditioned Transportation / Transportacion con Aire Acondicionado', 'si', '', '2014-12-10 00:13:32', '2015-01-31 08:14:22'),
(57, 12, 'Open Bar / Barra Libre (Nacional)', 'si', '', '2015-01-31 08:08:01', '2015-01-31 08:14:22'),
(58, 12, 'Meals / Alimentos (opcional)', 'si', '', '2015-01-31 08:08:01', '2015-01-31 08:14:22'),
(59, 12, 'Pier Tax / Impuesto de Muelle', '', 'no', '2015-01-31 08:08:01', '2015-01-31 08:14:22'),
(60, 19, '30 Min or 1 Hours Ride', 'si', '', '2015-01-31 08:24:49', '2015-01-31 08:24:49'),
(61, 19, 'Transportation from Playa del Carmen or Riviera Maya', 'si', '', '2015-01-31 08:24:49', '2015-01-31 08:24:49'),
(62, 19, 'No Transportation from Cancun', '', 'no', '2015-01-31 08:24:49', '2015-01-31 08:24:49'),
(63, 19, 'Reef Tax / Impuesto de Arrecife', '', 'no', '2015-01-31 08:24:49', '2015-01-31 08:24:49'),
(64, 11, 'Pier Tax / Impuesto de Muelle', '', 'no', '2015-01-31 08:37:16', '2015-01-31 08:37:16'),
(65, 5, 'Air Conditiones Transportation / Transportacion con Aire Acondicionado', 'si', '', '2015-02-02 13:57:47', '2015-02-02 13:57:47'),
(66, 5, 'Professional Tour Guide / Guia Profesional', 'si', '', '2015-02-02 13:57:47', '2015-02-02 13:57:47'),
(67, 5, 'Equipment / Equipo', 'si', '', '2015-02-02 13:57:47', '2015-02-02 13:57:47'),
(68, 5, 'Reef Tax / Impuesto de Arrecife', '', 'no', '2015-02-02 13:57:47', '2015-02-02 13:57:47'),
(69, 5, 'Tips / Propinas', '', 'no', '2015-02-02 13:57:47', '2015-02-02 13:57:47'),
(70, 4, 'Transportation / Transportacion', 'si', '', '2015-02-02 14:12:40', '2015-02-02 14:12:40'),
(71, 4, 'Professional Tour Guide / Guia Profesional', 'si', '', '2015-02-02 14:12:40', '2015-02-02 14:12:40'),
(72, 4, 'Meals and Beverages / Alimentos y Bebidas', 'si', '', '2015-02-02 14:12:40', '2015-02-02 14:12:40'),
(73, 4, 'Equipment / Equipo', 'si', '', '2015-02-02 14:12:40', '2015-02-02 14:12:40'),
(74, 4, 'Reef Tax / Impuesto de Arrecife', '', 'no', '2015-02-02 14:12:40', '2015-02-02 14:12:40'),
(75, 13, 'Air Conditioned Transportation / Transportacion con Aire Acondicionado', 'si', '', '2015-02-02 14:27:21', '2015-02-02 14:27:21'),
(76, 13, '(OPTIONAL) Meals and Beverages / Alimentos y Bebidas', 'si', '', '2015-02-02 14:27:21', '2015-02-02 14:27:21'),
(77, 13, 'Equipment / Equipo', 'si', '', '2015-02-02 14:27:21', '2015-02-02 14:27:21'),
(78, 14, 'Air Conditioned Transportation / Transportacion con Aire Acondicionado', 'si', '', '2015-02-02 14:42:40', '2015-02-02 14:42:40'),
(79, 14, 'Unlimited Meals and Beverages / Alimentos y Bebidas sin limite', 'si', '', '2015-02-02 14:42:40', '2015-02-02 14:42:40'),
(80, 14, 'Equipment / Equipo', 'si', '', '2015-02-02 14:42:40', '2015-02-02 14:42:40'),
(81, 14, 'Lockers and Towels / Casilleros y Toallas', 'si', '', '2015-02-02 14:42:40', '2015-02-02 14:42:40'),
(82, 15, 'Air Conditioned Transportation / Transportacion con Aire Acondicionado', 'si', '', '2015-02-02 14:51:54', '2015-02-02 14:51:54'),
(83, 15, 'Meals and Soft Drinks / Alimentos y Bebidas (sin alcohol)', 'si', '', '2015-02-02 14:51:54', '2015-02-02 14:51:54'),
(84, 15, 'All Equipment / Todo el Equipo', 'si', '', '2015-02-02 14:51:54', '2015-02-02 14:51:54'),
(85, 15, 'Towels / Toallas', '', 'no', '2015-02-02 14:51:54', '2015-02-02 14:51:54'),
(86, 8, 'Reef Tax / Impuesto de Arrecife', '', 'no', '2015-02-02 15:03:50', '2015-02-02 15:03:50'),
(87, 16, 'Air Conditioned Transportation / Transportacion con Aire Acondicionado', 'si', '', '2015-02-02 15:25:20', '2015-02-02 15:25:20'),
(88, 16, 'Professional Tour Guide / Guia Profesional', 'si', '', '2015-02-02 15:25:20', '2015-02-02 15:25:20'),
(89, 16, 'Meals and Beverages / Alimentos y Bebidas', 'si', '', '2015-02-02 15:25:20', '2015-02-02 15:25:20'),
(90, 16, 'All the Equipment / Todo el Equipo', 'si', '', '2015-02-02 15:25:20', '2015-02-02 15:25:20'),
(91, 17, 'Air Conditioned Transportation / Transportacion con Aire Acondicionado', 'si', '', '2015-02-02 15:40:39', '2015-02-02 15:40:39'),
(92, 17, 'Professional Tour Guide / Guia Profesional', 'si', '', '2015-02-02 15:40:39', '2015-02-02 15:40:39'),
(93, 17, 'Meals and Beverages / Alimentos y Bebidas', 'si', '', '2015-02-02 15:40:39', '2015-02-02 15:40:39'),
(94, 17, 'All the Equipment / Todo el Equipo', 'si', '', '2015-02-02 15:40:39', '2015-02-02 15:40:39'),
(95, 20, '(OPCIONAL) Air Conditioned Transportation / Transportacion con Aire Acondicionado', 'si', '', '2015-02-02 15:57:22', '2015-02-02 15:57:22'),
(96, 20, 'All the Equipment / Todo el Equipo', 'si', '', '2015-02-02 15:57:22', '2015-02-02 15:57:22'),
(97, 20, 'Beer and Soft Drinks / Cerveza, Aguas y Refrescos', 'si', '', '2015-02-02 15:57:22', '2015-02-02 15:57:22'),
(98, 20, 'FISHING LICENSE / LICENCIA PARA PESCA', 'si', '', '2015-02-02 15:57:22', '2015-02-02 15:57:22'),
(99, 22, 'Transportation / Transportacion', 'si', '', '2015-02-03 06:36:57', '2015-02-03 06:36:57'),
(100, 22, 'Swiming Program / Programa de Nado', 'si', '', '2015-02-03 06:36:57', '2015-02-03 06:36:57'),
(101, 22, 'Towels / Toallas', 'si', '', '2015-02-03 06:36:57', '2015-02-03 06:36:57'),
(102, 22, 'Photografs ', '', 'no', '2015-02-03 06:36:57', '2015-02-03 06:36:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE IF NOT EXISTS `estados` (
  `id` int(10) unsigned NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'pausa', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'baja', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paquetes`
--

CREATE TABLE IF NOT EXISTS `paquetes` (
  `id` int(10) unsigned NOT NULL,
  `tipos_id` int(11) NOT NULL,
  `estados_id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_en` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcion_en` text COLLATE utf8_unicode_ci NOT NULL,
  `duracion` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `duracion_en` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `latitud` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `longitud` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `enportada` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `enbanner` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `paquetes`
--

INSERT INTO `paquetes` (`id`, `tipos_id`, `estados_id`, `nombre`, `nombre_en`, `descripcion`, `descripcion_en`, `duracion`, `duracion_en`, `latitud`, `longitud`, `enportada`, `enbanner`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Ek Balam & Cenote', 'Ek Balam & Cenote', 'El sitio arqueológico de EkBalam, está localizado a 30 km al norte de la ciudad de Valladolid, a 2 km del poblado maya de Ek Balam que en Maya Yucateco significa "Jaguar negro"  Fue una de las capitales mayas  mas ricas.  En los primeros años del siglo XI, una irrupción extranjera posiblemente de los itzáes sometió los centros de poder de Ek Balam y Yaxuná. \r\nUna señal de la riqueza material y cultural, así como del desarrollo tecnológico y artístico en Ek Balam, está manifestado en la pintura mural, donde los artesanos alcanzaron gran maestría, decorando sus construcciones con increíbles obras de arte, que van desde tapas de bóvedas con diseños monocromáticos, hasta bóvedas completas cubiertas de coloridos y complejos murales.\r\nLa excursión está perfectamente diseñada para conocer los detalles de esta impresionante Zona Arqueológica y sus alrededores.\r\nEl itinerario es como sigue:\r\nSalida temprano por la mañana desde su hotel o punto de reunión, en un cómodo transporte con aire acondicionado, en el trayecto el guía les dará detalles del sitio arqueológico. Una vez llegando a Ek Balam la visita será guiada por un lapso promedio de 40 minutos, después se dará tiempo libre aproximadamente 30 minutos para pasear por la zona arqueológica, subir a la pirámide, caminar por sus habitaciones y entrar en sus bóvedas,  al término alistarse para salir hacia un hermoso Cenote para nadar. Después de haberse refrescado los llevaremos a comer un delicioso y variado buffet en donde podrá elegir entre ricos platillos incluyendo comida regional Yucateca.', 'The Archaeological Site of Ekbalam, is located 30 km north from the city of Valladolid and 2 km from the Mayan village of Ek Balam which in Yucatec Maya means "black Jaguar" . This was one of the richest Mayan capitals. In the beginnings of the eleventh century, a possible  foreign invasion from the Itzáes submitted the power centers of Ek Balam and Yaxuná.\r\nA sign of the material and cultural wealth as well as technological and artistic development in Ek Balam, is manifested in the mural painting, where artisans achieved great mastery, decorating their buildings with incredible pieces of art, ranging from caps vaults with monochromatic designs, to complete vaults covered with colorful and intricate murals.\r\nThe tour is perfectly designed to know all the details of this impressive archaeological zone and its surroundings.\r\nThe itinerary is as it follows:\r\nEarly morning departure from your hotel or meeting point, in a comfortable air-conditioned transportation, on the way the guide will give details of the archaeological site. Once reaching Ek Balam, visit will be guided for approximately  40 minutes, then approximately 30 minutes of free time to walk around the archaeological area, climb the pyramid, walk through the chambers and into the vaults, then after that get ready to leave for a beautiful Cenote swimming. After being refreshed we will have a delicious and varied buffet where you can choose from delicious dishes including regional Yucatecan food.\r\n', 'Todo el Día', 'All day', '20.89067001043956', '-88.13604712486267', 'si', 'si', '2014-05-26 20:45:10', '2015-02-12 21:22:49'),
(2, 1, 1, 'Chichen-itza & Cenote', 'Chichen-itza & Cenote', 'La zona arqueológica de Chichén- Itzá es reconocida como una de las 7 Maravillas del mundo moderno, su significado en maya es (Chichén) Boca del pozo; de los (Itzá) brujos de agua,  El dios que representa el sitioarqueologico de acuerdo con la historia es KUKULCAN, representación maya de QUETZALCOATL dios y emperador Azteca, y conocidos cada uno en sus culturas como  “Serpiente Emplumada”.\r\nLa excursión esta perfectamente diseñada para conocer los detalles de esta impresionante Zona Arquelogica y sus alrededores.\r\nEl itinerario es como sigue :\r\nSalida temprano por la mañana desde su hotel o punto de reunión, en un comodo transporte con aire acondicionado, en el trayecto el guía les dara detalles del sitio arqueológico. Una vez llegando a Chichen-Itza la visita será guiada por un lapso promedio de 40 minutos, después se dara tiempo libre aproximadamente 30 minutos para pasear por la zona arqueológica, comprar artesanías y manualidades hechas por los lugareños,  al termino alistarse para salir a un hermoso Cenote para nadar. Despues de haberse refrescadolos llevaremos a comer un delicioso y variado buffet en donde podrá elegir entre ricos platillos incluyendo comida regional yucateca.\r\n', 'The archaeological site of Chichen Itza is recognized as one of the 7 Wonders of the modern world, its meaning in Maya is (Chichén) Mouth of the well; of (Itza) water warlocks, the God that represents this Arqueolocical Site according to the history is KUKULCAN, known in the Mayan Culture as the "Feathered Serpent" In the Aztec Culture there is a similar god called Quetzalcoatl. The tour is designed to learn all the details of this impressive Arquelogical Zone and its surrounding areas.\r\nThe itinerary is as follows:\r\nEarly morning departure from your hotel or meeting place, in a comfortable air-conditioned transportation, on the way the guide will give you details of the archaeological site. Once arriving at Chichen-Itza visit will be guided by an average time of 40 minutes, right after you will be given about 30 minutes free time to walk around the archaeological site, buy arts and crafts made by locals, then we’ll depart to a beautiful Cenote to go swimming. After the refreshing experience just as the Mayans used to do we’ll have a delicious and varied buffet where you can choose from regular dishes to  regional Yucatecan food.\r\n', 'Todo el Día', 'All day', '20.682457358725596', '-88.5678505897522', 'si', 'si', '2014-06-11 23:09:29', '2014-12-27 00:11:30'),
(3, 1, 1, 'Coba Aldea Maya', 'Coba Mayan Village', 'Coba es considerado el más antiguo y el más grande sitio arqueologico de la ciudad, que data del período clásico del Imperio Maya. El tour gira le guiará a través de los 4 puntos más importantes de Coba; La Iglesia, Sus dibujos y pinturas , El campo donde se llevaba acabo el famoso Juego de Pelota, y Nohoch Mul. \r\nNohoch Mul es la pirámide más alta de la Península de Yucatán, que usted será capaz de subir hatsa la cima. En el lugar un guía experto le explicará todos los maravillosos hechos de la ciudad de Cobá, todos nuestros guias estan certificados por SECTUR, ellos le garantizarán un viaje atravez del tiempo para volver a descubrir esta magnífica ciudad antigua que había estado oculta durante siglos. Disfrute de un paseo en bicicleta (opcional) para explorar a través de senderos de la selva como antiguos mayas hicieron a pie.\r\n\r\nITINERARIO\r\n\r\nEl recorrido comienza en su hotel o punto de encuentro, en un transporte con aire acondicionado que le recogerá y le conducirá a la zona arqueológica de Cobá. en el sitio le espera un experto guía turístico quien le explicará todos los maravillosos hechos de la ciudad de Cobá. La visita guiada dura aproximadamente 1 hora en la zona y 45 minutos de tiempo libre. Compras Opcionales, tiempo en un mercado artesanal y de regalos. Después de esto se preparan para una rica nadada en un cenote, estilo maya, para posteriormente  disfrutar de un delicioso almuerzo buffet.', 'Coba is considered the oldest and the largest City dated from the classic period of the Mayan Empire. The tour will guide you through the 4 most important highlights of Coba; The Church, Its important Drawings and Paintings, The Ball Court, and Nohoch Mul. Nohoch Mul is the highest pyramid in The Yucatan Peninsula which you will be able to climb to the top. At the site location an expert and well tour guide will explain to you all the wonderful facts of the City of Coba, they are certified tour guides by SECTUR, who will ensure you to travel back in time to rediscover this magnificent ancient city which had been hidden for centuries. Enjoy an optional bike ride to explore through jungle paths as ancient Mayans did by barefoot.\r\n\r\nITINERARY\r\n\r\nThe tour starts at your hotel or meeting point, in an airconditioned transportation that will pick you up and drive you to the archeological site of Coba. At the site location and expert tour guide will explain all the wonderful facts of the City of Coba. About a 1 hour guided tour in the zone and 45 minutes of free time. Optional shopping, time at a local artisan and souvenir market. After this get ready for a Cenote Swim, mayan stile, and to ejoy a delicious buffet lunch.', 'Todo el Día', 'All day', '20.490783301612627', '-87.73280918598175', 'si', 'si', '2014-06-12 21:04:30', '2014-12-27 02:05:19'),
(4, 3, 1, 'Biosfera de Sian Ka''an', 'Sian Ka''an''s Biosphere', 'Actividad diseñada para adictos de aventura y paisaje natural a bordo de un jeep o safari, dejándote perder entre manglares entretejidos por debajo de aguas cristalinas. Este lugar sigue formando parte del segundo arrecife a nivel mundial siendo declarado Patrimonio de la Humanidad por la UNESCO y siendo hogar de fantásticas aves, tortugas y delfines en su ambiente natural.', 'Activity designed for addicts of adventure and natural scenery on board of a jeep or safari truck, traveling between mangroves, blue crystal waters below the ocean. This place is still part of the second reef in the world being declared a World Heritage Site by UNESCO and being home to uncountable species of birds, turtles and dolphins in their natural environment.', 'Todo el Día', 'All day', '20.091845933148818', '-87.4744963645935', '', '', '2014-07-15 21:29:36', '2015-02-02 14:13:00'),
(5, 3, 1, 'Nado con Turtugas', 'Under water world Sea Turtle Snorkel', 'En medio día de aventura sumérgete en uno de los mas hermosos arrecifes del aérea y disfruta de un snorkel único en su especie dejándote guiar a lo largo de un santuario de tortugas en su hábitat natural junto con la gran variedad de peces multicolores y un hermoso coral para después refrescarte en el agua cristalina dentro de un paisaje sobrenatural de un cenote o una caverna milenaria, donde podrás ver las impresionantes formaciones rocosas de estalactitas y estalagmitas incluso bajo el agua !!.', 'In half day dive into one of the most beautiful reefs and enjoy one of its kind snorkel activity with a professional tour guide that will take you along a turtle sanctuary in their natural habitat among with an incredible variety of colorful fish and beautiful coral. After that, cool off in the clear water in a supernatural landscape of a “cenote” or ancient cave, where you will see the amazing rock formations of stalactites and stalagmites even underwater !!.', 'Medio Día', 'All day', '20.399864132647398', '-87.30802774429321', '', '', '2014-07-15 21:31:20', '2015-02-02 13:58:50'),
(6, 1, 1, 'Tulum Express', 'Tulum Express', 'Situado en el sur de Playa del Carmen, Tulum es la segunda ciudad Maya más visitada de la península de Yucatán después de Chichén Itzá. Las contrastantes aguas color turquesa del Caribe crean un hermoso paisaje para quienes disfrutan de grandes paisajes, usted podrá disfrutar de una vista espectacula cuando el sol se eleva sobre el Caribe. Un experto y experto guía turístico le guiará a través de esta magnífica ciudad. Él estará mostrando y explicando acerca de los templos y edificios de esta ciudad comercial tal y como era solía ser tan hace siglos cuando la ciudad estaba en su mejor esplendor.\r\nITINERARIO\r\nRecogida en su hotel o lugar de reunion. La excursión comienza una vez llegando a Tulum , alrededor de una hora el tour sera guiado en la zona arqueologica y 45 minutos aproximadamente tendrán de tiempo libre,  compras opcionales en el mercado. Posteriormente regreso a su hotel \r\n', 'Located at the South of Playa del Carmen, Tulum is the second most visited Mayan city in the Yucatan peninsula after Chichen Itza. The contrasting Turquoise Caribbean Waters create a beautiful landscape for those who enjoy great pictures you to catch spectacular vistas when the sun is rising over the Caribbean. We are sure will cherish once your vacation will be over. An expert and knowledgeable tour guide will walk you through this magnificent city. He will be showing and explaining you the highlighted temples and buildings of this commercial city as how they used to be as back on those days when the city was at its best splendor. \r\nITINERARY\r\nPick up at the hotel’s lobby (where applies). Small group transportation will drive you to the archeological site of Tulum. At site location and expert and well knowledge tour guide will explain all the wonderful and most important facts of the City of Tulum. About an hour guided tour in the zone and 45 minutes aprox of free time. Optional shopping time at the market..right after well bringo you back to your hotel ', 'Medio Día', 'All day', '20.633426851764806', '-87.10681915283203', '', '', '2014-07-15 21:32:27', '2014-12-27 23:33:39'),
(7, 2, 1, 'Rio Secreto', 'Rio Secreto', 'En una aventura de medio día, nadar en Río Secreto, es una experiencia sensorial extraordinaria e inolvidable y muy bien podría ser una de las grandes maravillas del mundo. Venga y disfrute de este río subterráneo espectacular con miles de antiguas estructuras geológicas de estalactitas y estalagmitas, columnas, perlas, las cortinas y los corales y una gran variedad de colores y texturas. Es como retroceder en el tiempo para presenciar algo misterioso y verdaderamente espectacular.\r\n\r\nHoras Turísticos Dependiendo la ubicación del hotel:\r\nDiariamente a las 9:00 am | 10 a.m. | 24:00 | 13:00 pm | 14:00 pm \r\n', 'In a half day adventure, swimming in Río Secreto, an extraordinary and unforgettable sensory experience and could very well be one of the great wonders of the world. Come and experience this stunning underground river with thousands of ancient geological structures of dramatic stalactites and stalagmites, columns, pearls, curtains and corals and a variety of colors and textures. It’s like stepping back in time to witness something mysterious and truly spectacular.\r\n\r\nTour Hours Depending Hotel Location:\r\nDaily at 9:00 a.m. | 10:00 a.m. | 12:00 p.m. | 13:00 p.m. | 14:00 pm', 'Medio Día', 'All day', '20.72930464106847', '-87.00534582138062', 'si', '', '2014-07-15 21:33:16', '2014-12-27 23:50:46'),
(8, 3, 1, 'Mayan Adventure Snorkeling Tour', 'Mayan Adventure Snorkeling Tour', 'Ven y únete a nosotros en una maravillosa experiencia en la selva y el mar, descubriendo en un día tres extraordinarios ecosistemas en esta zona del mundo maya, la exótica vida marina en una hermosa caleta con aguas cristalinas que desemboca en el océano; Después exploraremos una caverna subterránea en medio de la selva con sus formaciones rocosas, y finalmente juntos descubriremos las maravilla y misticidad de un cenote abierto o alberca natural.\r\nDesde hoteles en Cancun : $119 USD Adult | $89 USD Menor (5 a 11 años)', 'Come join us for a wonderful experience in the jungle and the sea, discovering in one day three extraordinary ecosystems in this area of the Mayan world, the exotic marine life in a beautiful cove with crystal clear water that flows into the ocean; Then explore an underground cavern in the middle of the jungle with its rock formations, and finally  discover the wonder and mystique of an open cenote or natural pool.\r\nFrom Cancun Hotels: $119 USD Adult | $89 USD Children (5 to 11 years old)\r\n', 'Medio Día', 'All day', '20.73299721279593', '-87.01549530029297', 'si', '', '2014-07-15 22:05:35', '2015-02-02 15:03:55'),
(9, 3, 1, 'Isla Contoy', 'Isla Contoy', 'Disfruta de un día soleado a bordo de nuestro barco estando en contacto directo con la naturaleza y visitando el segundo arrecife más grande del mundo donde encontraras una gran variedad de especies marinas para después vivir una experiencia única en una isla virgen visitando un museo de flora y fauna para después dar paso a un delicioso buffet 100% caribeño a base de pescado asado y bebidas refrescantes mientras va cayendo el atardecer.', 'Enjoy a sunny day on board our boat surrounded by nature and visiting the world''s second largest reef, where you will find a wide variety of marine species, then a unique experience in a virgin island visiting a museum of flora and wildlife. Then enjoy a delicious buffet 100% based on Caribbean grilled fish and refreshing drinks while watching the sunset.', 'Todo el Día', 'Middle of the day', '21.23482233344449', '-86.74246788024902', 'si', '', '2014-07-15 22:17:30', '2015-01-31 08:57:36'),
(10, 2, 1, 'Aktunchen', 'Aktunchen', 'Aktun Chen es algo más que un Parque Natural. Es una aventura que hace una gran diferencia en tu viaje, si te vas a quedar en la Riviera Maya. Aktun Chen significa en Maya "pozo natural dentro de una cueva." Los espectaculares 640 metros (4 millas) de la cueva, que inspiró el nombre de Parque Natural de Aktun Chen es el sistema de cuevas seco más grande de la Riviera Maya. Este parque cerca de Tulum es un divertido y educativo santuario cuando usted está buscando un descanso de la arena y las olas de la playa.\r\n', 'Aktun Chen is more than just a Natural Park. It’s an adventure that makes a great day trip list if you are staying in Riviera Maya. Aktun Chen means in Maya “natural well inside a cave.” The spectacular, 640 meters (4 miles) of cave that inspired the name for Aktun Chen Natural Park is the largest dry cave system in the Riviera Maya.  This  park near Tulum is a fun and educational get-away when you are looking for a break from the sand and surf of the beach.', 'Medio Día', 'Middle of the day', '20.21581109239457', '-87.47108459472656', '', '', '2014-07-15 22:24:00', '2014-12-28 00:02:50'),
(11, 3, 1, 'Isla Mujeres (Catamaran)', 'Isla Mujeres (Catamaran)', 'Si lo que deseas es una vista espectacular de aguas cristalinas te invitamos a velear en nuestro Catamaran partiendo desde la bahía de Cancún con vistas indescriptible del Caribe Mexicano hasta arribar a Isla Mujeres, en donde te espera un snorkel para conocer la vida marina que rodea este hermoso lugar para después disfrutar de lugares típicos mexicanos en esta pequeña pero encantadora isla de pescadores.\r\n', 'If what you want is a spectacular view of crystal clear waters we invite you to sail on our Catamaran leaving from the bay of Cancun with indescribable views of the Mexican Caribbean on our way to Isla Mujeres, where you will snorkel and enjoy the beautiful marine life that surrounds this amazing place. Right after take a break enjoying the Island’s surrandings in this traditional Mexican fishermen village.', 'Todo el Día', 'Middle of the day', '21.233062254412808', '-86.73225402832031', '', '', '2014-07-15 22:27:51', '2015-01-31 08:39:21'),
(12, 3, 1, 'Reef snorkel (Maroma Catamaran)', 'Reef snorkel (Maroma Catamaran)', 'Prepárate en una aventura a bordo de un Catamaran con snorkel incluido ya sea para disfrutar de un dia soleado sintiendo la brisa del mar o bien una hermosa puesta de sol en nuestro sunset Cruise veleando alrededor de una de las mejores playas reconocidas a nivel mundial mientras disfrutas de música y bebidas a bordo. Ideal para grupos, bodas y eventos especiales. (Comida opcional)\r\n', 'Get ready for an adventure aboard of a Catamaran, snorkel during the day or enjoy a nice Sunset  Cruise, sailing around in one of the most beautiful beaches worldwide known MAROMA while enjoying music and drinks on board. (Lunch or Dinner optional) Ideal for groups, weddings and special events.', 'Medio Día', '', '20.739057651567244', '-86.96513414382935', '', '', '0000-00-00 00:00:00', '2015-03-26 21:59:29'),
(13, 4, 1, 'Xcaret', 'Xcaret', 'Xcaret es un parque temático MAYA que fue construido e inspirado por nuestro querido México. Un santuario para todos nuestros visitantes de todas las edades y culturas para apreciar la diversidad de la cultura mexicana más allá de la Península de Yucatán. \r\nXcaret abre sus puertas a lo largo del Mar Caribe para invitar a descubrir experiencias mágicas donde los ríos subterráneos le esperan, junto con la fauna de la selva maya y el complemento perfecto para aquellos amantes de los animales al encontrar jaguares, flamencos y tortugas, sin olvidar el colorido pabellón de mariposas y la parada obligatoria en el acuario natural. \r\nCuando cae la noche, usted será el invitado de honor en el show presentación "México Espectacular" una celebración de luz y color con más de 300 artistas que se llevará a su corazón a través de la historia de México desde los tiempos prehispánicos hasta nuestros días con el juego de pelota maya  (un ritual que solía ser jugado entre paredes que descendían hacia el interior, y colgando en lo alto de las paredes había anillos de piedra. El objetivo del juego era pasar el balón, sin tener que utilizar sus manos, y luego buscar que la pelota pasara a través de uno de los anillos. Inusualmente a lo cotidiano, el equipo ganador tras haber jugado varios dias y haber vencido, eran sacrificados, y esto era un gran honor.', 'Xcaret is a Mayan theme park, that was built and inspired by our beloved Mexico. A sanctuary for all our visitors of all ages and cultures to appreciate the diversity of Mexican culture beyond the Yucatan Peninsula.\r\nXcaret opens along the Caribbean Sea to invite you to discover magical experiences where underground rivers await you, along with the fauna of the Mayan jungle and the perfect complement for those animal lovers to find jaguars, flamingos and turtles without forgeting the colorful butterfly pavilion and the obligatory stop at the natural aquarium.\r\nWhen night falls, you will be the guest of honor at the show "Mexico Spectacular" a celebration of light and color with over 300 artists who will take your heart through the history of Mexico from pre-Hispanic times to the present day the Mayan ball game (a ritual that used to be played between walls descending inwards, and hanging high on the walls were stone rings. The goal was to move the ball without using their hands or feet, and then make the ball passed through one of the rings. Unusual as it may look, the winning team after playing several days and have conquered, were sacrificed, and this was a great honor.\r\n', 'Todo el Día', '', '20.580945502859933', '-87.11940407752991', '', '', '2014-12-10 04:47:25', '2015-02-02 14:27:25'),
(14, 4, 1, 'XEL-HA', 'XEL-HA', 'Situado a 50 minutos de Playa del Carmen, le ofrece un plan completo para disfrutar de un día completo en el parque ecológico (con su régimen de todo incluido: alimentos, bebidas, equipo de snorkel, llantas flotantes, tour con bicicletas, toallas y casilleros). En este parque acuatico natural se puede admirar y disfrutar de la belleza de aguas cristalinas, un sin fin de peces de colores de todos los tamaños y especies debido a la combinacion de del agua salada con la dulce. El lugar perfecto para  disfrutar de la aventura con las 3 líneas de tirolesas, en el que habrá un aterrizaje en el agua, o saltar desde la parte superior de la piedra del valor con aproximadamente 3 metros de alto. ', 'Located 50 minutes from Playa del Carmen, offers a complete plan to enjoy a full day in the ecological park (with all inclusive: food, beverages, snorkel equipment, inner tubes, bicycle tour, towels and lockers ). In this natural aquatic park you can admire and enjoy the beauty of crystal clear waters, endless colorful fish of all sizes and species due to the combination of salt water with fresh. The perfect place to enjoy the adventure with 3 zip lines, in which you will be landing in water, or jumping from the top of the courage  stone with about 3 meters high ', 'Todo el Día', '', '20.319395522476075', '-87.3571926355362', '', '', '2014-12-10 04:51:02', '2015-02-02 14:42:45'),
(15, 4, 1, 'XPLOR', 'XPLOR', 'La selva cobra vida en el mejor parque de aventura en la Riviera Maya. Parque Xplor es un mundo subterráneo único, donde podrá disfrutar de una aventura sin igual. Durante el dia deslizándote en tirolesas  entre puentes y arboles gigantescos , descubre increíbles cuevas y formaciones rocosas .  Por la noche, la Luna y las estrellas guiarán tu camino, maravillate con los misterios que Xplor Fuego tiene para ti.', 'The jungle comes to life in the best adventure park in Riviera Maya. Xplor Park is a unique underground world, where you will enjoy an unparalleled adventure. During the day fly between trees and bridges, discover amazing caves and extraordinary rock formations. And at night, the Moon and stars will guide your way, marvel at the mysteries that Xplor Fuego has for you.', 'Todo el Día', '', '20.59343983184492', '-87.12453782558441', '', '', '2014-12-10 04:52:01', '2015-02-02 14:52:01'),
(16, 5, 1, 'MAYAN EXTREME', 'MAYAN EXTREME ', 'En un perfecto medio día! Iremos al siguiente nivel EXTREMO! Cuando alto no es  suficientemente y rápido tampoco es lo suficientemente, ya nada es lo mismo así entonces prepárate para experimentar "extremo", donde tendrá el "viaje de tu vida" en las más largas y altas tirolesas a gran velocidad (12) en un parque completamente rústico y privado. Después de tu aventura y la adrenalina que regresa a la normalidad, es el momento de abordar el camión del ejército y adentrarse en la selva para refrescarse el agua cristalina de un gran cenote', 'Is a perfect half day! We''ll go to the next level of the EXTREME!! finding higher and sufficiently quick till you have enough, nothing is the same get ready to experience "trip of a lifetime" in the longest and highest 12 zip lines at high speed in a completely rustic and private park. After your adventure and adrenaline then come back to normal, it is time to get on the army truck and into the jungle towards a cool sinkhole with crystal clear water. ', 'Medio Día', '', '20.85363845335004', '-86.92981481552124', '', '', '2014-12-10 04:55:56', '2015-04-23 06:46:46'),
(17, 5, 2, 'GIMME ALL COMBO EXTREMO', 'GIMME ALL EXTREME COMBO', 'Una de las actividades principales y sólo para los conocedores de todo lo que es adrenalina.. Rete su agilidad al conducir un Polaris RZR en el circuito de la selva, a continuación, al lado están las 12 mejores tirolesas donde estará volando de árbol en árbol disfrutando de las vistas de la selva. Si esto no es suficiente existe una serie de puentes aéreos ligeramente desafiantes suspendidos en los árboles, debe mantener la adrenalina para estar preparado para lo que sigue ..El Tarzania!!!! El viaje más nuevo con la combinación de la montaña rusa y tirolesa. Usted estará subiendo y bajando, girando y gritando todo el camino a través de la selva. Después de volver a tierra firme estara listo para el gigante salto en bungee en medio de la selva para finalmente terminar el día nadando en el hermoso cenote volando y sintiendose como un super heroe volando en la última atracción', 'One of the main activities and only for connoisseurs of adrenaline games .. Start by driving a Polaris RZR in the circuit of the jungle, next are the top 12 zip lines where you will be flying from tree to tree enjoying the views of the jungle. If this is not enough there are a number of slightly challenging flight stopovers suspended in the trees, you should keep the adrenaline to be prepared for what follows ..The Tarzania !!!! The new journey with a combination of a roller coaster and zip line together. You will be up and down, twisting and screaming all the way through the jungle. After returning to the mainland we’ll be ready for the giant bungee jumping in the jungle to finally end the day swimming in the beautiful cenote , and feeling like a super hero flying in the latest attraction', 'Todo el Día', '', '20.85359834926333', '-86.92852735519409', '', '', '2014-12-10 04:57:41', '2015-04-23 06:49:20'),
(18, 5, 1, 'SNORKEL EXTREME', 'SNORKEL EXTREME', 'Sorprendete con la biodiversidad de México,  practica snorkel rodeado de tortugas marinas, corales y peces multicolores mientras disfrutas de una playa de arena blanca.\r\nDéjanos llevarte a  volar sobre la selva mientras recorres el circuito de tirolesas más rápido y emocionante de la Riviera Maya. Después te prepararemos con todo el equipo necesario para descender a rappel dentro de la selva y exploraremos un río subterráneo en medio de la selva disfrutando al máximo la belleza de los cenotes mayas. \r\nAl terminar el excitante recorrido de aventura, te esperará un delicioso buffet con comida de la región, bajo una palapa típica y rodeado por los sonidos de la naturaleza. \r\nDuración: Aproximadamente 7 horas desde el momento de ser recogido en el hotel hasta regresar al mismo.\r\n\r\nEl tour incluye: Transportación en van con aire acondicionado, guía bilingüe, entradas, equipo de snorkeling y montañismo, comida, agua y refrescos, seguros e impuestos. \r\n', 'Surprise yourself with Mexico´s biodiversity and snorkel between marine turtles, coral and colorful fish while you enjoy the best sandy beach around. Fly above the jungle canopy on a thrilling zip line ride! Test your tenacity as you rappel into the Mayan jungle and explore an underground river in the middle of the jungle. \r\nYou will be joined at all times by our professional guides, who will share with you the best snorkeling techniques and information regarding the flora and fauna that inhabits these unique ecosystems. \r\nDelight yourself with real Mexican cuisine as a perfect finish. Duration: Approximately 7 hours from pickup to drop off.\r\n\r\nTour includes: A/C Transportation, professional bilingual guide, entrance fees, climbing and snorkeling equipment, lunch and beverages, insurance and taxes\r\n', 'Medio Día', '', '20.462319042072423', '-87.27060556411743', '', '', '2014-12-10 04:58:40', '2015-05-14 21:23:20'),
(19, 5, 1, 'Motos Acuaticas (Maroma)', 'Wave Runners (Maroma)', 'Para aquellos que disfrutan de la vida del mar y la aventura, se ha diseñado un programa en el que se disfruta de una aventura increíble a velocidad conduciendo una moto acuática con snorkel incluido para ver la vida marina.', 'Maroma Adventure takes you to explore the jungle riding an ATV and, before the adrenaline cools off, challenge the Caribbean''s waves in front of Maroma Beach. Considered by Travel Discovery Channel as one of the most beautiful beaches in the world.', 'Medio Día', '', '20.72364519873989', '-86.97599172592163', '', '', '2014-12-10 04:59:23', '2015-01-31 08:25:02'),
(20, 6, 1, 'PESCA DEPORTIVA', 'DEEP SEA FISHING', 'Disfruta de la mejor pesca deportiva a bordo de nuestras embarcaciones con expertos capitanes. contacto reservations@abaexcursiones.com', 'Enjoy the best sport fishing with expert captains,either shared or private boats, contact reservations@abaexcursiones.com', 'Medio Día', '', '20.83470813610605', '-86.88715696334839', '', 'si', '2014-12-10 05:00:13', '2015-05-14 21:09:07'),
(21, 6, 2, 'Yucatan Explorer (incluye snorkel en Akumal)', 'Yucatan Explorer (include snorkel in Akumal)', 'Or if you prefer to set your hook in the BIG one, you can try your hand at some of the best sport fishing in the area on our Fishing Explorer Tour. ', 'Or if you prefer to set your hook in the BIG one, you can try your hand at some of the best sport fishing in the area on our Fishing Explorer Tour. ', 'Medio Día', '', '21.100996554028114', '-86.74555778503418', '', '', '2014-12-10 05:01:23', '2014-12-10 21:27:54'),
(22, 5, 1, 'Nado con Delfines "ROYAL SWIM"', 'Dolphins Swim "ROYAL SWIM"', 'Descubre el programa mas avanzado con delfines donde la adrenalina es la principal atracción. Disfruta de un paseo a toda velocidad mientras te jalan con su aleta dorsal y después de impulsan de la planta de tus pies para atravesar el agua en el famoso “foothpush”\r\n\r\nActividades :Levantamiento de la planta de los pies,Paseo dorsal, Beso y abrazo, Saludo de mano, Tiempo libre\r\n', 'Discover the most advanced program swiming with dolphins where adrenaline is the main attraction. Enjoy a speed ride while you are pulled by their dorsal fin, later the famous "foothpush"\r\nActivities :Footpush ,Dorsal tow, kiss and hug, Hand Shake, Hand target, Free time\r\n', 'Medio Día', '', '20.727378046151752', '-86.96663618087769', '', '', '2015-02-02 16:10:21', '2015-02-03 06:37:07'),
(23, 5, 1, 'Nado con Delfines "ENCUENTRO"', 'Dolphins Swim "ENCOUNTER"', 'Este programa ha sido diseñado para pequeños e incluso mujeres embarazadas o personas que solo quieren vivir la experciencia de tocar y estar cerca de un Delfin, permaneciendo exclusivamente en la plataforma sin necesidad de sumergirte en el agua pero estando en contacto directo con uno de los mamíferos mas amigables del mundo realizando actividades como: el beso, abrazo, saludo y tiempo libre', 'This program is designed for children and even pregnant women, or people who just want to live the experience of touching and beeing near to a Dolphin, remaining only on the platform without having to dive into the water but being in direct contact with one of the most incredible mammals in the world performing activities such as kissing, hugging, and greeting.', 'Medio Día', '', '21.14315031954876', '-86.77437543869019', '', '', '2015-02-03 04:35:08', '2015-02-03 04:53:24'),
(24, 5, 1, 'Nado con Delfines "SWIM ADVENTURE"', 'Dolphins Swim "SWIM ADVENTURE"', 'Programa intermedio donde podrás interactuar con don el delfin mas allá de una plataforma dejándote empujar sobre una tabla (boogie board) mientras te canta, te besa y te lleva a pasear en una aventura extraordinaria con actividades como:Bellyride Boogiepush Hand target,Saludo de mano,Beso y abrazo, Tiempo libre', 'Intermediate program where you can interact with the dolphin being pushed from the platform boogie board while he sings to you, kisses and take a ride on an extraordinary adventure with activities like: Bellyride Boogiepush target Hand, Hand shake , kiss and hug.', 'Medio Día', '', '20.724126861641103', '-86.97440385818481', '', '', '2015-02-03 04:50:15', '2015-02-03 04:54:06'),
(25, 5, 1, 'Nado con Delfines "ROYAL SWIM PLUS"', 'Dolphins Swim "ROYAL SWIM PLUS"', 'Si un delfín no es suficiente para ti, atrévete a vivir la experiencia que superará todas tus expectativas! Imagina un día junto a delfines, manatíes o leones marinos.. El programa Sea LifeCircle Manatíes o leones marinos incluye el mismo programa de 60 minutos con los delfines y además 30 minutos de actividad con cualquiera de las otras 2 especies. Estos mamíferos marinos gentiles robarán tu corazón mientras aprendes sobre su dieta, cuidados y anatomía. Dejate conquistar por estos adorables animales.', 'If a dolphin is not enough for you, dare to live the experience that will exceed all your expectations! Imagine a day with dolphins, manatees and sea lions .. The Sea Life Circle Manatees, or sea lions program includes the same 60-minute program with dolphins and also 30 minutes of activity with either of the other two species. These gentle marine mammals will steal your heart away while learning about their diet, cares and anatomy. Let yourself be conquered by these adorable animals.', 'Todo el Día', '', '20.502481062986686', '-87.21949338912964', '', '', '2015-02-03 05:04:28', '2015-05-14 20:42:57'),
(26, 7, 1, 'GRUPOS, BANQUETES Y CONGRESOS', 'GROUPS & CONVENTIONS', 'Atencion y servicio a grupos mayores informes al ++52(998)285 89 95 o envianos un correo a reservations@abaexcursiones.com', 'Service to groups and events please contact 52(998)285 89 95 or send us an email to reservations@abaexcursiones.com', 'Medio Día', '', '21.161681372089326', '-86.85192346572876', 'si', 'si', '2015-04-21 20:53:56', '2015-04-21 21:02:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservaciones`
--

CREATE TABLE IF NOT EXISTS `reservaciones` (
  `id` int(10) unsigned NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `correo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `comentario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `divisa` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `pq_nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `pq_descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pq_duracion` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `pq_latitud` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pq_longitud` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pq_tipo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pq_nino` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pq_adulto` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pq_fecha` date NOT NULL,
  `pq_tarifa_nino` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pq_tarifa_adulto` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `linkreporte` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `payment_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `payer_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `reservaciones`
--

INSERT INTO `reservaciones` (`id`, `nombre`, `apellidos`, `correo`, `telefono`, `comentario`, `divisa`, `pq_nombre`, `pq_descripcion`, `pq_duracion`, `pq_latitud`, `pq_longitud`, `pq_tipo`, `pq_nino`, `pq_adulto`, `pq_fecha`, `pq_tarifa_nino`, `pq_tarifa_adulto`, `linkreporte`, `payment_id`, `payer_id`, `status`, `created_at`, `updated_at`) VALUES
(7, 'Nicolás', 'Navarrete C.', 'nicosli@gmail.com', '9981734337', 'Esta prueba es para pagar en live', '', 'Prueba PayPal Live', 'Este es una prueba real ', 'no aplica', 'no aplica', 'no aplica', 'pago especial', '0', '1', '2015-02-24', '0', '4', 'IQQ4TV', 'PAY-3FJ12862HU728815JKTPA2XQ', 'Z7T3RD49MPCUJ', 'approved', '2015-02-13 20:33:15', '2015-02-13 20:43:49'),
(8, 'carlos ', 'test', 'night_ek@hotmail.com', '9885858585', 'TEST', '', 'PAGO TEST', 'TEST PAGOS ESPECIALES', 'no aplica', 'no aplica', 'no aplica', 'pago especial', '0', '1', '2015-02-13', '0', '1', '6HQA7F', '', '', 'nuevo', '2015-02-14 01:24:11', '2015-02-14 01:24:11'),
(9, 'JOE ', 'COLEMAN', 'night_ek@hotmail.com', '9988460026', 'test pago', 'MXN', 'CANNOPY EXTREME ', 'En un perfecto medio día! Iremos al siguiente nivel EXTREMO! Cuando alto no es  suficientemente y rápido tampoco es lo suficientemente, ya nada es lo mismo así entonces prepárate para experimentar ', 'Medio Día', '20.85363845335004', '-86.92981481552124', 'Aventuras Extremas', '0', '1', '2015-02-13', '140', '140', 'M5RYZF', '', '', 'nuevo', '2015-02-14 01:30:49', '2015-02-14 01:30:49'),
(13, 'TEST', 'FINAL', 'nicosli@gmail.com', '9981734337', 'TEST', 'USD', 'Coba Mayan Village', 'Coba is considered the oldest and the largest City dated from the classic period of the Mayan Empire. The tour will guide you through the 4 most important highlights of Coba; The Church, Its important Drawings and Paintings, The Ball Court, and Nohoch Mul', 'All day', '20.490783301612627', '-87.73280918598175', 'Mayan Archaeological Sites and Villages', '1', '1', '2015-02-13', '119.00', '89.00', 'X12F4L', '', '', 'cancelpayment', '2015-02-14 03:56:47', '2015-02-14 03:57:35'),
(14, 'werwer', 'ewrwer', 'werwer@werwer.cvom', '145789102', 'qweqweewqwe', 'MXN', 'Rio Secreto', 'En una aventura de medio día, nadar en Río Secreto, es una experiencia sensorial extraordinaria e inolvidable y muy bien podría ser una de las grandes maravillas del mundo. Venga y disfrute de este río subterráneo espectacular con miles de antiguas estruc', 'Medio Día', '20.72930464106847', '-87.00534582138062', 'Cenotes y Cavernas', '1', '1', '2015-04-14', '1386', '693', 'K4CJ8F', '', '', 'nuevo', '2015-04-14 02:06:25', '2015-04-14 02:06:25'),
(15, 'joe', 'coleman', 'night_ek@hotmail.com', '9988998899', 'test', 'MXN', 'Mayan Adventure Snorkeling Tour', 'Ven y únete a nosotros en una maravillosa experiencia en la selva y el mar, descubriendo en un día tres extraordinarios ecosistemas en esta zona del mundo maya, la exótica vida marina en una hermosa caleta con aguas cristalinas que desemboca en el océano;', 'Medio Día', '20.73299721279593', '-87.01549530029297', 'Playas Vírgenes Manglares y Arrecifes', '0', '1', '2015-05-07', '1246', '966', 'KBPF8F', '', '', 'nuevo', '2015-05-07 06:06:46', '2015-05-07 06:06:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temporadas`
--

CREATE TABLE IF NOT EXISTS `temporadas` (
  `id` int(10) unsigned NOT NULL,
  `paquetes_id` int(11) NOT NULL,
  `fhInicio` date NOT NULL,
  `fhFin` date NOT NULL,
  `precio_adulto` decimal(10,2) NOT NULL,
  `precio_nino` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `temporadas`
--

INSERT INTO `temporadas` (`id`, `paquetes_id`, `fhInicio`, `fhFin`, `precio_adulto`, `precio_nino`, `created_at`, `updated_at`) VALUES
(1, 2, '2014-06-11', '2014-12-31', '89.00', '75.00', '2014-06-11 23:53:47', '2014-06-11 23:53:47'),
(3, 3, '2014-06-12', '2014-06-30', '90.00', '88.00', '2014-06-12 21:11:49', '2014-06-12 21:11:49'),
(4, 7, '2014-01-01', '2014-12-31', '109.00', '50.00', '2014-07-15 22:03:43', '2014-07-15 22:03:43'),
(5, 8, '2014-07-15', '2014-12-31', '119.00', '89.00', '2014-07-15 22:11:12', '2014-07-15 22:11:12'),
(7, 10, '2014-07-15', '2014-12-31', '129.00', '99.00', '2014-07-15 22:25:08', '2014-07-15 22:25:08'),
(8, 11, '2014-07-15', '2014-12-31', '136.00', '106.00', '2014-07-15 22:29:28', '2014-07-15 22:29:28'),
(9, 21, '2014-12-09', '2014-12-31', '125.00', '110.00', '2014-12-10 05:02:43', '2014-12-10 05:02:43'),
(10, 20, '2014-12-09', '2014-12-31', '85.00', '70.00', '2014-12-10 05:02:56', '2014-12-10 05:02:56'),
(11, 19, '2014-12-09', '2014-12-31', '98.00', '98.00', '2014-12-10 05:03:10', '2014-12-10 05:03:10'),
(12, 18, '2014-12-09', '2014-12-31', '125.00', '120.00', '2014-12-10 05:03:24', '2014-12-10 05:03:24'),
(13, 17, '2014-12-09', '2014-12-31', '145.00', '120.00', '2014-12-10 05:03:35', '2014-12-10 05:03:35'),
(14, 16, '2014-12-09', '2014-12-31', '111.00', '102.00', '2014-12-10 05:03:52', '2014-12-10 05:03:52'),
(15, 15, '2014-12-09', '2014-12-31', '120.00', '110.00', '2014-12-10 05:04:13', '2014-12-10 05:04:13'),
(16, 14, '2014-12-09', '2014-12-31', '75.00', '70.00', '2014-12-10 05:04:26', '2014-12-10 05:04:26'),
(17, 13, '2014-12-09', '2014-12-31', '99.00', '90.00', '2014-12-10 05:04:40', '2014-12-10 05:04:40'),
(18, 12, '2014-12-09', '2014-12-31', '110.00', '115.00', '2014-12-10 05:04:51', '2014-12-10 05:04:51'),
(22, 2, '2015-01-01', '2015-12-31', '69.00', '49.00', '2014-12-27 00:20:25', '2014-12-27 00:20:25'),
(23, 3, '2015-01-01', '2015-12-31', '119.00', '89.00', '2014-12-27 01:26:45', '2014-12-27 01:26:45'),
(24, 3, '2014-12-26', '2014-12-31', '129.00', '99.00', '2014-12-27 01:28:37', '2014-12-27 01:28:37'),
(25, 6, '2014-12-27', '2015-12-31', '69.00', '59.00', '2014-12-27 23:36:29', '2014-12-27 23:36:29'),
(26, 8, '2015-01-01', '2015-12-31', '89.00', '69.00', '2014-12-28 00:53:34', '2014-12-28 00:53:34'),
(27, 1, '2015-01-01', '2015-12-31', '125.00', '98.00', '2015-02-03 05:18:54', '2015-02-03 05:18:54'),
(28, 4, '2015-02-01', '2015-02-28', '139.00', '100.00', '2015-02-12 02:50:30', '2015-02-12 02:50:30'),
(29, 16, '2015-02-12', '2015-02-16', '10.00', '10.00', '2015-02-12 18:41:42', '2015-02-12 18:41:42'),
(30, 7, '2015-01-01', '2015-12-31', '99.00', '49.50', '2015-03-24 06:30:07', '2015-03-24 06:30:07'),
(31, 12, '2015-01-01', '2015-12-31', '75.00', '55.00', '2015-03-26 22:00:15', '2015-03-26 22:00:15'),
(32, 5, '2015-01-01', '2015-12-31', '69.00', '45.00', '2015-03-26 22:32:18', '2015-03-26 22:32:18'),
(33, 9, '2015-01-01', '2015-12-31', '116.00', '89.00', '2015-03-26 22:33:15', '2015-03-26 22:33:15'),
(34, 4, '2015-01-01', '2015-12-31', '149.00', '119.00', '2015-03-26 22:55:25', '2015-03-26 22:55:25'),
(35, 10, '2015-05-14', '2015-12-31', '122.00', '61.00', '2015-05-14 20:10:07', '2015-05-14 20:10:07'),
(36, 11, '2015-05-14', '2015-12-31', '75.00', '38.00', '2015-05-14 20:14:29', '2015-05-14 20:14:29'),
(37, 13, '2015-05-14', '2015-07-04', '129.00', '65.00', '2015-05-14 20:19:19', '2015-05-14 20:19:19'),
(38, 13, '2015-07-05', '2015-08-15', '149.00', '75.00', '2015-05-14 20:21:30', '2015-05-14 20:21:30'),
(39, 13, '2015-08-16', '2015-12-24', '129.00', '65.00', '2015-05-14 20:22:09', '2015-05-14 20:22:09'),
(40, 14, '2015-05-14', '2015-07-05', '129.00', '65.00', '2015-05-14 20:24:22', '2015-05-14 20:24:22'),
(41, 14, '2015-07-06', '2015-08-15', '139.00', '70.00', '2015-05-14 20:25:43', '2015-05-14 20:25:43'),
(42, 14, '2015-08-16', '2015-12-24', '129.00', '65.00', '2015-05-14 20:26:28', '2015-05-14 20:26:28'),
(43, 15, '2015-05-14', '2015-07-05', '149.00', '75.00', '2015-05-14 20:27:59', '2015-05-14 20:27:59'),
(44, 15, '2015-07-06', '2015-08-15', '169.00', '85.00', '2015-05-14 20:28:44', '2015-05-14 20:28:44'),
(45, 15, '2015-08-16', '2015-12-24', '149.00', '75.00', '2015-05-14 20:29:12', '2015-05-14 20:29:12'),
(46, 16, '2015-05-14', '2015-12-31', '98.00', '80.00', '2015-05-14 20:32:09', '2015-05-14 20:32:09'),
(47, 18, '2015-05-14', '2015-12-31', '125.00', '98.00', '2015-05-14 20:34:35', '2015-05-14 20:34:35'),
(48, 19, '2015-05-14', '2015-12-31', '55.00', '55.00', '2015-05-14 20:37:07', '2015-05-14 20:37:07'),
(49, 23, '2015-05-14', '2015-12-31', '99.00', '89.00', '2015-05-14 20:39:04', '2015-05-14 20:39:04'),
(50, 24, '2015-05-14', '2015-12-31', '129.00', '89.00', '2015-05-14 20:40:32', '2015-05-14 20:40:32'),
(51, 22, '2015-05-14', '2015-12-31', '159.00', '89.00', '2015-05-14 20:41:49', '2015-05-14 20:41:49'),
(52, 25, '2015-05-14', '2015-12-31', '189.00', '99.00', '2015-05-14 20:43:37', '2015-05-14 20:43:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoCambio`
--

CREATE TABLE IF NOT EXISTS `tipoCambio` (
  `id` int(11) NOT NULL,
  `valor` decimal(10,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoCambio`
--

INSERT INTO `tipoCambio` (`id`, `valor`) VALUES
(1, '14.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE IF NOT EXISTS `tipos` (
  `id` int(10) unsigned NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_en` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`id`, `nombre`, `nombre_en`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Zonas Arqueológicas y Aldeas Mayas', 'Mayan Archaeological Sites and Villages', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Cenotes y Cavernas', 'Cenotes and Caverns', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Playas Vírgenes Manglares y Arrecifes', 'Virgin Beaches Coral reefs & Mangroves', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Parques Temáticos', 'Amusement Parks', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Aventuras Extremas', 'Extreme Adventures', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Pesca', 'Fishing', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Eventos y Grupos', 'Groups & Events', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `alias`, `nombre`, `apellido`, `created_at`, `updated_at`) VALUES
(1, 'jcoleman@abaexcursiones.com', '$2y$08$r9Gzi5QrtBkoDcZPkVZy9uJx/mUKtoHzY4Gn3CUnOMd7G6vu2GM3O', 'jcoleman', 'Joseph', 'Coleman', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'nicosli@nicosli.com', '$2y$08$r9Gzi5QrtBkoDcZPkVZy9uJx/mUKtoHzY4Gn3CUnOMd7G6vu2GM3O', 'nicosliX', 'Nicolas', 'Navarrete', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contiene`
--
ALTER TABLE `contiene`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paquetes`
--
ALTER TABLE `paquetes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reservaciones`
--
ALTER TABLE `reservaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `temporadas`
--
ALTER TABLE `temporadas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipoCambio`
--
ALTER TABLE `tipoCambio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contiene`
--
ALTER TABLE `contiene`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `paquetes`
--
ALTER TABLE `paquetes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `reservaciones`
--
ALTER TABLE `reservaciones`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `temporadas`
--
ALTER TABLE `temporadas`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT de la tabla `tipoCambio`
--
ALTER TABLE `tipoCambio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tipos`
--
ALTER TABLE `tipos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

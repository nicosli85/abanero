<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('frontend.inicio');
});

Route::get('/terminos', function()
{
	return View::make('frontend.terminos');
});

/* ----------- PARA CAMBIAR IDIOMA ------------ */
Route::get('/changeLang/{idioma}', function($idioma){
	Session::put('locale', $idioma);
	return Redirect::back()->with('message','Operation Successful ! $idioma');
});

/*
|--------------------------------------------------------------------------
| Mostrar List View de los tours
|--------------------------------------------------------------------------
*/
Route::get('/category/{categoria}/{id}', array("uses" => "ListviewController@lista"));
Route::get('/categoria/{categoria}/{id}', array("uses" => "ListviewController@lista"));
Route::post('/ajax/galeria/{id}', array("uses" => "PaquetesController@galeria"));

/*
|--------------------------------------------------------------------------
| Cotizacion GET formulario
|--------------------------------------------------------------------------
*/
Route::match(array("GET", "POST"), '/category/{categoria}/{id}/{fecha}/{adultos}/{ninos}', array("uses" => "ListviewController@listaForm"));

/*
|--------------------------------------------------------------------------
| Módulo Detalles
|--------------------------------------------------------------------------
*/
Route::get('/detailed/{categoria}/{id}/{fecha}/{adultos}/{ninos}', array( 'uses' => 'DetallesController@show' ));

/*
|--------------------------------------------------------------------------
| Módulo Datos
|--------------------------------------------------------------------------
*/
Route::get('/book/{paquete}/{id}/{fecha}/{adultos}/{ninos}', array( 'uses' => 'BookController@formulario' ));

/*
|--------------------------------------------------------------------------
| Crear Reservacion
|--------------------------------------------------------------------------
*/
Route::post('/view/order/{linkreporte}', array('uses' => 'BookController@setReserva'));
Route::get('/view/order/{linkreporte}', array('uses' => 'BookController@verOrder'));

/*
|--------------------------------------------------------------------------
| PAYPAL Integración
|--------------------------------------------------------------------------
*/
Route::post('/genera/liga/paypal/{linkreporte}', array('uses' => 'PaypalPaymentController@create'));
Route::get('/payment/{status}/{linkreporte}', array('uses' => 'PaypalPaymentController@executePayment'));
Route::get('/view/details/paypal/{payment_id}', array('uses' => 'PaypalPaymentController@show'));

/*
|--------------------------------------------------------------------------
| CONEKTA Integración
|--------------------------------------------------------------------------
*/
Route::get('/pay/order/{linkreporte}', array('uses' => 'ConektaController@payview'));
Route::post('/pay/order/{linkreporte}', array('uses' => 'ConektaController@makePay'));
Route::get('/pay/orderOxxo/{linkreporte}', array('uses' => 'ConektaController@payviewOxxo'));

/*
|--------------------------------------------------------------------------
| Contacto
|--------------------------------------------------------------------------
*/
Route::get('/contacto', function(){
	return View::make('frontend.contacto');
});

Route::get('/email', function(){
	return View::make('emails.comment');
});

Route::post('/contacto', function(){
	$datos_email['correo_remitente'] = Input::get('email');
	$datos_email['correo_electronico'] = "reservations@abaexcursiones.com";
	$datos_email['nombre'] = "Josep Coleman";

	$datos = array(
		"correo" => Input::get('email'),
		"telefono" => Input::get('phone'),
		"comentario" => Input::get('comment')
	);

	Mail::send('emails.comment', $datos, function($msg) use($datos_email){
	   $msg->from($datos_email['correo_remitente']);
	   $asunto='Nuevo mensaje de Página ABA';
	   $msg->to($datos_email['correo_electronico'], $datos_email['nombre'] )->subject($asunto);
	});

	return View::make('frontend.contacto')->with("mensaje", "ok");
});
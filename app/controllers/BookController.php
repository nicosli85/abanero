<?php 
class BookController extends BaseController{

	public function formulario($paquete, $id, $fecha, $adultos, $ninos){
		$nombrePaquete = $paquete;
		$paquete = Paquetes::getbyid($id);
		$similarListing = Paquetes::getSimilarListing($id);
		$incluye = Paquetes::incluye($id);
		$tipo = Paquetes::getNombreTipo_byIdPaquete($id);
		return View::make('frontend.book', array(
			'paquete' => $paquete,
			'fecha' => $fecha,
			'adultos' => $adultos,
			'ninos' => $ninos,
			'incluye' => $incluye,
			'similarListing' => $similarListing,
			'tipo' => $tipo,
			'nombrePaquete' => $nombrePaquete
		));
	}

	public function setReserva($linkreporte){
		$respuesta = Reservaciones::agregarReserva(Input::all());

		if( $respuesta['error'] == true ){
			return Redirect::to(Input::get('requestUrl'))->withErrors($respuesta['mensaje']);
		}else{
			$reserva = Reservaciones::getOrder($linkreporte);
			return View::make('frontend.viewOrder', array(
				'reserva' => $reserva,
				'linkreporte' => $linkreporte,
				'status' => 'nuevo'
			));
		}
	}

	public function verOrder($linkreporte){
		$reserva = Reservaciones::getOrder($linkreporte);
		if(empty($reserva))
			$reserva = "invalido";
		return View::make('frontend.viewOrder', array(
			'reserva' => $reserva,
			'linkreporte' => $linkreporte,
			'status' => $reserva[0]->status
		));
		
	}

	public function payment($status, $linkreporte){

		
	}
}
?>
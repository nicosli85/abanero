<?php 
class PaypalPaymentController extends BaseController {

    /**
     * object to authenticate the call.
     * @param object $_apiContext
     */
    private $_apiContext;

    /**
     * Set the ClientId and the ClientSecret.
     * @param 
     *string $_ClientId
     *string $_ClientSecret
     */
    private $_ClientId='AX9dmxCqZpD3Q_JzDXCk0vjj5bdjbBGMHNgfYqq7ZTUTFpSzitHgokSDn-rj';
    private $_ClientSecret='EGTmzxAPv2Y6VNPTolk__FWGU2f0sdR0q5_xIj63EIvrlHIGfm5jVwwHmbxU';

    /*
     *   These construct set the SDK configuration dynamiclly, 
     *   If you want to pick your configuration from the sdk_config.ini file
     *   make sure to update you configuration there then grape the credentials using this code :
     *   $this->_cred= Paypalpayment::OAuthTokenCredential();
    */
    public function __construct()
    {
        // ### Api Context
        // Pass in a `ApiContext` object to authenticate 
        // the call. You can also send a unique request id 
        // (that ensures idempotency). The SDK generates
        // a request id if you do not pass one explicitly. 

        /*$this->_apiContext = Paypalpayment:: ApiContext(
            Paypalpayment::OAuthTokenCredential(
                $this->_ClientId,
                $this->_ClientSecret
            )
        );*/

        $this->_apiContext = Paypalpayment::ApiContext($this->_ClientId, $this->_ClientSecret);

        // dynamic configuration instead of using sdk_config.ini

        $this->_apiContext->setConfig(array(
            'mode' => 'live',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => __DIR__.'/../PayPal.log',
            'log.LogLevel' => 'FINE'
        ));

    }

      /*
     * Create payment using credit card
     * url:payment/create
    */
    public function create($linkreporte)
    {
        $paquete = Reservaciones::getOrder($linkreporte);

        $payer = Paypalpayment::Payer();
        $payer->setPayment_method("paypal");

        if($paquete[0]->pq_adulto > 0){
            $item1 = Paypalpayment::Item();
            $item1->setName($paquete[0]->pq_nombre)
            ->setDescription(Lang::get('tarifaAdulto'))
            ->setCurrency(Util::getDivisa())
            ->setQuantity($paquete[0]->pq_adulto)
            ->setPrice($paquete[0]->pq_tarifa_adulto)
            ->setSku("ADULT-".$linkreporte);
        }
        
        if($paquete[0]->pq_nino > 0){
            $item2 = Paypalpayment::Item();
            $item2->setName($paquete[0]->pq_nombre)
                ->setDescription(Lang::get('tarifaNino'))
                ->setCurrency(Util::getDivisa())
                ->setQuantity($paquete[0]->pq_nino)
                ->setPrice($paquete[0]->pq_tarifa_nino)
                ->setSku("NINO-".$linkreporte);
        }

        $itemList = Paypalpayment::ItemList();
        if($paquete[0]->pq_nino > 0)
            $itemList->setItems(array($item1, $item2));
        else
            $itemList->setItems(array($item1));

        $total = ($paquete[0]->pq_adulto * $paquete[0]->pq_tarifa_adulto) + ($paquete[0]->pq_nino * $paquete[0]->pq_tarifa_nino);

        $amount = Paypalpayment:: Amount();
        $amount->setCurrency(Util::getDivisa());
        $amount->setTotal($total);

        $transaction = Paypalpayment:: Transaction();
        $transaction->setAmount($amount);
        $transaction->setDescription($paquete[0]->pq_nombre);
        $transaction->setItem_list($itemList);

        $baseUrl = Request::root();
        $redirectUrls = Paypalpayment:: RedirectUrls();
        $redirectUrls->setReturn_url($baseUrl.'/payment/confirmpayment/'.$linkreporte);
        $redirectUrls->setCancel_url($baseUrl.'/payment/cancelpayment/'.$linkreporte);

        $payment = Paypalpayment:: Payment();
        $payment->setIntent("sale");
        $payment->setPayer($payer);
        $payment->setRedirectUrls($redirectUrls);
        $payment->setTransactions(array($transaction));

        $response = $payment->create($this->_apiContext);

        //set the trasaction id , make sure $_paymentId var is set within your class
        $this->_paymentId = $response->id;

        //dump the repose data when create the payment
        $redirectUrl = $response->links[1]->href;

        //this is will take you to complete your payment on paypal
        //when you confirm your payment it will redirect you back to the rturned url set above
        //inmycase sitename/payment/confirmpayment this will execute the getConfirmpayment function bellow
        //the return url will content a PayerID var
        //return Redirect::to( $redirectUrl );

        return View::make('frontend.ligaPaypal', array(
            'liga' => $redirectUrl
        ));

    }  

    public function executePayment($status, $linkreporte){
        
        if($status == "confirmpayment"){
            // Get the payment Object by passing paymentId
            // payment id and payer ID was previously stored in database in
            // create() fuction , this function create payment using "paypal" method
            $paymentId = Input::get('paymentId');
            $PayerID = Input::get('PayerID');
            $payment = Paypalpayment::get($paymentId, $this->_apiContext);

            // PaymentExecution object includes information necessary 
            // to execute a PayPal account payment. 
            // The payer_id is added to the request query parameters
            // when the user is redirected from paypal back to your site
            $execution = Paypalpayment::PaymentExecution();
            $execution->setPayer_id($PayerID);

            //Execute the payment
            $payment->execute($execution,$this->_apiContext);
            Reservaciones::setStatus($payment->state, $linkreporte);
        }else{
            Reservaciones::setStatus($status, $linkreporte);
        }

        Reservaciones::setPaymentId(Input::get('paymentId'), $linkreporte);
        Reservaciones::setPayerId(Input::get('PayerID'), $linkreporte);


        $reserva = Reservaciones::getOrder($linkreporte);

        if(empty($reserva))
            $reserva = "invalido";

        return Redirect::to('/view/order/'.$linkreporte);

    }

    /*
        Use this call to get details about payments that have not completed, 
        such as payments that are created and approved, or if a payment has failed.
        url:payment/PAY-3B7201824D767003LKHZSVOA
    */
    public function show($payment_id)
    {
       $payment = Paypalpayment::get($payment_id,$this->_apiContext);

       return View::make('frontend.detallesPaypal', array('payment' => $payment));
    }
}
?>
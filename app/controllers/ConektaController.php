<?php 
class ConektaController extends BaseController {

	public function payviewOxxo($linkreporte){
		$reserva = Reservaciones::getOrder($linkreporte);
		if(empty($reserva))
			$reserva = "invalido";

		if($reserva[0]->status == 'paid')
			return Redirect::to('/view/order/'.$linkreporte);
		else{
			$total = ($reserva[0]->pq_adulto * $reserva[0]->pq_tarifa_adulto) + ($reserva[0]->pq_nino * $reserva[0]->pq_tarifa_nino);

			$ck = ConektaStore::checaReserva($reserva[0]->id);

			if($ck == false){
				Conekta::setApiKey('key_HryMbGA24shkmaYWPrebgw');
				Conekta::setApiVersion("1.0.0");

				$tarifaNino = $reserva[0]->pq_tarifa_nino;
				$tarifaAdulto = $reserva[0]->pq_tarifa_adulto;

				if($reserva[0]->divisa == 'USD'){
						$total = $total * tipoCambio::get();
						$tarifaNino = $reserva[0]->pq_tarifa_nino * tipoCambio::get();
						$tarifaAdulto = $reserva[0]->pq_tarifa_adulto * tipoCambio::get();
				}

				$item1 = array(
					"name"=>$reserva[0]->pq_nombre,
			        "sku"=>"ADULTO-".$linkreporte,
			        "unit_price"=> $tarifaAdulto."00",
			        "description"=>Lang::get('tarifaAdulto'),
			        "quantity"=> $reserva[0]->pq_adulto,
			        "type"=>"tour"
				);

				$items[] = $item1;

				if($reserva[0]->pq_nino > 0){
					$item2 = array(
						"name"=>$reserva[0]->pq_nombre,
				        "sku"=>"NINO-".$linkreporte,
				        "unit_price"=> $tarifaNino."00",
				        "description"=>Lang::get('tarifaNino'),
				        "quantity"=> $reserva[0]->pq_nino,
				        "type"=>"tour"
					);
					$items[] = $item2;
				}

				try {
					$charge = Conekta_Charge::create(array(
					  'description'=>$reserva[0]->pq_nombre,
					  'reference_id'=>$reserva[0]->linkreporte,
					  'amount'=>$total."00",
					  'currency'=>'MXN',
					   'cash'=> array(
							'type'=>'oxxo'
						),
					  "details"=> array(
					    "email"=>$reserva[0]->correo,
					    "line_items"=> $items
					  )
					));	

					ConektaStore::set($charge->payment_method, $reserva[0]->id);
					$arr = array(
						"barcode_url" => $charge->payment_method->barcode_url,
						"barcode" => $charge->payment_method->barcode,
						"expires_at" => $charge->payment_method->expires_at
					);

				} catch (Exception $e) {
					return Redirect::to('/view/order/'.$linkreporte)->with('errorProceso', $e->getMessage());
				}
			}else{
				$arr = array(
					"barcode_url" => $ck->url,
					"barcode" => $ck->barcode,
					"expires_at" => $ck->expiration
				);
			}
			

			return View::make('frontend.pagoConektaOxxo', array(
				'reserva' => $reserva,
				'linkreporte' => $linkreporte,
				'status' => $reserva[0]->status,
				'total' => $total,
				'conOxxo' => $arr
			));
		}
	}

	public function payview($linkreporte){
		$reserva = Reservaciones::getOrder($linkreporte);
		if(empty($reserva))
			$reserva = "invalido";

		if($reserva[0]->status == 'paid')
			return Redirect::to('/view/order/'.$linkreporte);
		else{
			$total = ($reserva[0]->pq_adulto * $reserva[0]->pq_tarifa_adulto) + ($reserva[0]->pq_nino * $reserva[0]->pq_tarifa_nino);

			if($reserva[0]->divisa == 'USD')
				$total = $total * tipoCambio::get();

			return View::make('frontend.pagoConekta', array(
				'reserva' => $reserva,
				'linkreporte' => $linkreporte,
				'status' => $reserva[0]->status,
				'total' => $total
			));
		}
	}

	public function makePay($linkreporte){
		Conekta::setApiKey('key_HryMbGA24shkmaYWPrebgw');
		Conekta::setApiVersion("1.0.0");

		$reserva = Reservaciones::getOrder($linkreporte);
		$total = ($reserva[0]->pq_adulto * $reserva[0]->pq_tarifa_adulto) + ($reserva[0]->pq_nino * $reserva[0]->pq_tarifa_nino);
		$tarifaNino = $reserva[0]->pq_tarifa_nino;
		$tarifaAdulto = $reserva[0]->pq_tarifa_adulto;

		if($reserva[0]->divisa == 'USD'){
				$total = $total * tipoCambio::get();
				$tarifaNino = $reserva[0]->pq_tarifa_nino * tipoCambio::get();
				$tarifaAdulto = $reserva[0]->pq_tarifa_adulto * tipoCambio::get();
		}

		$item1 = array(
			"name"=>$reserva[0]->pq_nombre,
	        "sku"=>"ADULTO-".$linkreporte,
	        "unit_price"=> $tarifaAdulto."00",
	        "description"=>Lang::get('tarifaAdulto'),
	        "quantity"=> $reserva[0]->pq_adulto,
	        "type"=>"tour"
		);

		$items[] = $item1;

		if($reserva[0]->pq_nino > 0){
			$item2 = array(
				"name"=>$reserva[0]->pq_nombre,
		        "sku"=>"NINO-".$linkreporte,
		        "unit_price"=> $tarifaNino."00",
		        "description"=>Lang::get('tarifaNino'),
		        "quantity"=> $reserva[0]->pq_nino,
		        "type"=>"tour"
			);
			$items[] = $item2;
		}

		try {
			$charge = Conekta_Charge::create(array(
			  'description'=>$reserva[0]->pq_nombre,
			  'reference_id'=>$reserva[0]->linkreporte,
			  'amount'=>$total."00",
			  'currency'=>'MXN',
			  'card'=>Input::get('conektaTokenId'),
			  "details"=> array(
			    "email"=>$reserva[0]->correo,
			    "line_items"=> $items
			  )
			));	

			if($charge->status == 'paid'){
				Reservaciones::setStatus($charge->status, $linkreporte);
				Reservaciones::setPaymentId($charge->id, $linkreporte);
				return Redirect::to('/view/order/'.$linkreporte);
			}

		} catch (Exception $e) {
			return Redirect::to('/pay/order/'.$linkreporte)->with('errorProceso', $e->getMessage());
		}

		

	}

} 
?>
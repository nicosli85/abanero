<?php 
class ListviewController extends BaseController{
	
	public function lista($categoria, $id){

		$tipo = Tipos::getbyid($id);
		$paquetes = Paquetes::getbyTiposid($id);
		$fecha = date("Y-m-d");
		$adultos = "1";
		$ninos = "0";
		
		return View::make('frontend.listview', array(
			'id_tipo' => $id,
			'tipo' => $tipo,
			'paquetes' => $paquetes,
			'fecha' => $fecha,
			'adultos' => $adultos,
			'ninos' => $ninos
		));
	}

	public function listaForm($categoria, $id, $fecha, $adultos, $ninos){

		$tipo = Tipos::getbyid($id);
		$paquetes = Paquetes::getbyTiposid($id);
		return View::make('frontend.listview', array(
			'id_tipo' => $id,
			'tipo' => $tipo,
			'paquetes' => $paquetes,
			'fecha' => $fecha,
			'adultos' => $adultos,
			'ninos' => $ninos
		));
	}

}
?>
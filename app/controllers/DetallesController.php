<?php 
class DetallesController extends BaseController{

	public function show($categoria, $id, $fecha, $adultos, $ninos){
		$paquete = Paquetes::getbyid($id);
		$similarListing = Paquetes::getSimilarListing($id);
		$incluye = Paquetes::incluye($id);

		return View::make('frontend.detalles', array(
			'paquete' => $paquete,
			'adultos' => $adultos,
			'ninos' => $ninos,
			'fecha' => $fecha,
			'similarListing' => $similarListing,
			'incluye' => $incluye,
			'categoria' => $categoria
		));
	}

}
?>
<?php 
class Paquetes extends Eloquent{

	protected $table = 'paquetes';

	public static function getbyTiposid($id){
		$len = App::getLocale();
		$len = ($len == 'en')?  '_'.$len : '';

		$paquetes = DB::table('paquetes')->select(
			'paquetes.id',
			'paquetes.nombre'. $len . ' as nombre',
			'paquetes.descripcion'. $len . ' as descripcion',
			'paquetes.duracion'. $len .' as duracion'
		)->where('tipos_id', '=', $id)->where('estados_id', '=', '1')->get();

		return $paquetes;
	}

	public static function getbyid($id){
		$len = App::getLocale();
		$len = ($len == 'en')?  '_'.$len : '';

		$paquete = DB::table('paquetes')->select(
			'paquetes.id',
			'paquetes.nombre'. $len . ' as nombre',
			'paquetes.descripcion'. $len . ' as descripcion',
			'paquetes.duracion'. $len .' as duracion',
			'longitud', 
			'latitud'
		)->where('id', '=', $id)->where('estados_id', '=', '1')->get();

		return $paquete;
	}

	public static function getSimilarListing($id_paquete){
		$len = App::getLocale();
		$len = ($len == 'en')?  '_'.$len : '';
		$id_tipo = DB::table('paquetes')->select('id', 'tipos_id')->where('id', '=', $id_paquete)->get();
		$id_tipo = $id_tipo[0]->tipos_id;
		$list = DB::table('paquetes')->select(
			'id',
			'nombre'. $len . ' as nombre'
		)->where('tipos_id', '=', $id_tipo)->where('id', '!=', $id_paquete)->get();

		return $list;
	}

	public static function incluye($id){
		$incluye = DB::table('contiene')->where('paquete_id', '=', $id)->get();
		$len = App::getLocale();
		foreach ($incluye as $key => $val) {
			if(strpos($val->descripcion, "/")){
				$valores = explode("/", $val->descripcion);
				$res = ($len == 'en')? $valores[0] : $valores[1];
				$val->descripcion = $res;				
			}
		}

		return $incluye;
	}

	public static function getNombreTipo_byIdPaquete($id){
		$len = App::getLocale();
		$len = ($len == 'en')?  '_'.$len : '';
		$id_tipo = DB::table('paquetes')->select('id', 'tipos_id')->where('id', '=', $id)->get();
		$id_tipo = $id_tipo[0]->tipos_id;
		$tipo = DB::table('tipos')->select('nombre'.$len.' as nombre')->where('id', '=', $id_tipo)->get();

		return $tipo[0]->nombre;
	}
}
?>
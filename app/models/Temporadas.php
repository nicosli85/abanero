<?php 
class Temporadas extends Eloquent{
	protected $table = "temporadas";

	public static function getTarifaAdulto($fecha, $id_paquete){

		$local = Config::get('app.locale');

		$tarifa = temporadas::where('paquetes_id', '=', $id_paquete)->
        where('fhInicio', '<=', $fecha)->
        where('fhFin', '>=', $fecha)->get();

        $tarifa = ($tarifa->isEmpty() )? 'offsale' : $tarifa[0]->precio_adulto;
        $tarifa = ($local == 'es')? $tarifa * tipoCambio::get() : $tarifa;        
		
		return $tarifa;
	}

	public static function getTarifaNino($fecha, $id_paquete){

		$local = Config::get('app.locale');

		$tarifa = temporadas::where('paquetes_id', '=', $id_paquete)->
        where('fhInicio', '<=', $fecha)->
        where('fhFin', '>=', $fecha)->get();

        $tarifa = ($tarifa->isEmpty() )? 'offsale' : $tarifa[0]->precio_nino;
        $tarifa = ($local == 'es')? $tarifa * tipoCambio::get() : $tarifa;        
		
		return $tarifa;
	}
}
?>
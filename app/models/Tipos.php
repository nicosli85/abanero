<?php 
class Tipos extends Eloquent{
	protected $table = 'tipos';

	public static function lista(){
		$len = App::getLocale();
		$len = ($len == 'en')?  '_'.$len : '';
		$tipos = DB::table('tipos')->select('tipos.nombre' . $len . ' as nombre', 'tipos.id')->get();
		return $tipos;
	}

	public static function getbyid($id){
		$len = App::getLocale();
		$len = ($len == 'en')?  '_'.$len : '';
		$tipo = DB::table('tipos')->select('tipos.nombre'. $len . ' as nombre')->where('id', '=', $id)->get();
		return $tipo;
	}
}
?>
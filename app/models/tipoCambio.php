<?php 
class tipoCambio extends Eloquent{
	protected $table = "tipoCambio";
	protected $fillable = array('valor');
	public $timestamps = false;

	public static function get(){
		$model = tipoCambio::first();
		return $model->valor;
	}
}
?>
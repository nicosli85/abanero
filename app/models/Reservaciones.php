<?php 
class Reservaciones extends Eloquent{
	protected $table = 'reservaciones';

	protected $fillable = array(
		"nombre",
		"apellidos",
		"correo",
		"telefono",
		"comentario",
		"divisa",
		"pq_nombre",
		"pq_descripcion",
		"pq_duracion",
		"pq_latitud",
		"pq_longitud",
		"pq_tipo",
		"pq_nino",
		"pq_adulto",
		"pq_fecha",
		"pq_tarifa_nino",
		"pq_tarifa_adulto",
		"linkreporte",
		"payment_id",
		"payer_id",
		"status"
	);

	public static function agregarReserva($input){

		$respuesta = array();

		$reglas = array(
			"nombre" => "required",
			"apellidos" => "required",
			"correo" => "required",
			"telefono" => array('required','numeric'),
			"divisa" => "required",
			"pq_nombre" => "required",
			"pq_descripcion" => "required",
			"pq_duracion" => "required",
			"pq_latitud" => "required",
			"pq_longitud" => "required",
			"pq_tipo" => "required",
			"pq_nino" => "required",
			"pq_adulto" => "required",
			"pq_fecha" => "required",
			"pq_tarifa_nino" => array('required', 'numeric'),
			"pq_tarifa_adulto" => array('required', 'numeric'),
			"linkreporte" => "required"
		);

		$validator = Validator::make($input, $reglas);

		if ($validator->fails()){
            
            $respuesta['mensaje'] = $validator;
            $respuesta['error']   = true;

        }else{

            $reservacion = static::create($input);
            $respuesta['mensaje'] = 'exito';
            $respuesta['error']   = false;
            $respuesta['data']    = $reservacion;
        }

        return $respuesta; 

	}

	public static function getOrder($linkreporte){

		$reserva = DB::table('reservaciones')->where('linkreporte', '=', $linkreporte)->get();

		return $reserva;
	}

	public static function setPaymentId($payment_id, $linkreporte){

		$affectedRows = Reservaciones::where('linkreporte', '=', $linkreporte)->update(array('payment_id' => $payment_id));

		return $affectedRows;

	}

	public static function setStatus($status, $linkreporte){
		$affectedRows = Reservaciones::where('linkreporte', '=', $linkreporte)->update(array('status' => $status));
	}

	public static function setPayerId($payer_id, $linkreporte){
		$affectedRows = Reservaciones::where('linkreporte', '=', $linkreporte)->update(array('payer_id' => $payer_id));
	}

}
?>
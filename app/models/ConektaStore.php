<?php 
class ConektaStore extends Eloquent{

	protected $table = 'conekta_store';

	public static function checaReserva($id_reserva){
		try {
			$ob = ConektaStore::where("id_reserva", "=", $id_reserva)->firstOrFail();
			return $ob;
		} catch (Exception $e) {
			return false;
		}
	}

	public static function set($charge, $id_reserva){
		ConektaStore::insert(
			array(
				"id_reserva" 	=> $id_reserva,
				"store"			=> "oxxo",
				"barcode"		=> $charge->barcode,
				"expiration"	=> $charge->expires_at,
				"url"			=> $charge->barcode_url
			)
		);	
	}

}
?>
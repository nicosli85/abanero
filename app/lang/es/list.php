<?php

return array(

	/*
	|------------------------------------------------------------------------------
	| Etiquetas - ESPAÑOL
	|------------------------------------------------------------------------------
	| El siguiente lenguaje es usado para el Menu Principal
	|
	*/
	'recomendado' 	=> 'recomendado',
	'poradulto' 	=> 'por / adulto',
	'seleccionar' 	=> 'Seleccionar',
	'vendido'		=> 'agotado',
	'cerrar'		=> 'cerrar',
	'tarifaAdulto'	=> 'Rate per adult',
	'tarifaNino'	=> 'Rate per kid'
);
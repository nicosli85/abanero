<?php

return array(

	/*
	|------------------------------------------------------------------------------
	| Etiquetas - ESPAÑOL
	|------------------------------------------------------------------------------
	| El siguiente lenguaje es usado para el Menu Principal
	|
	*/
	'fotos' 		=> 'Galería',
	'mapa' 			=> 'Mapa',
	'descripcion'	=> 'Descripción',
	'incluye'		=> 'Incluye',
	'noIncluye'		=> 'No incluye',
	'buscar'		=> 'Buscar de nuevo',
	'btnBook'		=> 'RESERVAR TOUR',
	'ayudaTit'		=> 'Necesita ayuda?',
	'ayudaText'		=> 'Estamos contentos de ayudarle, nuestro equipo de asistencia técnica estan a su servicio',
	'lista'			=> 'Tours Similares'
);
<?php

return array(

	/*
	|------------------------------------------------------------------------------
	| Etiquetas - ESPAÑOL
	|------------------------------------------------------------------------------
	| El siguiente lenguaje es usado para el Menu Principal
	|
	*/
	'titulo' => 'Atracciones y Excursiones',
	'eslogan' => 'Brindamos una experiencia moderna y confortable',

	/*-------------------- BANNER EN MEDIO ------------*/
	'banTitulo' 	=> '¿Cómo trabaja ABA?',
	'paso1' 		=> 'Explore destinos',
	'paso1sub'		=> 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
	'paso2' 		=> 'Cheque tarifas',
	'paso2sub'		=> 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
	'paso3' 		=> 'Reserve en linea',
	'paso3sub'		=> 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
	'paso4' 		=> 'Disfrute su tour',
	'paso4sub'		=> 'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
);
<?php

return array(

	/*
	|------------------------------------------------------------------------------
	| Etiquetas - ESPAÑOL
	|------------------------------------------------------------------------------
	| El siguiente lenguaje es usado para el Menu Principal
	|
	*/
	'inicio' 		=> 'Inicio',
	'excursiones' 	=> 'Excursiones',
	'contacto'		=> 'Contacto',

	/*------------ BREADCUMS -----*/
	'breadInicio' 	=> 'inicio',
	'breadResult' 	=> 'Tour Resultados',
	'breadDetalles'		=> 'Detalles',
	'breadBook'		=> 'Reservar',
	'breadOrden'	=> 'Ver Orden'
);
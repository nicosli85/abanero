<?php

return array(

	/*
	|------------------------------------------------------------------------------
	| Etiquetas - ESPAÑOL
	|------------------------------------------------------------------------------
	| El siguiente lenguaje es usado para el Menu Principal
	|
	*/
	'titulo' 				=> 'Estado de la reserva',
	'verOrden'				=> 'Ver Orden',
	'gracias'				=> 'Gracias. Su reserva ha sido procesada',
	'correoText'			=> 'Se ha enviado los detalles a su correo electrónico proporcionado',
	'pagoTitulo'			=> 'Pago',
	'pagoText'				=> 'Para garantizar la reserva debe de realizar el pago',
	'boton'					=> 'Pagar $ :total :divisa con PayPal',
	'travelInformation'		=> 'Información Personal',
	'travelId'				=> 'Reserva Id',
	'travelNombre'			=> 'Nombre',
	'travelApellido'		=> 'Apellido',
	'travelCorreo'			=> 'Correo electrónico',
	'tourInformation'		=> 'Información del Tour',
	'tourNombre'			=> 'Nombre',
	'tourFecha'				=> 'Fecha',
	'tourAdultos'			=> 'Adultos',
	'tourNinos'				=> 'Niños',
	'linkOrden'				=> 'Link orden',
	'pagoEstado'			=> 'Pago :estado',
	'exitoText'				=> 'La transaccion fue exitosa',
	'btnDetalles'			=> 'VER DETALLES',
	'botonCredit'			=> 'Pagar $ :total :divisa con tarjeta de crédito',
	'botonOxxo'				=> 'Pagar $ :total :divisa con OXXO'
);
<?php

return array(

	/*
	|------------------------------------------------------------------------------
	| Etiquetas - INGLES
	|------------------------------------------------------------------------------
	| El siguiente lenguaje es usado para el Menu Principal
	|
	*/
	'titulo'	=> 'Información Personal',
	'nombre'	=> 'Nombre',
	'apellido'	=> 'Apellido',
	'correo'	=> 'Dirección de correo electrónico',
	'telefono'	=> 'Número de teléfono',
	'checkbox'	=> 'Quiero recibir <span class="skin-color">ABA</span> promociones en un futuro',
	'btn'		=> 'CONFIRMAR RESERVA',
	'comentario'=> 'Comentario'
);
<?php

return array(

	/*
	|------------------------------------------------------------------------------
	| Etiquetas - ESPAÑOL
	|------------------------------------------------------------------------------
	| El siguiente lenguaje es usado para el Menu Principal
	|
	*/
	'titulo' => 'Dónde?',
	'categoria' => 'Seleccione categoría',
	'fecha' => 'fecha tour',
	'adultos' => 'adultos',
	'ninos' => 'niños',
	'boton' => 'BUSCAR',
	'donde' => 'Dónde',
	'cuando' => 'Cuando',
	'quien' => 'No. de personas',
	'titCont' => 'Envíanos por favor tus dudas, sugerencias o requerimientos',
	'_con-email' => 'Correo electrónico',
	'_con-enteremail' => 'Introduzca correo',
	'_con-telefono'	=> 'Teléfono',
	'_con-comentario' => 'Comentario',
	'_con-enviar'	=> 'Enviar'
);
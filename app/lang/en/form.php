<?php

return array(

	/*
	|------------------------------------------------------------------------------
	| Etiquetas - INGLES
	|------------------------------------------------------------------------------
	| El siguiente lenguaje es usado para el Menu Principal
	|
	*/
	'titulo' => 'Where',
	'categoria' => 'Select category',
	'fecha' => 'Check In',
	'adultos' => 'adults',
	'ninos' => 'Kids',
	'boton' => 'SEARCH NOW',
	'donde' => 'Where',
	'cuando' => 'When',
	'quien' => 'Number of persons',
	'titCont' => 'Please send us your questions, suggestions or requirements',
	'_con-email' => 'Email address',
	'_con-enteremail' => 'Enter email',
	'_con-telefono'	=> 'Phone',
	'_con-comentario' => 'Comment',
	'_con-enviar'	=> 'Send'
);
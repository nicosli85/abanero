<?php

return array(

	/*
	|------------------------------------------------------------------------------
	| Etiquetas - INGLES
	|------------------------------------------------------------------------------
	| El siguiente lenguaje es usado para el Menu Principal
	|
	*/
	'inicio' 		=> 'Home',
	'excursiones' 	=> 'Excursions',
	'contacto'		=> 'Contact',

	/*------------ BREADCUMS -----*/
	'breadInicio' 	=> 'Home',
	'breadResult' 	=> 'Tour Search Result',
	'breadDetalles'	=> 'Detailed',
	'breadBook'		=> 'Book',
	'breadOrden'	=> 'View Order'
);
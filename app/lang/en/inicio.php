<?php

return array(

	/*
	|------------------------------------------------------------------------------
	| Etiquetas - ESPAÑOL
	|------------------------------------------------------------------------------
	| El siguiente lenguaje es usado para el Menu Principal
	|
	*/
	'titulo' 		=> 'Attractions and Excursions',
	'eslogan' 		=> 'We\'re bringing you a modern and comfortable tour experience',

	/*-------------------- BANNER EN MEDIO ------------*/
	'banTitulo' 	=> 'How ABA Works?',
	'paso1' 		=> 'Explore Destinations',
	'paso1sub'		=> 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
	'paso2' 		=> 'Check Rates',
	'paso2sub'		=> 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
	'paso3' 		=> 'Book Online',
	'paso3sub'		=> 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
	'paso4' 		=> 'Get Ready to enjoy',
	'paso4sub'		=> 'Lorem Ipsum is simply dummy text of the printing and typesetting industry'

	
);
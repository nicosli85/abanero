<?php

return array(

	/*
	|------------------------------------------------------------------------------
	| Etiquetas - INGLES
	|------------------------------------------------------------------------------
	| El siguiente lenguaje es usado para el Menu Principal
	|
	*/
	'recomendado' 	=> 'Recomended',
	'poradulto' 	=> 'per / adult',
	'seleccionar' 	=> 'Select',
	'vendido'		=> 'SOLD',
	'cerrar'		=> 'close',
	'tarifaAdulto'	=> 'Tarifa por adulto',
	'tarifaNino'	=> 'Tarifa por niño'
);
<?php

return array(

	/*
	|------------------------------------------------------------------------------
	| Etiquetas - ESPAÑOL
	|------------------------------------------------------------------------------
	| El siguiente lenguaje es usado para el Menu Principal
	|
	*/
	'titulo' 				=> 'Bookin Status',
	'verOrden'				=> 'View Order',
	'gracias'				=> 'Thank You. Your Booking Order is procesed.',
	'correoText'			=> 'A email has been sent to your provided email address.',
	'pagoTitulo'			=> 'Payment',
	'pagoText'				=> 'To guarantee the reservation must make payment',
	'boton'					=> 'Pay $ :total :divisa with PayPal',
	'travelInformation'		=> 'Traveler Information',
	'travelId'				=> 'Booking Id',
	'travelNombre'			=> 'First Name',
	'travelApellido'		=> 'Last Name',
	'travelCorreo'			=> 'E-mail Address',
	'tourInformation'		=> 'Tour Information',
	'tourNombre'			=> 'Tour Name',
	'tourFecha'				=> 'Chech In',
	'tourAdultos'			=> 'Adults',
	'tourNinos'				=> 'Kids',
	'linkOrden'				=> 'Link Order',
	'pagoEstado'			=> 'Payment :estado',
	'exitoText'				=> 'the transaction was successful',
	'btnDetalles'			=> 'VIEW DETAILS',
	'botonCredit'			=> 'Pay $ :total :divisa with Credit Card',
	'botonOxxo'				=> 'Pay $ :total :divisa with OXXO'
);
<?php

return array(

	/*
	|------------------------------------------------------------------------------
	| Etiquetas - ESPAÑOL
	|------------------------------------------------------------------------------
	| El siguiente lenguaje es usado para el Menu Principal
	|
	*/
	'fotos' 		=> 'Photos',
	'mapa' 			=> 'Map',
	'descripcion'	=> 'Description',
	'incluye'		=> 'Includes',
	'noIncluye'		=> 'No Include',
	'buscar'		=> 'Search Again',
	'btnBook'		=> 'BOOK TOUR',
	'ayudaTit'		=> 'Need Help?',
	'ayudaText'		=> 'We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.',
	'lista'			=> 'Similar Listing'

);
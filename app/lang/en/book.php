<?php

return array(

	/*
	|------------------------------------------------------------------------------
	| Etiquetas - INGLES
	|------------------------------------------------------------------------------
	| El siguiente lenguaje es usado para el Menu Principal
	|
	*/
	'titulo'	=> 'Your Personal Information',
	'nombre'	=> 'First Name',
	'apellido'	=> 'Last Name',
	'correo'	=> 'Email Address',
	'telefono'	=> 'Phone Number',
	'checkbox'	=> 'I want to receive <span class="skin-color">ABA</span> promotional offers in the future',
	'btn'		=> 'CONFIRM BOOKING',
	'comentario'=> 'Comment'
);
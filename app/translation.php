<?php
/*
|--------------------------------------------------------------------------
| Translations
|--------------------------------------------------------------------------
|
| This script gets the locale of user and load the appropriate file 
| Este script checa en una variable de sesion llamada locale
| si tiene guardada un idioma, de lo contrario guarda el idioma por default
|
*/

// Establecemos los idiomas permitidos

$localesAllowed = array('es', 'en');

define('DEFAULT_LANG', 'en');

if (!Session::has('locale'))
    Session::put('locale', DEFAULT_LANG);   

App::setLocale(Session::get('locale'));
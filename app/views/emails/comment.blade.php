    <!-- 
 * Template Name: Unify - Responsive Bootstrap Template
 * Description: Business, Corporate, Portfolio and Blog Theme.
 * Version: 1.6
 * Author: @htmlstream
 * Website: http://htmlstream.com
 -->
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Responsive Email Template</title>
                                                                                                                                                                                                                                                                                                                                                                                                        
<style type="text/css">
    .ReadMsgBody {width: 100%; background-color: #ffffff;}
    .ExternalClass {width: 100%; background-color: #ffffff;}
    body     {width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Arial, Helvetica, sans-serif}
    table {border-collapse: collapse;}
    
    @media only screen and (max-width: 640px)  {
                    body[yahoo] .deviceWidth {width:440px!important; padding:0;}    
                    body[yahoo] .center {text-align: center!important;}  
            }
            
    @media only screen and (max-width: 479px) {
                    body[yahoo] .deviceWidth {width:280px!important; padding:0;}    
                    body[yahoo] .center {text-align: center!important;}  
            }
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="font-family: Arial, Helvetica, sans-serif">

<!-- Wrapper -->
<table width="100%"  border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
        <td width="100%" valign="top">
            
            <!--Start Header-->
            <table width="700" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                <tr>
                    <td style="padding: 6px 0px 0px">
                        <table width="650" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                            <tr>
                                <td width="100%" >
                                    <!--Start logo-->
                                    <table  border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
                                        <tr>
                                            <td class="center" style="padding: 20px 0px 10px 0px">
                                                <a href="#"><img src="https://abaexcursiones.com/images/logo.png"></a>
                                            </td>
                                        </tr>
                                    </table><!--End logo-->
                                </td>
                            </tr>
                        </table>
                   </td>
                </tr>
            </table> 
            <!--End Header-->

            <!--Start Bottom Block-->
            <table width="100%" bgcolor="#e67e22" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                <tr>
                    <td>
                         <table width="700" bgcolor="#e67e22" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                            <tr>
                                <td>
                                    <!-- Left box  -->
                                    <table width="40%"  border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
                                        <tr>
                                            <td class="center">
                                                <table  border="0" cellpadding="0" cellspacing="0" align="center"> 
                                                    <tr>
                                                        <td  class="center" style="font-size: 16px; color: #ffffff; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 20px; vertical-align: middle; padding: 50px 10px 0; ">
                                                             Comentario del cliente:
                                                       </td>
                                                    </tr>
                                                    <tr>
                                                        <td  class="center" style="font-size: 12px; color: #ffffff; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; " >
                                                            @if(isset($comentario))
                                                            {{$comentario}}
                                                            @endif
                                                       </td>
                                                    </tr>
                                                     <tr>
                                                        <td  class="center" style="font-size: 12px; color: #ffffff; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; " >
                                                            <strong>Teléfono</strong>
                                                            @if(isset($telefono))
                                                                {{$telefono}}
                                                            @endif
                                                       </td>
                                                    </tr>
                                                    <tr>
                                                        <td  class="center" style="font-size: 12px; color: #ffffff; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; " >
                                                            <strong>Correo</strong>
                                                            @if(isset($correo))
                                                                {{$correo}}
                                                            @endif
                                                       </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table> <!--End left box--> 
                                    <!--Right box-->
                                    <table width="49%"  border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                                        
                                    </table><!--End right box-->
                                </td>
                            </tr>
                        </table>
                        <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                            <tr>
                                <td width="100%" bgcolor="#e67e22">
                                    <!--Right Box-->
                                    <table width="40%"  border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
                                        <tr>
                                            <td style=" padding: 20px;">
                                                <table  align="center">
                                                    <tr>
                                                        <td  valign="top" style="padding: 7px 15px; text-align: center; background-color: #3498db;" class="center">
                                                            <a style="color: #fff; font-size: 12px; font-weight: bold; text-decoration: none; font-family: Arial, sans-serif; text-alight: center;" href="http://admin.abaexcursiones.com/">ENTRAR AL SISTEMA</a>
                                                        </td>                                   
                                                     </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table><!--End Right Box-->
                                    <!-- Left Box  -->
                                    <table width="60%"  border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
                                        <tr>
                                            <td class="left">
                                                <table  border="0" cellpadding="0" cellspacing="0" align="left"> 
                                                    <tr>
                                                        <td  class="center" style="font-size: 16px; color: #ffffff; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 20px; vertical-align: right; padding: 25px 20px 40px;">
                                                            Sistema de administración de ABA Excursiones
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table><!--End Left Box-->
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>                
            </table>
            <!--End Bottom Block  -->  
            
            <!-- Footer -->
            <table width="700" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                <tr>
                    <td>
                        <table width="700"  border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                            <tr>
                                <td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 20px; vertical-align: middle; padding: 30px 10px 0px; " >
                                     Este correo se envió desde la página web de abaexcursiones.com
                                </td>
                            </tr>
                              <tr>
                                <td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 20px; vertical-align: middle; padding: 10px 10px 0px; " >
                                     
                                </td>
                            </tr>
                            <tr>
                                <td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 20px; vertical-align: middle; padding: 10px 50px 30px; " >
                                    Copyright © ABA Excursiones
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--End Footer-->

            <div style="height:15px">&nbsp;</div><!-- divider -->
          
        </td>
    </tr>
</table> 
<!-- End Wrapper -->
</body>
</html>
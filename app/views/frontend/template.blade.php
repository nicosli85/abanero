<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html> <!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>Aba excursiones</title>
    
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo | Responsive HTML5 Travel Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Theme Styles -->
    {{HTML::style('/css/bootstrap.min.css')}}
    {{HTML::style('/css/font-awesome.min.css')}}
    {{HTML::style('/css/animate.min.css')}}
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    
    <!-- Current Page Styles -->
    {{HTML::style('/components/revolution_slider/css/settings.css')}}
    {{HTML::style('/components/revolution_slider/css/style.css')}}
    {{HTML::style('/components/jquery.bxslider/jquery.bxslider.css')}}
    {{HTML::style('/components/flexslider/flexslider.css')}}
    
    <!-- Main Style -->
    {{HTML::style('/css/style.css')}}
    
    <!-- Custom Styles -->
    {{HTML::style('/css/custom.css')}}

    <!-- Updated Styles -->
    {{HTML::style('/css/updates.css')}}
    
    <!-- Responsive Styles -->
    {{HTML::style('/css/responsive.css')}}
    
    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
    <style>
        section#content {  min-height: 1000px; padding: 0; position: relative; overflow: hidden; }
        #main { padding-top: 20px; }
        .page-title, .page-description { color: #fff; }
        .page-title { font-size: 3.6em; font-weight: bold; }
        .page-description { font-size: 2.5em; margin-bottom: 50px; }
        .featured { position: absolute; right: 50px; bottom: 50px; z-index: 9; margin-bottom: 0;  text-align: right; }
        .featured figure a { border: 2px solid #fff; }
        .featured .details { margin-right: 10px; }
        .featured .details > * { color: #fff; line-height: 1.25em; margin: 0; font-weight: bold; text-shadow: 2px -2px rgba(0, 0, 0, 0.2); }
    </style>
    @yield('scriptHead')
</head>
<body>
    <input type="hidden" id="rootPath" value="{{Request::root()}}">
    <div id="page-wrapper">
        <header id="header" class="navbar-static-top">
            <div class="topnav hidden-xs">
                <div class="container">
                    <ul class="quick-menu pull-left">
                        <!--<li><a href="#">MY ACCOUNT</a></li>-->
                        <li class="ribbon">
                            <a href="#">{{(App::getLocale() == 'en')? 'Español': 'English'}}</a>
                            <ul class="menu mini">
                                <li class="{{(App::getLocale() == 'en')? 'active': ''}}">
                                    <a href="{{asset('/changeLang/en')}}" title="English">English</a>
                                </li>
                                <li class="{{(App::getLocale() == 'es')? 'active': ''}}">
                                    <a href="{{asset('/changeLang/es')}}" title="Español">Español</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="quick-menu pull-right">
                        <!--<li><a href="#travelo-login" class="soap-popupbox">LOGIN</a></li>
                        <li><a href="#travelo-signup" class="soap-popupbox">SIGNUP</a></li>
                        <li class="ribbon currency">
                            <a href="#" title="">USD</a>
                            <ul class="menu mini">
                                <li class="active"><a href="#" title="USD">Dolares USD</a></li>
                                <li><a href="#" title="MXN">Pesos MXN</a></li>
                            </ul>
                        </li>-->
                    </ul>
                </div>
            </div>
            
            <div class="main-header">
                
                <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
                    Mobile Menu Toggle
                </a>

                <div class="container">
                    <h1 class="logo navbar-brand">
                        <a href="{{asset('/')}}" title="Aba Excursiones - home">
                            <img src="{{asset('images/logo.png')}}" alt="Aba Excursiones" />
                        </a>
                    </h1>
                    
                    <nav id="main-menu" role="navigation">
                        <ul class="menu">
                            <li class="menu-item">
                                <a href="{{asset('/')}}">{{Lang::get('temp.inicio')}}</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="{{asset('/')}}">{{Lang::get('temp.excursiones')}}</a>
                                <ul>
                                    @foreach (Tipos::lista() as $key => $val) 
                                        <li><a href="{{asset('/category/'.Funciones::generaURL($val->nombre).'/'.$val->id)}}">
                                        {{$val->nombre}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="menu-item">
                                <a href="{{asset('/contacto')}}">{{Lang::get('temp.contacto')}}</a>
                            </li>
                            <li class="menu-item">
                                <a href="{{asset('/terminos')}}">Términos y condiciones</a>
                            </li>
                            <li class="menu-item">
                                @if((App::getLocale() == 'en'))
                                    <a href="{{asset('/changeLang/es')}}" title="Español"><i class="soap-icon-globe"></i> Español</a>
                                @endif
                                @if((App::getLocale() == 'es'))
                                    <a href="{{asset('/changeLang/en')}}" title="Español"><i class="soap-icon-globe"></i> English</a>
                                @endif
                            </li>
                            
                        </ul>
                    </nav>
                </div>
                
                <nav id="mobile-menu-01" class="mobile-menu collapse">
                    <ul id="mobile-primary-menu" class="menu">
                        <li class="menu-item">
                            <a href="{{asset('/')}}">{{Lang::get('temp.inicio')}}</a>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="#">{{Lang::get('temp.excursiones')}}</a>
                            <ul>
                                @foreach (Tipos::lista() as $key => $val) 
                                    <li><a href="{{asset('/category/'.Funciones::generaURL($val->nombre).'/'.$val->id)}}">
                                    {{$val->nombre}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="menu-item">
                            <a href="{{asset('/contacto')}}">{{Lang::get('temp.contacto')}}</a>
                        </li>
                    </ul>
                    
                    <ul class="mobile-topnav container">
                        <!--<li><a href="#">MY ACCOUNT</a></li>-->
                        <li class="ribbon language menu-color-skin">
                            <a href="#" data-toggle="collapse">{{(App::getLocale() == 'en')? 'Español': 'English'}}</a>
                            <ul class="menu mini">
                                <li class="{{(App::getLocale() == 'en')? 'active': ''}}">
                                    <a href="{{asset('/changeLang/en')}}" title="English">English</a>
                                </li>
                                <li class="{{(App::getLocale() == 'es')? 'active': ''}}">
                                    <a href="{{asset('/changeLang/es')}}" title="Español">Español</a>
                                </li>
                            </ul>
                        </li>
                        <!--<li><a href="#travelo-login" class="soap-popupbox">LOGIN</a></li>
                        <li><a href="#travelo-signup" class="soap-popupbox">SIGNUP</a></li>-->
                        <!--<li class="ribbon currency menu-color-skin">
                            <a href="#">USD</a>
                            <ul class="menu mini">
                                <li class="active"><a href="#" title="USD">USD</a></li>
                                <li><a href="#" title="MXN">MXN</a></li>
                            </ul>
                        </li>-->
                    </ul>
                    
                </nav>
            </div>
            <div id="travelo-signup" class="travelo-signup-box travelo-box">
                <div class="login-social">
                    <a href="#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator"><label>OR</label></div>
                <div class="simple-signup">
                    <div class="text-center signup-email-section">
                        <a href="#" class="signup-email"><i class="soap-icon-letter"></i>Sign up with Email</a>
                    </div>
                    <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund olicy, and Host Guarantee Terms.</p>
                </div>
                <div class="email-signup">
                    <form>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="first name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="last name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="email address">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="password">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="confirm password">
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Tell me about Travelo news
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund Policy, and Host Guarantee Terms.</p>
                        </div>
                        <button type="submit" class="full-width btn-medium">SIGNUP</button>
                    </form>
                </div>
                <div class="seperator"></div>
                <p>Already a Travelo member? <a href="#travelo-login" class="goto-login soap-popupbox">Login</a></p>
            </div>

            <div id="travelo-login" class="travelo-login-box travelo-box">
                <div class="login-social">
                    <a href="#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator"><label>OR</label></div>
                <form>
                    <div class="form-group">
                        <input type="text" class="input-text full-width" placeholder="email address">
                    </div>
                    <div class="form-group">
                        <input type="password" class="input-text full-width" placeholder="password">
                    </div>
                    <div class="form-group">
                        <a href="#" class="forgot-password pull-right">Forgot password?</a>
                        <div class="checkbox checkbox-inline">
                            <label>
                                <input type="checkbox"> Remember me
                            </label>
                        </div>
                    </div>
                </form>
                <div class="seperator"></div>
                <p>Don't have an account? <a href="#travelo-signup" class="goto-signup soap-popupbox">Sign up</a></p>
            </div>
        </header>
        
        @yield('contenido')
        
        <footer id="footer">
            <div class="bottom">
                <div class="container">
                    <div class="logo pull-left">
                        <a href="{{asset('/')}}" title="Travelo - home">
                            <img src="{{asset('images/logo.png')}}" alt="" />
                        </a>
                    </div>
                    <div class="pull-right">
                        <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
                    </div>
                    <div class="copyright pull-right">
                        <p>&copy; 2014 ABA excursiones</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- Style Custom Modulos -->
    @yield('style')
    
    <!-- Javascript -->
    {{HTML::script('js/jquery-1.11.1.min.js')}}
    {{HTML::script('js/jquery.noconflict.js')}}
    {{HTML::script('js/modernizr.2.7.1.min.js')}}
    {{HTML::script('js/jquery-migrate-1.2.1.min.js')}}
    
    {{HTML::script('js/jquery-ui.1.10.4.min.js')}}
    
    <!-- Twitter Bootstrap -->
    {{HTML::script('js/bootstrap.js')}}
    
    <!-- Flex Slider -->
    {{HTML::script('components/flexslider/jquery.flexslider-min.js')}}
    
    <!-- load BXSlider scripts -->
    {{HTML::script('components/jquery.bxslider/jquery.bxslider.min.js')}}
    
    <!-- parallax -->
    {{HTML::script('js/jquery.stellar.min.js')}}
    
    <!-- waypoint -->
    {{HTML::script('js/waypoints.min.js')}}

    <!-- load page Javascript -->
    {{HTML::script('js/theme-scripts.js')}}
    {{HTML::script('js/scripts.js')}}

    @yield('script')
    
    <script type="text/javascript">
        tjq(".flexslider").flexslider({
            animation: "fade",
            controlNav: false,
            animationLoop: true,
            directionNav: false,
            slideshow: true,
            slideshowSpeed: 5000
        });
    </script>
</body>
</html>
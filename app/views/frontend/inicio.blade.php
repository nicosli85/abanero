@extends('frontend.template')
@stop

@section('contenido')
<section id="content" class="slideshow-bg">
    <div id="slideshow">
        <div class="flexslider">
            <ul class="slides">
                <li>
                    <div class="slidebg" style="background-image: url(images/homepage10_bg2.jpg);"></div>
                </li>
                <li>
                    <div class="slidebg" style="background-image: url(images/homepage10_bg3.jpg);"></div>
                </li>
                <li>
                    <div class="slidebg" style="background-image: url(images/homepage10_bg4.jpg);"></div>
                </li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div id="main">
            
            <div class="row">
                <div class="col-md-6">
                    <img src="{{asset('images/logoSliderInicio.png')}}" alt="">
                    <h1 class="page-title">{{Lang::get('inicio.titulo')}}</h1>
                    <h2 class="page-description no-float no-padding">{{Lang::get('inicio.eslogan')}}</h2>
                </div>
                <div class="col-md-6">
                    <div class="youtubeFrameCont">
                        <iframe width="100%" height="280" src="//www.youtube.com/embed/teHXTMx48i8?rel=0&amp;autoplay=0" frameborder="0" allowfullscreen=""></iframe>  
                    </div>                  
                </div>
            </div>
            
            <div class="search-box-wrapper style2">
                <div class="search-box">
                    <ul class="search-tabs clearfix ">
                        <li class="active"><a href="#hotels-tab" data-toggle="tab"><i class="soap-icon-beach"></i><span>TOURS</span></a></li>
                    </ul>
                    <div class="visible-mobile">
                        <ul id="mobile-search-tabs" class="search-tabs clearfix">
                            <li class="active"><a href="#hotels-tab">TOURS</a></li>
                        </ul>
                    </div>
                    
                    <div class="search-tab-content">
                        <div class="tab-pane fade active in" id="hotels-tab">
                            <form action="#" method="post" id="cotizadorForm">
                                <h4 class="title">{{Lang::get('form.titulo')}}</h4>
                                <div class="row">
                                    <div class="form-group col-sm-6 col-md-4">
                                        <div class="selector">
                                            <select class="full-width" id="paqueteSelect" required>
                                                <option value="">{{Lang::get('form.categoria')}}</option>
                                                @foreach (Tipos::lista() as $key => $val) 
                                                    <option value="{{$val->id}}">{{$val->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-3">
                                        <div class="datepicker-wrap">
                                            <input type="text" id="fecha" name="fecha" class="input-text full-width" required placeholder="{{Lang::get('form.fecha')}}" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <div class="selector">
                                                    <select class="full-width" name="adultos" id="adultos" required>
                                                        <option value="">{{Lang::get('form.adultos')}}</option>
                                                        <option value="1">1 Adult</option>
                                                        <option value="2">2 Adults</option>
                                                        <option value="3">3 Adults</option>
                                                        <option value="4">4 Adults</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="selector">
                                                    <select class="full-width" name="ninos" id="ninos">
                                                        <option value="">{{Lang::get('form.ninos')}}</option>
                                                        <option value="0">0 Kid</option>
                                                        <option value="1">1 Kid</option>
                                                        <option value="2">2 Kids</option>
                                                        <option value="3">3 Kids</option>
                                                        <option value="4">4 Kids</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <button type="submit" class="full-width">{{Lang::get('form.boton')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @foreach (Tipos::lista() as $key => $val) 
                                    <input type="hidden" value="{{Funciones::generaURL($val->nombre)}}" id="tipos_id_{{$val->id}}">
                                @endforeach
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="row image-box style4">
                 @foreach (Tipos::lista() as $key => $val) 

                    <div class="col-sm-6 col-md-4">
                        <article class="box">
                            <figure>
                                <a class="hover-effect" title="" href="{{asset('/category/'.Funciones::generaURL($val->nombre).'/'.$val->id)}}">
                                <img width="270" height="160" alt="" src="{{asset('images/cateImages/'.$val->id.'.png')}}"></a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title">{{$val->nombre}}</h4>
                            </div>
                        </article>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
</section>

<div class="global-map-area section parallax" data-stellar-background-ratio="0.5">
    <div class="container description text-center">
        <h1>{{Lang::get('inicio.banTitulo')}}</h1>
        <br>
        <div class="travelo-process">
            <img src="images/travelo_process.png" alt="">
            <div class="process first icon-box style12">
                <div class="details animated fadeInUp" data-animation-type="fadeInUp" data-animation-delay="1" >
                    <h4>{{Lang::get('inicio.paso1')}}</h4>
                    <!--<p class="hidden-xs">{{Lang::get('inicio.paso1sub')}}</p>-->
                </div>
                <div class="icon-wrapper animated slideInLeft" data-animation-type="slideInLeft" data-animation-delay="0" >
                    <i class="soap-icon-beach circle"></i>
                </div>
            </div>
            <div class="process second icon-box style12">
                <div class="icon-wrapper animated slideInRight" data-animation-type="slideInRight" data-animation-delay="1.5" >
                    <i class="soap-icon-availability circle"></i>
                </div>
                <div class="details animated fadeInUp" data-animation-type="fadeInUp" data-animation-delay="2.5" >
                    <h4>{{Lang::get('inicio.paso2')}}</h4>
                    <!--<p class="hidden-xs">{{Lang::get('inicio.paso2sub')}}</p>-->
                </div>
            </div>
            <div class="process third icon-box style12">
                <div class="icon-wrapper animated slideInRight" data-animation-type="slideInRight" data-animation-delay="2" >
                    <i class="soap-icon-stories circle"></i>
                </div>
                <div class="details animated fadeInUp" data-animation-type="fadeInUp" data-animation-delay="3" >
                    <h4>{{Lang::get('inicio.paso3')}}</h4>
                    <!--<p class="hidden-xs">{{Lang::get('inicio.paso3sub')}}</p>-->
                </div>
            </div>
            <div class="process forth icon-box style12">
                <div class="details animated fadeInUp" data-animation-type="fadeInUp" data-animation-delay="4.5" >
                    <h4>{{Lang::get('inicio.paso4')}}</h4>
                    <!--<p class="hidden-xs">{{Lang::get('inicio.paso4sub')}}</p>-->
                </div>
                <div class="icon-wrapper animated slideInLeft" data-animation-type="slideInLeft" data-animation-delay="3.5" >
                    <i class="soap-icon-adventure takeoff-effect1 circle"></i>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('style')
<style>
    .youtubeFrameCont{
        background: #DDD;
        width: 450px;
        margin: 0 auto;
        border: 5px solid #207EB8;
        border-radius: 7px;
        height: 290px;
    }
</style>
@stop

@section('script')
{{HTML::script('js/jquery.validate.min.js')}}

<script>
    tjq().ready(function() {
        tjq("#cotizadorForm").validate({
            rules: {
                paqueteSelect: "required",
                fecha: "required",
                adultos: "required",
                ninos: "required",
            }
        });

        tjq("#cotizadorForm").submit(function(){
            if(tjq("#cotizadorForm").validate().valid()){
                paqueteSelect = tjq("#paqueteSelect").val();
                
                url = tjq("#tipos_id_"+paqueteSelect).val();
                url += "/" + paqueteSelect;
                url += "/" + tjq("#fecha").val();
                url += "/" + tjq("#adultos").val();
                url += "/" + tjq("#ninos").val();

                actual = tjq("#cotizadorForm").attr("action");
                tjq("#cotizadorForm").attr("action", "{{asset('/category')}}/" + url);
            }

        });
    });
</script>
@stop
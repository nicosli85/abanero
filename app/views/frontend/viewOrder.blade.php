@extends('frontend.template')
@stop

@section('contenido')
<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">{{Lang::get('orden.verOrden')}}</h2>
        </div>
        <ul class="breadcrumbs pull-right">
            <li><a href="{{asset('/')}}">{{Lang::get('temp.breadInicio')}}</a></li>
            <li class="active">{{Lang::get('temp.breadOrden')}}</li>
        </ul>
    </div>
</div>
<?php 
    $total = ($reserva[0]->pq_adulto * $reserva[0]->pq_tarifa_adulto) + ($reserva[0]->pq_nino * $reserva[0]->pq_tarifa_nino);
?>
<section id="content" class="gray-area">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="booking-information travelo-box">
                    @if($reserva != 'invalido')
                    
                    <h2>{{Lang::get('orden.titulo')}}</h2>
                    @if($reserva[0]->status == "approved" OR $reserva[0]->status == "paid")
                    <div class="booking-confirmation clearfix">
                        <div class="message">
                            <h2>Confirmed</h2>
                            <p></p>
                        </div>
                    </div>
                    @else
                    <div class="booking-confirmation clearfix">
                        <div class="message">
                            <h4 class="main-message">{{Lang::get('orden.gracias')}}</h4>
                            <p>{{Lang::get('orden.correoText')}}</p>
                        </div>
                    </div>
                    @endif
                    <hr>
                    @if($reserva[0]->status != "approved" AND $reserva[0]->status != "paid")
                    <div class="booking-confirmation clearfix">
                        <i class="soap-icon-recommend icon circle"></i>
                        <div class="message">
                            <h2 class="pay">{{Lang::get('orden.pagoTitulo')}}</h2>
                            <p>{{Lang::get('orden.pagoText')}}</p>
                        </div>
                    </div>
                    <hr>
                    <h3>Métodos de pago</h3>
                    <div class="">
                        <div class="btn-pagos">
                            <img class="payImg" src="{{asset('/images/paypal.png')}}" alt="">
                            <a class="btn-large m-b-16" id="btnPayPal"><i class="soap-icon-settings"></i>
                            {{Lang::get('orden.boton', array('total' => number_format($total,0), 'divisa' => $reserva[0]->divisa))}}</a>
                        </div>
                        <div class="btn-pagos">
                            <img class="payImg" src="{{asset('/images/tarjetas.png')}}" alt="">
                            <a class="btn-large m-b-16" id="btnCard" href="{{asset('/pay/order/'.$linkreporte)}}"><i class="soap-icon-settings"></i>
                            {{Lang::get('orden.botonCredit', array('total' => number_format($total,0), 'divisa' => $reserva[0]->divisa))}}</a>
                        </div>
                        @if(Session::get('errorProceso') != "")
                            <div class="errorCharBack"><strong>{{Session::get('errorProceso')}}</strong></div>
                        @endif
                        <div class="btn-pagos">
                            <img class="payImg" src="{{asset('/images/oxxo.png')}}" alt="">
                            <a class="btn-large m-b-16" id="btnCard" href="{{asset('/pay/orderOxxo/'.$linkreporte)}}"><i class="soap-icon-settings"></i>
                            {{Lang::get('orden.botonOxxo', array('total' => number_format($total,0), 'divisa' => $reserva[0]->divisa))}}</a>
                        </div>
                    </div>
                    @endif

                    @if($reserva[0]->status == "approved" OR $reserva[0]->status == "paid")
                    <div class="sm-section clearfix">
                        <i class="soap-icon-recommend circle white-color skin-bg no-border pull-left" style="font-size: 40px; margin-right: 15px;"></i>
                        <div class="">
                            <div class="pull-left" style="padding: 0 15px;">
                                <h1>{{Lang::get('orden.pagoEstado', array('estado' => "exitoso"))}}</h1>
                                <p>{{Lang::get('orden.exitoText')}}</p>
                            </div>
                            @if($reserva[0]->status == "approved")
                            <div class="pull-right">
                                <a href="#" class="button btn-large green" data-toggle="modal" data-target="#detailsModal" id="btnDetails">{{Lang::get('orden.btnDetalles')}}</a>
                            </div>
                            @endif
                        </div>
                    </div>
                    @endif

                    <hr>
                    <h2>{{Lang::get('orden.travelInformation')}}</h2>
                    <dl class="term-description">
                        <dt>{{Lang::get('orden.travelId')}}:</dt><dd style='color:#C00'><b>{{$reserva[0]->linkreporte}}</b></dd>
                        <dt>{{Lang::get('orden.travelNombre')}}:</dt><dd>{{$reserva[0]->nombre}}</dd>
                        <dt>{{Lang::get('orden.travelApellido')}}:</dt><dd>{{$reserva[0]->apellidos}}</dd>
                        <dt>{{Lang::get('orden.travelCorreo')}}:</dt><dd>{{$reserva[0]->correo}}</dd>
                    </dl>
                    <hr>
                    <h2>{{Lang::get('orden.tourInformation')}}</h2>
                    <dl class="term-description">
                        <dt>{{Lang::get('orden.tourNombre')}}:</dt><dd>{{$reserva[0]->pq_nombre}}</dd>
                        <dt>{{Lang::get('orden.tourFecha')}}:</dt><dd>{{$reserva[0]->pq_fecha}}</dd>
                        <dt>{{Lang::get('orden.tourAdultos')}}:</dt><dd>{{$reserva[0]->pq_adulto}}</dd>
                        @if($reserva[0]->pq_nino > 0)
                        <dt>{{Lang::get('orden.tourNinos')}}:</dt><dd>{{$reserva[0]->pq_nino}}</dd>
                        @endif
                    </dl>
                    <hr>
                    <h2>{{Lang::get('orden.linkOrden')}}</h2>
                    <p>Praesent dolor lectus, rutrum sit amet risus vitae, imperdiet cursus neque. Nulla tempor nec lorem eu suscipit. Donec dignissim lectus a nunc molestie consectetur. Nulla eu urna in nisi adipiscing placerat. Nam vel scelerisque magna. Donec justo urna, posuere ut dictum quis.</p>
                    <br>
                    <a href="{{asset('/view/order/'.$linkreporte)}}" class="red-color underline view-link">{{asset('/view/order/'.$linkreporte)}}</a>
                    @else
                        <h4 style="color:#C00">Error, invalid reservation...</h4>
                    @endif
                </div>
            </div>
            <div class="sidebar col-sm-4 col-md-3">
                <div class="travelo-box contact-box">
                    <h4>{{Lang::get('detalles.ayudaTit')}}</h4>
                    <p>{{Lang::get('detalles.ayudaText')}}</p>
                    <address class="contact-details">
                        <span class="contact-phone"><i class="soap-icon-phone"></i> 1-800-123-HELLO</span>
                        <br>
                        <a class="contact-email" href="#">help@abaexcursiones.com</a>
                    </address>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="opacity-overlay" style="display: none;" id="block"><div class="container"><div class="popup-wrapper"><i class="fa fa-spinner fa-spin spinner"></i><div class="col-xs-12 col-sm-9 popup-content" style="height: auto; visibility: hidden;"></div></div></div></div>
<!-- Modal -->
<div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Payment Details</h4>
      </div>
      <div class="modal-body" id="result">
        Wait please...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@stop

@section('style')
<style>
	section#content {
		padding-top: 40px;
	}
	#main {
		padding-top: 0;
	}
	.pay{
		margin-bottom: 0;
	}
	.payImg{
		  margin-right: 10px;
          display: block;
          margin-bottom: 10px;
	}
    .pay_id{
        color: #C00;
    }
    .btn-pagos{
        margin-bottom: 15px;
    }
    .m-b-16, .m-b-16:hover{
          margin-bottom: 16px;
        background-color: #93DB38;
        background-image: -webkit-linear-gradient(top,#27B4E7,#178CAE);
        background-image: linear-gradient(top,#4d90fe,#4787ed);
        border: 1px solid #0F8FBD;
        padding: 11px 29px;
        color: #FFF;
        font-size: 14px;
        margin-top: 0;
        cursor: pointer;
        display: inline-block;
    }
    .errorCharBack{
        color: #FFF;
          background-color: #E52A2A;
          padding: 8px 18px;
          margin-bottom: 11px;
          font-weight: bold;
    }
</style>
@stop

@section('script')
<script type="text/javascript">
    tjq(document).ready(function(){

        tjq("#btnDetails").click(function(){
            tjq.ajax({
                url: "{{asset('/view/details/paypal/'.$reserva[0]->payment_id)}}",
                success: function(data){
                    tjq("#result").html(data);
                }
            });
        });

        tjq("#btnPayPal").click(function(e){
            tjq("#block").show();
            e.preventDefault();
            tjq.ajax({
                type:"POST",
                url: "{{asset('/genera/liga/paypal/'.$linkreporte)}}",
                success: function(data){
                    if(data != "error")
                        tjq(location).attr('href', data);
                }
            });
            
            return false;
        });
    });
</script>
@stop
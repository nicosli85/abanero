{{Session::put('result', Request::url());}}
@extends('frontend.template')
@stop

@section('contenido')

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&appId=238783612998562&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">{{$tipo[0]->nombre}}</h2>
        </div>
        <ul class="breadcrumbs pull-right">
            <li><a href="{{asset('/')}}">{{Lang::get('temp.breadInicio')}}</a></li>
            <li class="active">{{Lang::get('temp.breadResult')}}</li>
        </ul>
    </div>
</div>

<div class="container">
    <div class="search-tab-content">
        <div class="tab-pane fade active in" id="hotels-tab">
            <form action="#" id="cotizadorForm" method="post">
                <div class="row">
                    <div class="form-group col-sm-6 col-md-3">
                        <h4 class="title">{{Lang::get('form.donde')}}</h4>
                        <label>{{Lang::get('form.titulo')}}</label>
                        <div class="selector">
                            <?php 
                                foreach (Tipos::lista() as $k => $v) {
                                    $ar[$v->id] = $v->nombre;
                                }
                            ?>
                            {{ Form::select('paqueteSelect', $ar, $id_tipo, array('class' => 'full-width', 'required' => 'on', 'id' => 'paqueteSelect')) }}
                            
                        </div>
                    </div>
                    
                    <div class="form-group col-sm-6 col-md-4">
                        <h4 class="title">{{Lang::get('form.cuando')}}</h4>
                        <label>{{Lang::get('form.fecha')}}</label>
                        <div class="datepicker-wrap">
                            <input type="text" id="fecha" name="fecha" class="input-text full-width" placeholder="{{Lang::get('form.fecha')}}" value="{{$fecha}}" >
                            <img class="ui-datepicker-trigger" src="images/icon/blank.png" alt="" title="">
                        </div>
                    </div>
                    
                    <div class="form-group col-sm-6 col-md-3">
                        <h4 class="title">{{Lang::get('form.quien')}}</h4>
                        <div class="row">
                            <div class="col-xs-6">
                                <label>{{Lang::get('form.adultos')}}</label>
                                <div class="selector">
                                    {{ Form::select('adultos', array(
                                        '1' => '01',
                                        '2' => '02',
                                        '3' => '03',
                                        '4' => '04',

                                    ), $adultos, array('class' => 'full-width', 'required' => 'on', 'id' => 'adultos')) }}
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <label>{{Lang::get('form.ninos')}}</label>
                                <div class="selector">
                                    {{ Form::select('ninos', array(
                                        '0' => '0',
                                        '1' => '01',
                                        '2' => '02',
                                        '3' => '03',
                                        '4' => '04',
                                    ), $ninos, array('class' => 'full-width', 'required' => 'on', 'id' => 'ninos')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group col-sm-6 col-md-2 fixheight">
                        <label class="hidden-xs">&nbsp;</label>
                        <button type="submit" class="full-width icon-check" >{{Lang::get('form.boton')}}</button>
                    </div>
                </div>
                @foreach (Tipos::lista() as $key => $val) 
                    <input type="hidden" value="{{Funciones::generaURL($val->nombre)}}" id="tipos_id_{{$val->id}}">
                @endforeach
            </form>
        </div>
    </div>
</div>

<section id="content">
<div class="container">
	<div class="hotel-list listing-style3 hotel">
		@foreach ($paquetes as $key => $val)
		<?php
			$file = asset('/images/shortcodes/gallery-popup/paquete_'.$val->id.'/portada.jpg');
			$file_headers = @get_headers($file);
			if($file_headers[0] == 'HTTP/1.0 404 Not Found' OR $file_headers[0] == 'HTTP/1.1 404 Not Found') {
			    $img = '/images/no-portada.png';
			}
			else {
			    $img = '/images/shortcodes/gallery-popup/paquete_'.$val->id.'/portada.jpg';
			}
			$tarifa = Temporadas::getTarifaAdulto($fecha, $val->id);
            $urlSelect = ($adultos != "")? $adultos : '';
            $urlSelect = ($ninos != "0" OR $ninos != "")? $urlSelect."/".$ninos : '';
		?>
        <article class="box">
            <figure class="col-sm-5 col-md-4">
                <a title="" href="{{asset('/ajax/galeria/'.$val->id)}}" class="hover-effect popup-gallery">
                <img width="270" height="180" alt="" src="{{asset($img)}}"></a>
            </figure>
            <div class="details col-sm-7 col-md-8">
                <div>
                    <div>
                        <h4 class="box-title">{{$val->nombre}}<small><i class="soap-icon-clock yellow-color"></i> {{$val->duracion}}</small></h4>
                        <div class="amenities">
                            <div class="fb-like" data-href="{{asset('/detailed/'.Funciones::generaURL($val->nombre).'/'.$val->id.'/'.$fecha.'/'.$urlSelect)}}" data-layout="button" data-action="recommend" data-show-faces="true" data-share="true"></div>
                        </div>
                    </div>
                    <div>
                        <div class="five-stars-container">
                            <span class="five-stars" style="width: 80%;"></span>
                        </div>
                        <span class="review">{{Lang::get('list.recomendado')}}</span>
                    </div>
                </div>
                <div>
                    <p>{{Funciones::recortarCadena($val->descripcion, 140)}}</p>
                    <div>
                        @if($tarifa > 0) 
                            <span class="price"><small>{{Lang::get('list.poradulto')}}</small>{{number_format($tarifa, 0)}} {{Util::getDivisa()}}</span>
                            <a class="button btn-small full-width text-center" title="" href="{{asset('/detailed/'.Funciones::generaURL($val->nombre).'/'.$val->id.'/'.$fecha.'/'.$urlSelect)}}">{{Lang::get('list.seleccionar')}}</a>
                        @else
                            <span class="price">{{Lang::get('list.vendido')}}</span>
                        @endif
                    </div>
                </div>
            </div>
        </article>
        @endforeach
    </div>
</div>
</section>
@stop

@section('style')
<style>
	.listing-style3.hotel .details>*>*{
		padding-top: 0px;
	}
</style>
@stop

@section('script')
{{HTML::script('js/jquery.validate.min.js')}}

<script>
    tjq().ready(function() {
        tjq(".cerrarGaleria").live("click", function(){
            tjq("#soap-gallery-popup").fadeOut(200);
        });
        tjq("#cotizadorForm").validate({
            rules: {
                paqueteSelect: "required",
                fecha: "required",
                adultos: "required",
                ninos: "required",
            }
        });

        tjq("#cotizadorForm").submit(function(){
            if(tjq("#cotizadorForm").validate().valid()){
                paqueteSelect = tjq("#paqueteSelect").val();
                
                url = tjq("#tipos_id_"+paqueteSelect).val();
                url += "/" + paqueteSelect;
                url += "/" + tjq("#fecha").val();
                url += "/" + tjq("#adultos").val();
                url += "/" + tjq("#ninos").val();

                actual = tjq("#cotizadorForm").attr("action");
                tjq("#cotizadorForm").attr("action", "{{asset('/category')}}/" + url);
            }

        });
    });
</script>
@stop
@extends('frontend.template')
@stop

@section('contenido')
<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">{{Lang::get('orden.verOrden')}}</h2>
        </div>
        <ul class="breadcrumbs pull-right">
            <li><a href="{{asset('/')}}">{{Lang::get('temp.breadInicio')}}</a></li>
            <li class="active">{{Lang::get('temp.breadOrden')}}</li>
        </ul>
    </div>
</div>
<section id="content" class="gray-area">
    <div class="container">
        @if(Session::get('errorProceso') != "")
        <div class="errorCharBack"><strong>{{Session::get('errorProceso')}}</strong></div>
        @endif
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="booking-information travelo-box">
                    
                            <a type="button" class="btn btn-primary pull-right m-t-20" href="javascript:window.print();">Imprimir</a>
                    <div class="container">
                        <div class="row">
                       
                        <div class="col-xs-12 col-md-12">
                            <img src="{{asset('images/oxxo.png')}}">
                            <img src="{{asset($conOxxo['barcode_url'])}}" class="oxxoBarcode">
                            <div class="codigoOxxo">{{$conOxxo['barcode']}}</div>
                            <div class="expiraOxxoDate">Expiración: {{date("d/m/y", $conOxxo['expires_at'])}}</div>   
                            <h3 class="gTo">Total $ {{number_format($total,0)}} Pesos MXN </h3>         
                        </div>            
                        <hr>
                        <div class="col-xs-12 col-md-12" style="font-size: 12pt; line-height: 2em;">
                            <h2>{{Lang::get('orden.travelInformation')}}</h2>
                                    <dl class="term-description">
                                        <dt>{{Lang::get('orden.travelId')}}:</dt><dd style='color:#C00'><b>{{$reserva[0]->linkreporte}}</b></dd>
                                        <dt>{{Lang::get('orden.travelNombre')}}:</dt><dd>{{$reserva[0]->nombre}}</dd>
                                        <dt>{{Lang::get('orden.travelApellido')}}:</dt><dd>{{$reserva[0]->apellidos}}</dd>
                                        <dt>{{Lang::get('orden.travelCorreo')}}:</dt><dd>{{$reserva[0]->correo}}</dd>
                                    </dl>
                                    <hr>
                                    <h2>{{Lang::get('orden.tourInformation')}}</h2>
                                    <dl class="term-description">
                                        <dt>{{Lang::get('orden.tourNombre')}}:</dt><dd>{{$reserva[0]->pq_nombre}}</dd>
                                        <dt>{{Lang::get('orden.tourFecha')}}:</dt><dd>{{$reserva[0]->pq_fecha}}</dd>
                                        <dt>{{Lang::get('orden.tourAdultos')}}:</dt><dd>{{$reserva[0]->pq_adulto}}</dd>
                                        @if($reserva[0]->pq_nino > 0)
                                        <dt>{{Lang::get('orden.tourNinos')}}:</dt><dd>{{$reserva[0]->pq_nino}}</dd>
                                        @endif
                                    </dl>
                                    <hr>
                        </div>
        
                        </div>
                    </div>

                    <nav>
                      <ul class="pager">
                        <li class="previous"><a href="{{asset('/view/order/'.$reserva[0]->linkreporte)}}"><span aria-hidden="true">&larr;</span> return</a></li>
                      </ul>
                    </nav>
                                        
                    
                    
                </div>
            </div>
            <div class="sidebar col-sm-4 col-md-3">
                <div class="travelo-box contact-box">
                    <h4>{{Lang::get('detalles.ayudaTit')}}</h4>
                    <p>{{Lang::get('detalles.ayudaText')}}</p>
                    <address class="contact-details">
                        <span class="contact-phone"><i class="soap-icon-phone"></i> 01 800 161 47 28</span>
                        <br>
                        <a class="contact-email" href="#">reservations@abaexcursiones.com</a>
                    </address>
                </div>
            </div>
        </div>
    </div>
</section>


@stop

@section('style')
<style>
	section#content {
		padding-top: 40px;
	}
	#main {
		padding-top: 0;
	}
	.pay{
		margin-bottom: 0;
	}
	.payImg{
		margin-right: 10px;
	}
    .pay_id{
        color: #C00;
    }
    .f-14{
        font-size: 14px;
    }
    .oxxoBarcode{
        display: block;
        margin-top: 15px;
        width: 348px;
    }
    .codigoOxxo{
        font-size: 16px;
        margin-top: 5px;
        font-family: Calibri, Arial;
        margin-left: 10px;
    }
    .expiraOxxoDate{
        font-family: Calibri;
        font-size: 16px;
        margin-left: 11px;
        margin-top: 3px;
        margin-bottom: 25px;
    }
    .m-t-20{
        margin-top: 20px;
    }
    .gTo{
        display: inline-block;
        border: 1px solid #64CC00;
        padding: 9px;
        margin-bottom: 32px;
    }
    @media print {
        .contact-box, nav, .btn, .page-title, .main-header, #footer{
            display: none;
        }
    }
</style>
@stop

@section('scriptHead')

@stop

@section('script')

<script type="text/javascript">

    
</script>
@stop
@extends('frontend.template')
@stop

@section('contenido')
<div class="container">
    <h1>Términos y condiciones</h1>
    <ul>
   <li>- Precios calculados por el periodo de fechas y número de personas establecido.</li>

<li>- Tarifas sujetas a cambio, para garantizar la tarifa y disponibilidad es necesario depositar un apartado o el 

pago total según condiciones la opción que se elija al momento de reservar  </li>

<li>- Reservaciones con llegada en Temporada Regular deben ser CANCELADAS 4 días naturales antes de la fecha 

de llegada SIN PENALIZACION solo los cargos bancarios generados por la operación.</li>

<li>- Reservaciones con llegada en Temporada Alta deben ser CANCELADAS 15 días naturales antes de la fecha de 

llegada SIN PENALIZACION solo los cargos bancarios generados por la operación. (Semana Santa, Días 

Festivos de acuerdo al destino, Julio y Agosto, Acción de Gracias, Noche Buena, Año Nuevo, etc.).</li>
</ul>
</div>


@stop

@section('style')
<style>
li{font-size:14px; line-height:22px; }
</style>
@stop

@section('script')

@stop
{{Session::put('detailed', Request::root().'/detailed/'.$nombrePaquete.'/'.$paquete[0]->id.'/'.$fecha.'/'.$adultos.'/'.$ninos);}}
@extends('frontend.template')
@stop
@section('contenido')
<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">{{$paquete[0]->nombre}}</h2>
        </div>
        <ul class="breadcrumbs pull-right">
            <li><a href="{{asset('/')}}">{{Lang::get('temp.breadInicio')}}</a></li>
            <li><a href="{{ asset(Session::get('result')) }}">{{Lang::get('temp.breadResult')}}</a></li>
            <li><a href="{{ asset(Session::get('detailed')) }}">{{Lang::get('temp.breadDetalles')}}</a></li>
            <li class="active">{{Lang::get('temp.breadBook')}}</li>
        </ul>
    </div>
</div>
<?php 
    $urlSelect = ($adultos != "")? $adultos : '';
    $urlSelect = ($ninos != "0" OR $ninos != "")? $urlSelect."/".$ninos : '';

    $tarifa = Temporadas::getTarifaAdulto($fecha, $paquete[0]->id);
    $tarifaNino = Temporadas::getTarifaNino($fecha, $paquete[0]->id);
    $linkreporte = Funciones::generaLinkReporte(6);
?>
<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-md-9">
                <div class="booking-section travelo-box">
                    @if (Session::get('mensaje'))

                        <div class="alert alert-success">{{ Session::get('mensaje') }}</div>

                    @endif
                    <form class="booking-form" id="formDatos" method="post" action="{{asset('/view/order/'.$linkreporte)}}">
                        <input type="hidden" name="pq_nombre" value="{{$paquete[0]->nombre}}">
                        <input type="hidden" name="pq_descripcion" value="{{$paquete[0]->descripcion}}">
                        <input type="hidden" name="pq_duracion" value="{{$paquete[0]->duracion}}">
                        <input type="hidden" name="pq_latitud" value="{{$paquete[0]->latitud}}">
                        <input type="hidden" name="pq_longitud" value="{{$paquete[0]->longitud}}">
                        <input type="hidden" name="pq_nino" value="{{$ninos}}">
                        <input type="hidden" name="pq_adulto" value="{{$adultos}}">
                        <input type="hidden" name="pq_fecha" value="{{$fecha}}">
                        <input type="hidden" name="pq_tarifa_nino" value="{{$tarifaNino}}">
                        <input type="hidden" name="pq_tarifa_adulto" value="{{$tarifa}}">
                        <input type="hidden" name="linkreporte" value="{{$linkreporte}}">
                        <input type="hidden" name="pq_tipo" value="{{$tipo}}">
                        <input type="hidden" name="requestUrl" value="{{Request::url()}}">
                        <input type="hidden" name="status" value="nuevo">
                        <input type="hidden" name="divisa" value="{{Util::getDivisa()}}">

                        <div class="person-information">
                            <h2>{{Lang::get('book.titulo')}}</h2>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-6 col-md-6">
                                    <label>{{Lang::get('book.nombre')}}</label>
                                    <input type="text" name="nombre" class="input-text full-width" value="" placeholder="">
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <label>{{Lang::get('book.apellido')}}</label>
                                    <input type="text" name="apellidos" class="input-text full-width" value="" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 col-md-6">
                                    <label>{{Lang::get('book.correo')}}</label>
                                    <input type="text" name="correo" class="input-text full-width" value="" placeholder="">
                                </div>
                                <div class="col-sm-6 col-md-6">
                                   	<label>{{Lang::get('book.telefono')}}</label>
                                    <input type="text" name="telefono" class="input-text full-width" value="" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{Lang::get('book.comentario')}}</label>
                                <div class="selector">
                                    <textarea name="comentario" class="full-width" id="" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input name="is_receive_promotion" type="checkbox" value="1"> {{Lang::get('book.checkbox')}}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 col-md-5">
                                <button type="submit" class="full-width btn-large">{{Lang::get('book.btn')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
                
                <div id="hotel-features" class="tab-container">
                    <ul class="tabs">
                        <li class="active"><a href="#hotel-description" data-toggle="tab">{{Lang::get('detalles.descripcion')}}</a></li>
                        <li><a href="#incluye" data-toggle="tab">{{Lang::get('detalles.incluye')}}</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="hotel-description">
                            
                            <div class="long-description">
                                <h2>{{$paquete[0]->nombre}}</h2>
                                <p>{{$paquete[0]->descripcion}}</p>
                                <hr>
                            	<img src="{{asset('images/logoSliderInicio.png')}}" alt="">
                            </div>

                        </div>
                        <div class="tab-pane fade" id="incluye">
                            <div id="hotel-amenities">
                                <h2>{{Lang::get('detalles.incluye')}}</h2>
                                <ul class="amenities clearfix style1">
                                    @foreach ($incluye as $key => $val)
                                    @if($val->incluye == 'si')
                                    <li class="">
                                        <div class="icon-box style1"><i class="soap-icon-wifi"></i>{{$val->descripcion}}</div>
                                    </li>
                                    @endif
                                    @endforeach
                                </ul>
                                <br>
                                <?php $no = false; ?>
                                @foreach ($incluye as $key => $val)
                                @if($val->no_incluye == 'no')
                                    <?php $no = true; ?>
                                @endif
                                @endforeach
                                {{($no)? '<h2>'.Lang::get('detalles.noIncluye').'</h2>' : ''}}
                                <ul class="amenities clearfix style1">
                                    @foreach ($incluye as $key => $val)
                                    @if($val->no_incluye == 'no')
                                    <li class="">
                                        <div class="icon-box style1"><i class="soap-icon-wifi"></i>{{$val->descripcion}}</div>
                                    </li>
                                    @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>                
                </div>
            </div>
            <div class="sidebar col-md-3">
            	<?php
					$file = asset('/images/shortcodes/gallery-popup/paquete_'.$paquete[0]->id.'/portada.jpg');
                    $file_headers = @get_headers($file);
                    if($file_headers[0] == 'HTTP/1.0 404 Not Found' OR $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                        $img = '/images/no-portada.png';
                    }
                    else {
                        $img = '/images/shortcodes/gallery-popup/paquete_'.$paquete[0]->id.'/portada.jpg';
                    }

		            $total = ($adultos * $tarifa) + ($ninos * $tarifaNino);
				?>
                <article class="detailed-logo">
                    <figure>
                        <img width="114" height="85" src="{{asset($img)}}" alt="">
                    </figure>
                    <div class="details">
                        <h2 class="box-title">{{$paquete[0]->nombre}}<small><i class="soap-icon-clock yellow-color"></i>
                        <span class="fourty-space">{{$paquete[0]->duracion}}</span></small></h2>
                        <span class="price clearfix">
                            <small class="pull-left">TOTAL</small>
                            @if($tarifa > 0) 
                            	<span class="pull-right">$ {{number_format($total, 0)}} {{Util::getDivisa()}}</span>
	                        @else
	                            <span class="price">SOLD</span>
	                        @endif
                        </span>
                        <div class="feedback clearfix">
                            <div title="" class="five-stars-container" data-toggle="tooltip" data-placement="bottom" data-original-title="4 stars">
                                <span class="five-stars" style="width: 80%;"></span>
                            </div>
                            <span class="review pull-right">{{Lang::get('list.recomendado')}}</span>
                        </div>
						@if($adultos > 1 OR $ninos > 0)
                        <div class="feedback clearfix">
                            <div title="" class="">
                            	<span class="pull-left">Rate per {{$adultos}} adults: <b>${{number_format($tarifa, 0)}}</b></span>
                            	<span class="five-stars" style="width: 80%;"></span>
                            </div>
                            <span class="review pull-right"><b>${{number_format($tarifa * $adultos, 0)}} {{Util::getDivisa()}}</b></span>
                        </div>
                        @if($ninos > 0)
                        <div class="feedback clearfix">
                            <div title="" class="">
                            	<span class="pull-left">Rate per {{$ninos}} kids: <b>${{number_format($tarifaNino, 0)}}</b></span>
                            	<span class="five-stars" style="width: 80%;"></span>
                            </div>
                            <span class="review pull-right"><b>${{number_format($tarifaNino * $ninos, 0)}} {{Util::getDivisa()}}</b></span>
                        </div>
                        @endif
                        @endif                        
                    </div>
                </article>
                <div class="travelo-box contact-box">
                    <h4>{{Lang::get('detalles.ayudaTit')}}</h4>
                    <p>{{Lang::get('detalles.ayudaText')}}</p>
                    <address class="contact-details">
                        <span class="contact-phone"><i class="soap-icon-phone"></i> +52-998-285-89-95</span>
                        <br>
                        <a class="contact-email" href="#">reservations@abaexcursiones.com</a>
                    </address>
                </div>
                <div class="travelo-box">
                    <h4>{{Lang::get('detalles.lista')}}</h4>
                    <div class="image-box style14">
                    	@foreach($similarListing as $k => $v)
                    	<?php 
                    		$file = asset('/imgPaquetes/paquete_'.$v->id.'/portada.jpg');
							$file_headers = @get_headers($file);
							if($file_headers[0] == 'HTTP/1.0 404 Not Found' OR $file_headers[0] == 'HTTP/1.1 404 Not Found') {
							    $img = '/images/no-portada.png';
							}
							else {
							    $img = '/imgPaquetes/paquete_'.$v->id.'/portada.jpg';
							}
							$tarifa = Temporadas::getTarifaAdulto($fecha, $v->id);
                    	?>
                    	@if($tarifa > 0) 
	                        <article class="box">
	                            <figure>
	                                <a href="{{asset('/detailed/'.Funciones::generaURL($v->nombre).'/'.$v->id.'/'.$fecha.'/'.$urlSelect)}}"><img src="{{asset($img)}}" alt=""></a>
	                            </figure>
	                            <div class="details">
	                                <h5 class="box-title"><a href="{{asset('/detailed/'.Funciones::generaURL($v->nombre).'/'.$v->id.'/'.$fecha.'/'.$urlSelect)}}">{{$v->nombre}}</a></h5>
	                                <label class="price-wrapper">
	                                    <span class="price-per-unit">${{@number_format($tarifa, 0)}} {{Util::getDivisa()}}</span>
                                        <br>{{Lang::get('list.poradulto')}}
	                                </label>
	                            </div>
	                        </article>
                        @endif
                    	@endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@stop

@section('style')
<style>
	section#content {
		padding-top: 40px;
	}
	#main {
		padding-top: 0;
	}
</style>
@stop

@section('script')
{{HTML::script('js/jquery.validate.min.js')}}
<script type="text/javascript">
    tjq().ready(function() {
        tjq("#formDatos").validate({
			rules: {
				nombre: "required",
				apellidos: "required",
				correo: {
					required: true,
					email: true
				},
				telefono: {
					required: true,
					minlength: 8
				},
				email: {
					required: true,
					email: true
				},
			}
		});
    });
</script>
@stop
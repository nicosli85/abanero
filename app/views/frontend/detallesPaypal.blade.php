<img class="payImg" src="{{asset('/images/paypal.png')}}" alt="">
<h2>Pay Information</h2>
<dl class="term-description">
    <dt>Payment ID:</dt><dd>{{$payment->id}}</dd>
    <dt>State:</dt><dd>{{$payment->state}}</dd>
    <dt>Intent:</dt><dd>{{$payment->intent}}</dd>
    <dt>Payment method:</dt><dd>{{$payment->payer->payment_method}}</dd>
    <dt>TOTAL PAY:</dt><dd>{{$payment->transactions[0]->amount->total}} {{$payment->transactions[0]->amount->currency}}</dd>
    <dt>Description:</dt><dd>{{$payment->transactions[0]->description}}</dd>
</dl>
<h2>Payer Information</h2>
<dl class="term-description">
    <dt>First Name:</dt><dd>{{$payment->payer->payer_info->first_name}}</dd>
    <dt>Last Name:</dt><dd>{{$payment->payer->payer_info->last_name}}</dd>
    <dt>Email:</dt><dd>{{$payment->payer->payer_info->email}}</dd>
</dl>
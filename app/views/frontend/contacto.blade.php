@extends('frontend.template')
@stop

@section('contenido')
<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">{{Lang::get('temp.contacto')}}</h2>
        </div>
        <ul class="breadcrumbs pull-right">
            <li><a href="{{asset('/')}}">{{Lang::get('temp.breadInicio')}}</a></li>
            <li class="active">{{Lang::get('temp.contacto')}}</li>
        </ul>
    </div>
</div>
<div class="container" style="padding-top:25px">
	@if(isset($mensaje))
		<div class="alert alert-success alert-dismissible" role="alert">
		  <strong>Éxito!</strong> su mensaje se envió correctamente, en breve nos comunicaremos con usted
		</div>
		<a href="{{asset('/')}}" class="btn btn-primary">Regresar a inicio</a>
	@else
	<h3>{{Lang::get('form.titCont')}}</h3>
	{{ Form::open(array('url' => asset('contacto'), 'method' => 'post')) }}
	  <div class="form-group">
	    <label for="email">{{Lang::get('form._con-email')}}</label>
	    <input type="email" class="form-control" name="email" id="email" placeholder="{{Lang::get('form._con-enteremail')}}" required>
	  </div>
	  <div class="form-group">
	    <label for="phone">{{Lang::get('form._con-telefono')}}</label>
	    <input type="text" class="form-control" name="phone" id="phone" placeholder="{{Lang::get('form._con-telefono')}}" required>
	  </div>
	  <div class="form-group">
	    <label for="comment">{{Lang::get('form._con-comentario')}}</label>
	    <textarea class="form-control" name="comment" id="comment" placeholder="{{Lang::get('form._con-comentario')}}" required></textarea>
	  </div>
	  
	  <button type="submit" class="btn btn-primary">{{Lang::get('form._con-enviar')}}</button>
	{{ Form::close() }}
	@endif
</div>
<style type="text/css">
	.alert.alert-success {
	  background: #69AD00 !important;
	}
</style>
@stop
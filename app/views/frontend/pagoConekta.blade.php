@extends('frontend.template')
@stop

@section('contenido')
<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">{{Lang::get('orden.verOrden')}}</h2>
        </div>
        <ul class="breadcrumbs pull-right">
            <li><a href="{{asset('/')}}">{{Lang::get('temp.breadInicio')}}</a></li>
            <li class="active">{{Lang::get('temp.breadOrden')}}</li>
        </ul>
    </div>
</div>
<section id="content" class="gray-area">
    <div class="container">
        @if(Session::get('errorProceso') != "")
        <div class="errorCharBack"><strong>{{Session::get('errorProceso')}}</strong></div>
        @endif
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="booking-information travelo-box">
                    
                    <div class="container">
                        <div class="row">
                       
        <div class="col-xs-12 col-md-4">
        
            <!-- CREDIT CARD FORM STARTS HERE -->
            <div class="panel panel-default credit-card-box">
                <div class="panel-heading display-table" >
                    <div class="row display-tr" >
                        <h3 class="panel-title display-td" >Payment Details</h3>
                        <div class="display-td" >                            
                            <img class="img-responsive pull-right" src="{{asset('/images/tarjetas.png')}}">
                        </div>
                    </div>                    
                </div>
                <div class="panel-body">
                    <div class="errorChar"><strong>Error</strong></div>
                    <form role="form" id="payment-form" action="{{asset('/pay/order/'.$reserva[0]->linkreporte)}}" method="POST">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="nombreCompleto">FULL NAME</label>
                                    <input type="text" class="form-control" name="nombreCompleto" data-conekta="card[name]" />
                                </div>
                            </div>                        
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="cardNumber">CARD NUMBER</label>
                                    <div class="input-group">
                                        <input 
                                            type="tel"
                                            class="form-control"
                                            name="cardNumber"
                                            id="cardNumber" 
                                            autofocus
                                            autocomplete="cc-number" placeholder="Valid Card Number" required
                                            data-conekta="card[number]"
                                        />
                                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7 col-md-7">
                                <label>Fecha de expiración</label>
                                <div class="row">  
                                    <div class="col-md-6">
                                        <label>
                                          <input 
                                            type="text" 
                                            size="2" 
                                            class="form-control" 
                                            placeholder="MM"
                                            id="mes" 
                                            data-conekta="card[exp_month]"/>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <input 
                                            type="text" 
                                            size="4" 
                                            class="form-control"
                                            placeholder="AAAA" 
                                            id="anio" 
                                            data-conekta="card[exp_year]"/>
                                    </div>
                                  </div>
                            </div>
                            <div class="col-xs-5 col-md-5 pull-right">
                                <div class="form-group">
                                    <label for="cardCVC">CV CODE</label>
                                    <input 
                                        type="tel" 
                                        class="form-control"
                                        name="cardCVC"
                                        placeholder="CVC"
                                        autocomplete="cc-csc"
                                        id="cvcode" 
                                        required
                                        data-conekta="card[cvc]"
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <h3>Total Cargo $ {{number_format($total,0)}} Pesos MXN </h3>
                                <button class="full-width f-14 btn-submit" type="submit">Pagar Ahora</button>
                            </div>
                        </div>
                        <div class="row" style="display:none;">
                            <div class="col-xs-12">
                                <p class="payment-errors"></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>            
            <!-- CREDIT CARD FORM ENDS HERE -->
             <nav>
                      <ul class="pager">
                        <li class="previous"><a href="{{asset('/view/order/'.$reserva[0]->linkreporte)}}"><span aria-hidden="true">&larr;</span> return</a></li>
                      </ul>
                    </nav>
            
        </div>            
        
        <div class="col-xs-12 col-md-8" style="font-size: 12pt; line-height: 2em;">
            <h2>{{Lang::get('orden.travelInformation')}}</h2>
                    <dl class="term-description">
                        <dt>{{Lang::get('orden.travelId')}}:</dt><dd style='color:#C00'><b>{{$reserva[0]->linkreporte}}</b></dd>
                        <dt>{{Lang::get('orden.travelNombre')}}:</dt><dd>{{$reserva[0]->nombre}}</dd>
                        <dt>{{Lang::get('orden.travelApellido')}}:</dt><dd>{{$reserva[0]->apellidos}}</dd>
                        <dt>{{Lang::get('orden.travelCorreo')}}:</dt><dd>{{$reserva[0]->correo}}</dd>
                    </dl>
                    <hr>
                    <h2>{{Lang::get('orden.tourInformation')}}</h2>
                    <dl class="term-description">
                        <dt>{{Lang::get('orden.tourNombre')}}:</dt><dd>{{$reserva[0]->pq_nombre}}</dd>
                        <dt>{{Lang::get('orden.tourFecha')}}:</dt><dd>{{$reserva[0]->pq_fecha}}</dd>
                        <dt>{{Lang::get('orden.tourAdultos')}}:</dt><dd>{{$reserva[0]->pq_adulto}}</dd>
                        @if($reserva[0]->pq_nino > 0)
                        <dt>{{Lang::get('orden.tourNinos')}}:</dt><dd>{{$reserva[0]->pq_nino}}</dd>
                        @endif
                    </dl>
                    <hr>
        </div>
        
    </div>
</div>

                    
                    
                    
                    
                </div>
            </div>
            <div class="sidebar col-sm-4 col-md-3">
                <div class="travelo-box contact-box">
                    <h4>{{Lang::get('detalles.ayudaTit')}}</h4>
                    <p>{{Lang::get('detalles.ayudaText')}}</p>
                    <address class="contact-details">
                        <span class="contact-phone"><i class="soap-icon-phone"></i> 01 800 161 47 28</span>
                        <br>
                        <a class="contact-email" href="#">reservations@abaexcursiones.com</a>
                    </address>
                </div>
            </div>
        </div>
    </div>
</section>


@stop

@section('style')
<style>
	section#content {
		padding-top: 40px;
	}
	#main {
		padding-top: 0;
	}
	.pay{
		margin-bottom: 0;
	}
	.payImg{
		margin-right: 10px;
	}
    .pay_id{
        color: #C00;
    }
    .f-14{
        font-size: 14px;
    }
    /* CSS for Credit Card Payment form */
    .credit-card-box .panel-title {
        display: inline;
        font-weight: bold;
    }
    .credit-card-box .form-control.error {
        border-color: red;
        outline: 0;
        box-shadow: inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(255,0,0,0.6);
    }
    .credit-card-box label.error {
      font-weight: bold;
      color: red;
      padding: 2px 8px;
      margin-top: 2px;
    }
    .credit-card-box .payment-errors {
      font-weight: bold;
      color: red;
      padding: 2px 8px;
      margin-top: 2px;
    }
    .credit-card-box label {
        display: block;
    }
    /* The old "center div vertically" hack */
    .credit-card-box .display-table {
        display: table;
    }
    .credit-card-box .display-tr {
        display: table-row;
    }
    .credit-card-box .display-td {
        display: table-cell;
        vertical-align: middle;
        width: 50%;
    }
    /* Just looks nicer */
    .credit-card-box .panel-heading img {
        min-width: 180px;
    }
    .errorCharBack{
        color: #FFF;
          background-color: #E52A2A;
          padding: 8px 18px;
          margin-bottom: 11px;
          font-weight: bold;
    }
    .errorChar{
        color: #FFF;
          background-color: #E52A2A;
          padding: 8px 18px;
          margin-bottom: 11px;
          display: none;
          font-weight: bold;
    }
</style>
@stop

@section('scriptHead')
<script type="text/javascript" src="https://conektaapi.s3.amazonaws.com/v0.3.2/js/conekta.js"></script>
<script type="text/javascript">
    Conekta.setPublishableKey('key_P2auS6qy5hofWr4t58RpppQ');
</script>
@stop

@section('script')
<script type="text/javascript" src="{{asset('js/jquery.mask.js')}}"></script>
<script type="text/javascript">

    tjq("#payment-form").submit(function(event){
        tjq(".errorChar").html("").hide();
        var tokenParams;
        tokenParams = {
          "card": {
            "number": tjq('input[data-conekta="card[number]"]').val(),
            "name": tjq('input[data-conekta="card[name]"]').val(),
            "exp_year": tjq('input[data-conekta="card[exp_year]"]').val(),
            "exp_month": tjq('input[data-conekta="card[exp_month]"]').val(),
            "cvc": tjq('input[data-conekta="card[cvc]"]').val()
          }
        };

        tjq(".btn-submit").prop("disabled", true);

        Conekta.token.create(tokenParams, successResponseHandler, errorResponseHandler);


        return false;
    });

    function successResponseHandler(token){
        tjq("#payment-form").append(tjq("<input type=\"hidden\" name=\"conektaTokenId\" />").val(token.id));
        tjq("#payment-form").get(0).submit();
    }

    function errorResponseHandler(error){
        tjq(".errorChar").html(error.message).show();
        tjq(".btn-submit").prop("disabled", false);
    }

    


    jQuery(function($) {

        tjq('#cardNumber').mask('0000000000000000');
        tjq('#cvcode').mask('000');
        tjq('#mes').mask('00');
        tjq('#anio').mask('0000');
        
    });
</script>
@stop
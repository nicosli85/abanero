{{Session::put('detailed', Request::url());}}
@extends('frontend.template')
@stop

@section('contenido')

<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">{{$paquete[0]->nombre}}</h2>
        </div>
        <ul class="breadcrumbs pull-right">
            <li><a href="{{asset('/')}}">{{Lang::get('temp.breadInicio')}}</a></li>
            <li><a href="{{ asset(Session::get('result')) }}">{{Lang::get('temp.breadResult')}}</a></li>
            <li class="active">{{Lang::get('temp.breadDetalles')}}</li>
        </ul>
    </div>
</div>
<?php 
    $urlSelect = ($adultos != "")? $adultos : '';
    $urlSelect = ($ninos != "0" OR $ninos != "")? $urlSelect."/".$ninos : '';
?>
<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-md-9">
                <div class="tab-container style1" id="hotel-main-content">
                    <ul class="tabs">
                        <li class="active"><a data-toggle="tab" href="#photos-tab">{{Lang::get('detalles.fotos')}}</a></li>
                        <li class=""><a data-toggle="tab" href="#map-tab">{{Lang::get('detalles.mapa')}}</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="photos-tab" class="tab-pane fade active in">
                            <div class="photo-gallery style1" data-animation="slide" data-sync="#photos-tab .image-carousel">
	                            
	                            <ul class="slides">
	                            		<?php

                                            $imgs = "";
                                            $dir='images/shortcodes/gallery-popup/paquete_'.$paquete[0]->id.'/';
                                            $images = glob("$dir{*.jpg}", GLOB_BRACE);  
                                            if(!empty($images))
                                            foreach($images as $v){

                                                if( ! preg_match('/portada/', $v) ){
                                                    $imgs[] = $v;

                                                }

                                            }

                                        ?>
                                        @if(!empty($imgs))
                                             @foreach($imgs as $ind => $val)
                                                <li>{{ HTML::image($val, '') }}</li>
                                            @endforeach
                                        @endif   
	                                </ul>
	                                
                                </div>
                            <div class="image-carousel style1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photos-tab .photo-gallery">
                            
                            	<ul class="slides">
                                    <?php

                                        $imgs = "";
                                        $dir='images/shortcodes/gallery-popup/paquete_'.$paquete[0]->id.'/thumbnail/';
                                        $images = glob("$dir{*.jpg}", GLOB_BRACE);  
                                        if(!empty($images))
                                        foreach($images as $v){

                                            if( ! preg_match('/portada/', $v) ){
                                                $imgs[] = $v;

                                            }

                                        }

                                    ?>
                                    @if(!empty($imgs))
                                         @foreach($imgs as $ind => $val)
                                            <li>{{ HTML::image($val, '') }}</li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                            
                        </div>
                        <div id="map-tab" class="tab-pane fade"></div>                        
                    </div>
                </div>
                
                <div id="hotel-features" class="tab-container">
                    <ul class="tabs">
                        <li class="active"><a href="#hotel-description" data-toggle="tab">{{Lang::get('detalles.descripcion')}}</a></li>
                        <li><a href="#incluye" data-toggle="tab">{{Lang::get('detalles.incluye')}}</a></li>
                        <li><a href="#search-again" data-toggle="tab">{{Lang::get('detalles.buscar')}}</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="hotel-description">
                            
                            <div class="long-description">
                                <h2>{{$paquete[0]->nombre}}</h2>
                                <p>{{$paquete[0]->descripcion}}</p>
                                <hr>
                            	<img src="{{asset('images/logoSliderInicio.png')}}" alt="">
                            </div>

                        </div>
                        <div class="tab-pane fade" id="search-again">
                            <div class="update-search clearfix">
                            	<form action="#" id="cotizadorForm" method="post">
                                <div class="col-md-4">
			                        <h4 class="title">{{Lang::get('form.donde')}}</h4>
			                        <label>{{Lang::get('form.titulo')}}</label>
			                        <div class="selector">
			                            <?php 
			                                foreach (Tipos::lista() as $k => $v) {
			                                    $ar[$v->id] = $v->nombre;
			                                }
			                            ?>
			                            {{ Form::select('paqueteSelect', $ar, 1, array('class' => 'full-width', 'required' => 'on', 'id' => 'paqueteSelect')) }}
			                            
			                        </div>
			                    </div>
			                    
			                    <div class="form-group col-sm-6 col-md-3">
			                        <h4 class="title">{{Lang::get('form.cuando')}}</h4>
			                        <label>{{Lang::get('form.fecha')}}</label>
			                        <div class="datepicker-wrap">
			                            <input type="text" id="fecha" name="fecha" class="input-text full-width" placeholder="Check In" value="{{$fecha}}" >
			                            <img class="ui-datepicker-trigger" src="{{asset('images/icon/blank.png')}}" alt="" title="">
			                        </div>
			                    </div>
                                
                                <div class="col-md-3">
                                    <h4 class="title">{{Lang::get('form.quien')}}</h4>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label>{{Lang::get('form.adultos')}}</label>
                                            <div class="selector">
                                            	{{ Form::select('adultos', array(
			                                        '1' => '01',
			                                        '2' => '02',
			                                        '3' => '03',
			                                        '4' => '04',

			                                    ), $adultos, array('class' => 'full-width', 'required' => 'on', 'id' => 'adultos')) }}
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <label>{{Lang::get('form.ninos')}}</label>
                                            <div class="selector">
                                                {{ Form::select('ninos', array(
			                                        '0' => '0',
			                                        '1' => '01',
			                                        '2' => '02',
			                                        '3' => '03',
			                                        '4' => '04',
			                                    ), $ninos, array('class' => 'full-width', 'required' => 'on', 'id' => 'ninos')) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-2">
                                    <h4 class="visible-md visible-lg">&nbsp;</h4>
                                    <label class="visible-md visible-lg">&nbsp;</label>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <button data-animation-duration="1" data-animation-type="bounce" class="full-width icon-check animated bounce" type="submit" >{{Lang::get('form.boton')}}</button>
                                        </div>
                                    </div>
                                </div>
                                @foreach (Tipos::lista() as $key => $val) 
				                    <input type="hidden" value="{{Funciones::generaURL($val->nombre)}}" id="tipos_id_{{$val->id}}">
				                @endforeach
                                </form>
                            </div>
                            
                        </div>
                        <div class="tab-pane fade" id="incluye">
                            <div id="hotel-amenities">
                                <h2>{{Lang::get('detalles.incluye')}}</h2>
                                <ul class="amenities clearfix style1">
                                    @foreach ($incluye as $key => $val)
                                    @if($val->incluye == 'si')
                                    <li class="">
                                        <div class="icon-box style1"><i class="fa fa-check"></i>{{$val->descripcion}}</div>
                                    </li>
                                    @endif
                                    @endforeach
                                </ul>
                                <br>
                                <?php $no = false; ?>
                                @foreach ($incluye as $key => $val)
                                @if($val->no_incluye == 'no')
                                    <?php $no = true; ?>
                                @endif
                                @endforeach
                                {{($no)? '<h2>'.Lang::get('detalles.noIncluye').'</h2>' : ''}}
                                <ul class="amenities clearfix style2">
                                    @foreach ($incluye as $key => $val)
                                    @if($val->no_incluye == 'no')
                                    <li class="">
                                        <div class="icon-box style2"><i class="fa fa-times"></i>{{$val->descripcion}}</div>
                                    </li>
                                    @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>                
                </div>
            </div>
            <div class="sidebar col-md-3">
            	<?php
					$file = asset('/images/shortcodes/gallery-popup/paquete_'.$paquete[0]->id.'/portada.jpg');
					$file_headers = @get_headers($file);
					if($file_headers[0] == 'HTTP/1.0 404 Not Found' OR $file_headers[0] == 'HTTP/1.1 404 Not Found') {
					    $img = '/images/no-portada.png';
					}
					else {
					    $img = '/images/shortcodes/gallery-popup/paquete_'.$paquete[0]->id.'/portada.jpg';
					}

					$tarifa = Temporadas::getTarifaAdulto($fecha, $paquete[0]->id);
					$tarifaNino = Temporadas::getTarifaNino($fecha, $paquete[0]->id);

		            $total = ($adultos * $tarifa) + ($ninos * $tarifaNino);
				?>
                <article class="detailed-logo">
                    <figure>
                        <img width="114" height="85" src="{{asset($img)}}" alt="">
                    </figure>
                    <div class="details">
                        <h2 class="box-title">{{$paquete[0]->nombre}}<small><i class="soap-icon-clock yellow-color"></i>
                        <span class="fourty-space">{{$paquete[0]->duracion}}</span></small></h2>

                        <span class="price clearfix">
                            <small class="pull-left">TOTAL</small>
                            @if($tarifa > 0) 
                            	<span class="pull-right" id="granTotalText">$ {{number_format($total, 0)}} {{Util::getDivisa()}}</span>
	                        @else
	                            <span class="price">{{Lang::get('list.vendido')}}</span>
	                        @endif
                        </span>

                        <div class="feedback clearfix">
                            <div title="" class="five-stars-container" data-toggle="tooltip" data-placement="bottom" data-original-title="4 stars">
                                <span class="five-stars" style="width: 80%;"></span>
                            </div>
                            <span class="review pull-right">{{Lang::get('list.recomendado')}}</span>
                        </div>
						
                        <div class="feedback clearfix">
                            <div title="" class="">
                            	<span class="pull-left" id="subTotalAdultoText">Rate per {{$adultos}} adults: <b>${{number_format($tarifa, 0)}}</b></span>
                            	<span class="five-stars" style="width: 80%;"></span>
                            </div>
                            <span class="review pull-right" id="subTotalAdulto"><b>${{number_format($tarifa * $adultos, 0)}} {{Util::getDivisa()}}</b></span>
                        </div>
                        
                        <div class="feedback clearfix" id="divSubNinos" style="display:{{($ninos>0)? 'block' : 'none'}}">
                            <div title="" class="">
                            	<span class="pull-left" id="subTotalNinoText">Rate per {{$ninos}} kids: <b>${{number_format($tarifaNino, 0)}}</b></span>
                            	<span class="five-stars" style="width: 80%;"></span>
                            </div>
                            <span class="review pull-right" id="subTotalNino"><b>${{number_format($tarifaNino * $ninos, 0)}} {{Util::getDivisa()}}</b></span>
                        </div>
                        
                        @if($tarifa > 0) 
                        <div class="row">
                            <div class="col-xs-6">
                                <label>{{Lang::get('form.adultos')}}</label>
                                <div class="selector">
                                    {{ Form::select('adultosQuick', array(
                                        '1' => '01',
                                        '2' => '02',
                                        '3' => '03',
                                        '4' => '04',

                                    ), $adultos, array('class' => 'full-width', 'required' => 'on', 'id' => 'adultosQuick')) }}
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <label>{{Lang::get('form.ninos')}}</label>
                                <div class="selector">
                                    {{ Form::select('ninosQuick', array(
                                        '0' => '0',
                                        '1' => '01',
                                        '2' => '02',
                                        '3' => '03',
                                        '4' => '04',
                                    ), $ninos, array('class' => 'full-width', 'required' => 'on', 'id' => 'ninosQuick')) }}
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="tarifa_adulto" value="{{number_format($tarifa, 0, ',', '')}}">
                        <input type="hidden" id="tarifa_nino" value="{{number_format($tarifaNino, 0, ',', '')}}">
                        @endif
                        <!--<a class="button btn-extra yellow"><i class="soap-icon-beach"></i><span><b class="sg">Satisfaction Guarantee</b><br><em>Book Tour</em></span></a>-->
                        <br>
                        <a class="button btn-large orange full-width" id="linkBook" href="{{asset('/book/'.Funciones::generaURL($paquete[0]->nombre).'/'.$paquete[0]->id.'/'.$fecha.'/'.$urlSelect)}}">{{Lang::get('detalles.btnBook')}}</a>
                        
                    </div>
                </article>
                <div class="travelo-box contact-box">
                    <h4>{{Lang::get('detalles.ayudaTit')}}</h4>
                    <p>{{Lang::get('detalles.ayudaText')}}</p>
                    <address class="contact-details">
                        <span class="contact-phone"><i class="soap-icon-phone"></i> +52-998-285-89-95</span>
                        <br>
                        <a class="contact-email" href="#">reservations@abaexcursiones.com</a>
                    </address>
                </div>
                <div class="travelo-box">
                    <h4>{{Lang::get('detalles.lista')}}</h4>
                    <div class="image-box style14">
                    	@foreach($similarListing as $k => $v)
                    	<?php 
                    		$file = asset('/images/shortcodes/gallery-popup/paquete_'.$v->id.'/1.jpg');
							$file_headers = @get_headers($file);
							if($file_headers[0] == 'HTTP/1.0 404 Not Found' OR $file_headers[0] == 'HTTP/1.1 404 Not Found') {
							    $img = '/images/no-portada.png';
							}
							else {
							    $img = '/images/shortcodes/gallery-popup/paquete_'.$v->id.'/1.jpg';
							}
							$tarifa = Temporadas::getTarifaAdulto($fecha, $v->id);
                    	?>
                    	@if($tarifa > 0) 
	                        <article class="box">
	                            <figure>
	                                <a href="{{asset('/detailed/'.Funciones::generaURL($v->nombre).'/'.$v->id.'/'.$fecha.'/'.$urlSelect)}}"><img src="{{asset($img)}}" alt=""></a>
	                            </figure>
	                            <div class="details">
	                                <h5 class="box-title"><a href="{{asset('/detailed/'.Funciones::generaURL($v->nombre).'/'.$v->id.'/'.$fecha.'/'.$urlSelect)}}">{{$v->nombre}}</a></h5>
	                                <label class="price-wrapper">
	                                    <span class="price-per-unit">${{@number_format($tarifa, 2)}} {{Util::getDivisa()}}</span>
                                        <br>{{Lang::get('list.poradulto')}}
	                                </label>
	                            </div>
	                        </article>
                        @endif
                    	@endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@stop

@section('style')
<style>
	section#content {
		padding-top: 40px;
	}
	#main {
		padding-top: 0;
	}
	button.btn-extra,a.button.btn-extra{
		width: 100%;
	}
	.sg{
		font-size: 10px;
	}
    .icon-box.style2>i {
        display: block;
        width: 42px;
        float: left;
        background: #E73270;
        line-height: 42px;
        color: #FFF;
        font-size: 2em;
        margin-right: 15px;
    }
    .icon-box.style2 {
        line-height: 1.75em;
        color: #7A7A7B;
        font-size: 0.9167em;
        display: inline-block;
        background: #F5F5F5 !important;
        width: 100%;
        line-height: 42px;
    }
</style>
@stop

@section('script')
<!-- Google Map Api -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
{{HTML::script('js/jquery.validate.min.js')}}
<script type="text/javascript">
    tjq(document).ready(function() {
        tjq("#adultosQuick").on("change", function(){
            granTotal = ( tjq("#adultosQuick").val() * tjq("#tarifa_adulto").val() ) + ( tjq("#ninosQuick").val() * tjq("#tarifa_nino").val() )
            subTotal = tjq("#adultosQuick").val() * tjq("#tarifa_adulto").val();
            tjq("#granTotalText").text("$ " + granTotal + " {{Util::getDivisa()}}");
            tjq("#subTotalAdultoText").html("RATE PER "+tjq("#adultosQuick").val()+" ADULTS: <b>$"+tjq("#tarifa_adulto").val()+"</b>");
            tjq("#subTotalAdulto").html("<b>$"+subTotal+" {{Util::getDivisa()}}</b>");
            urlLink();
        });
        tjq("#ninosQuick").on("change", function(){
            if(tjq("#adultosQuick").val() > 0){
                tjq("#divSubNinos").slideDown(100);
                granTotal = ( tjq("#adultosQuick").val() * tjq("#tarifa_adulto").val() ) + ( tjq("#ninosQuick").val() * tjq("#tarifa_nino").val() )
                subTotal = tjq("#ninosQuick").val() * tjq("#tarifa_nino").val();
                tjq("#granTotalText").text("$ " + granTotal + " {{Util::getDivisa()}}");
                tjq("#subTotalNinoText").html("RATE PER "+tjq("#ninosQuick").val()+" ADULTS: <b>$"+tjq("#tarifa_nino").val()+"</b>");
                tjq("#subTotalNino").html("<b>$"+subTotal+" {{Util::getDivisa()}}</b>");
            }
            if(tjq("#ninosQuick").val() == 0)
                tjq("#divSubNinos").slideUp(100);
            urlLink();
        });
    });
    
    function urlLink(){
        urlBook = "{{Request::root().'/book/'.$categoria}}";
        urlBook += "/" + "{{$paquete[0]->id}}";
        urlBook += "/" + "{{$fecha}}";
        urlBook += "/" + tjq("#adultosQuick").val();
        urlBook += "/" + tjq("#ninosQuick").val();

        tjq("#linkBook").attr("href", urlBook);
    }

    tjq('a[href="#map-tab"]').on('shown.bs.tab', function (e) {
        var center = panorama.getPosition();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });
    var map = null;
    var panorama = null;
    var marcador = null;
    var fenway = new google.maps.LatLng({{$paquete[0]->latitud}}, {{$paquete[0]->longitud}});
    var mapOptions = {
        center: fenway,
        zoom: 12
    };
    
    var panoramaOptions = {
        position: fenway,
        pov: {
            heading: 34,
            pitch: 10
        }
    };
    function initialize() {
        tjq("#map-tab").height(tjq("#hotel-main-content").width() * 0.6);
        map = new google.maps.Map(document.getElementById('map-tab'), mapOptions);
        panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);
        marcador = new google.maps.Marker({position: fenway,map: map, animation: google.maps.Animation.DROP, title:"Ubicacion del tour"});
        map.setStreetView(panorama);
    }
    google.maps.event.addDomListener(window, 'load', initialize);

    tjq().ready(function() {
        tjq("#cotizadorForm").validate({
            rules: {
                paqueteSelect: "required",
                fecha: "required",
                adultos: "required",
                ninos: "required",
            }
        });

        tjq("#cotizadorForm").submit(function(){
            if(tjq("#cotizadorForm").validate().valid()){
                paqueteSelect = tjq("#paqueteSelect").val();
                
                url = tjq("#tipos_id_"+paqueteSelect).val();
                url += "/" + paqueteSelect;
                url += "/" + tjq("#fecha").val();
                url += "/" + tjq("#adultos").val();
                url += "/" + tjq("#ninos").val();

                actual = tjq("#cotizadorForm").attr("action");
                tjq("#cotizadorForm").attr("action", "{{asset('/category')}}/" + url);
            }

        });
    });
</script>
@stop
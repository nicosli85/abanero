<a href="#" class="cerrarGaleria"><i class="fa fa-compress"></i> {{ Lang::get( 'list.cerrar' ) }}</a>
<div class="clearfix"></div>
<div class="photo-gallery style1" id="photo-gallery1" data-animation="slide" data-sync="#image-carousel1">
    <ul class="slides">
        <?php

            $imgs = "";
            $dir='images/shortcodes/gallery-popup/paquete_'.$id.'/';
            $images = glob("$dir{*.jpg}", GLOB_BRACE);  

            foreach($images as $v){

                if( ! preg_match('/portada/', $v) ){
                    $imgs[] = $v;

                }

            }

        ?>
        @if(!is_null($imgs))
             @foreach($imgs as $ind => $val)
                <li>{{ HTML::image($val, '') }}</li>
            @endforeach
        @endif     
    </ul>
</div>
<div class="image-carousel style1" id="image-carousel1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photo-gallery1">
    <ul class="slides">
        <?php

            $imgs = "";
            $dir='images/shortcodes/gallery-popup/paquete_'.$id.'/thumbnail/';
            $images = glob("$dir{*.jpg}", GLOB_BRACE);  

            foreach($images as $v){

                if( ! preg_match('/portada/', $v) ){
                    $imgs[] = $v;

                }

            }

        ?>
        @if(!is_null($imgs))
             @foreach($imgs as $ind => $val)
                <li>{{ HTML::image($val, '') }}</li>
            @endforeach
        @endif
    </ul>
</div>
<style type="text/css">
    .cerrarGaleria{
        color: #FFF;
        float: right;
        display: block;
        font-size: 14px;
        margin-bottom: 5px;
    }
</style>
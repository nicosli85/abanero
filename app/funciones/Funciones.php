<?php 
class Funciones{
	public static function generaURL($texto){

        $liga = str_replace(" ", "-", $texto);

        $liga = str_replace("á", "a", $liga);

        $liga = str_replace("é", "e", $liga);

        $liga = str_replace("í", "i", $liga);

        $liga = str_replace("ó", "o", $liga);

        $liga = str_replace("ú", "u", $liga);

        $liga = str_replace("ñ", "n", $liga);

        $liga = str_replace("(", "-", $liga);

        $liga = str_replace(")", "-", $liga);

        $liga = str_replace("\'", "-", $liga);

        return $liga;

    }

    public static function recortarCadena($string, $limit, $break=".", $pad="…") { 
    	$string = substr($string, 0, $limit)."..."; 
        
        // return with no change if string is shorter than $limit 
    	/*if(strlen($string) <= $limit) 
    		return $string; // is $break present between $limit and the end of the string? 
    	if(false !== ($breakpoint = strpos($string, $break, $limit))) { 
    		if($breakpoint < strlen($string) - 1) { 
    			$string = substr($string, 0, $breakpoint) . $pad; 
    		}
    	
    	} */
    	return $string;
    }

     public static function generaLinkReporte($length=10,$uc=TRUE,$n=TRUE,$sc=FALSE){



        $source = '';

        if($uc==1) $source .= 'ABCDEFGHIJKLMNPQRSTUVWXYZ';

        if($n==1) $source .= '123456789';

        if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';

        if($length>0){

            $rstr = "";

            $source = str_split($source,1);

            for($i=1; $i<=$length; $i++){

                mt_srand((double)microtime() * 1000000);

                $num = mt_rand(1,count($source));

                $rstr .= $source[$num-1];

            }



        }

        return $rstr;

        

    }
}
?>